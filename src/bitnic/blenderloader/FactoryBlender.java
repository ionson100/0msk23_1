package bitnic.blenderloader;

import bitnic.core.Controller;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import org.apache.log4j.Logger;

import java.io.IOException;

public class FactoryBlender {

    private static final Logger log = Logger.getLogger(FactoryBlender.class);
    static FBlenderLoader cur_node2;
    public static void AddMessage(String msg){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if(cur_node2!=null){
                    cur_node2.label_message.setText(cur_node2.label_message.getText()+System.lineSeparator()+msg);
                }
            }
        });

    }

    public static void Run() {
        if(cur_node2!=null){
            stop();
        }
        FXMLLoader loader = new FXMLLoader(FBlenderLoader.class.getResource("f_loader.fxml"));
        Node pane = null;
        try {
            pane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e);
        }
        FBlenderLoader loader2 = loader.getController();
        cur_node2 = loader2;

        Controller.Instans.stack_panel.getChildren().add(pane);
    }

    public static void stop() {
        try {
            Platform.runLater(() -> {
                if (cur_node2 != null) {
                    cur_node2.close();
                    cur_node2 = null;
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(ex);
        }
    }


}
