package bitnic.blenderloader;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogBase;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public class FBlenderLoader implements Initializable {

    public Button bt_close;
    public Label label_message;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bt_close.setOnAction(e -> close());
    }

    public void close() {
        DialogBase.DeleteBlenda();

    }

}
