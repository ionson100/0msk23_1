package bitnic.pagetableproduct.itemproduct;

import bitnic.dialogfactory.DialogFactory;
import bitnic.pagesale.CustomScrollEvent;
import bitnic.pagetableproduct.IIttemProduct;
import bitnic.pagetableproduct.itemproductgroup.ItemProductGroupOne;
import bitnic.utils.UtilsOmsk;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import bitnic.model.MProduct;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class ItemProductOne extends GridPane implements Initializable,IIttemProduct {

    private double lastYposition=0d;
    private Logger log=Logger.getLogger(ItemProductOne.class);
    public ImageView image_product;
    public Label name_product;
    public Label price_product;
    public Label self_amount_product;
    private MProduct mProduct;


    public ItemProductOne(MProduct mProduct) {


        this.mProduct = mProduct;


        try {
        InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
        ResourceBundle bundle = new PropertyResourceBundle(inputStream);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_product.fxml"),bundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
        setUserData(mProduct);
    }

    boolean runDialog =false;
    @Override
    public void initialize(URL location, ResourceBundle resources) {


        setOnMousePressed( event -> {
            runDialog =true;
            lastYposition = event.getSceneY();
        } );


        this.minWidth(1200);
        name_product.setPadding(new Insets(0, 0, 0, mProduct.delta + 10));
        name_product.setText(mProduct.name);
        price_product.setText(String.valueOf(mProduct.price));
        self_amount_product.setText(String.valueOf(mProduct.amount_self));
        this.setOnMouseClicked(event -> {
            if(runDialog){
                DialogFactory.ProductDialog(mProduct);
            }
        });
        if (validateProductFinishDate(mProduct)==null) {
            image_product.setImage(new Image("/bitnic/image/bullet_w_down.png"));//просрочен
        }else if (validateProductFinishDate(mProduct)==false){
            image_product.setImage(new Image("/bitnic/image/50.png"));// подходит к концу
        }



        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                runDialog =false;
                double newYposition = event.getSceneY();
                double diff = 0;
                if(newYposition>lastYposition){
                    diff = newYposition - lastYposition+ UtilsOmsk.deltaScrol;
                }else {
                    diff = newYposition - lastYposition-UtilsOmsk.deltaScrol;
                }
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });


    }



    private Boolean validateProductFinishDate(MProduct mProduct) {
        if (mProduct.series == null || mProduct.series.trim().equals("")) {
            String d = "20180504";//mProduct.series.trim();
             DateFormat df = new SimpleDateFormat("yyyyMMdd");
            String d1=df.format(new Date());


            Date result1 = null;
            Date result2 = null;
            try {
                result1 = df.parse(d);
                result2 = df.parse(d1);
            } catch (ParseException e) {
                log.error(e);
                e.printStackTrace();
            }
           // System.out.println(result1);
           // System.out.println(result2);
            long milliseconds = result1.getTime() - result2.getTime();
            if(milliseconds<=0) return null;
            int days = (int) (milliseconds / (24 * 60 * 60 * 1000));
            if(days> SettingAppE.instance.maxDatecheckProduct)
                return true;

            return false;
        }
        return true;
    }

    @Override
    public void setOpen () {

    }

    @Override
    public void setClose (ListView<IIttemProduct> listView) {

    }

    @Override
    public MProduct getProduct () {
        return mProduct;
    }

    @Override
    public void setBlue () {
        name_product.setStyle("-fx-text-fill:#86d3e3 ");
        price_product.setStyle("-fx-text-fill:#86d3e3 ");
        self_amount_product.setStyle("-fx-text-fill:#86d3e3 ");
    }
}
