package bitnic.pagetableproduct;

import bitnic.model.MProduct;
import javafx.scene.control.ListView;

public  interface IIttemProduct{
    void setOpen ();
    void setClose (ListView<IIttemProduct> listView);
    MProduct getProduct ();
    void setBlue();
}
