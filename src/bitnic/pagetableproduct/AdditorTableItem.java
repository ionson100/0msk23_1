package bitnic.pagetableproduct;

import bitnic.model.MProduct;
import bitnic.pagetableproduct.itemproduct.ItemProductOne;
import bitnic.pagetableproduct.itemproductgroup.ItemProductGroupOne;
import bitnic.senders.IAction;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;

import java.util.List;

public class AdditorTableItem {

    public void build ( ListView listView , List <MProduct> mProductList , IAction iActionClick ) {

        listView.setItems(null);
        ObservableList observableList = FXCollections.observableArrayList();
        for (MProduct mProduct : mProductList) {

            if (mProduct.isGroup == 1) {
                ItemProductOne one = new ItemProductOne(mProduct);
                observableList.add ( one );
            } else {

                ItemProductGroupOne one = new ItemProductGroupOne(mProduct, iActionClick);
                one.setPrefWidth ( 1800 );
                observableList.add ( one );
            }

        }
        listView.setCellFactory(param -> new CheckCell());
        listView.setItems(observableList);
    }

    class CheckCell extends ListCell<IIttemProduct> {

        @Override
        public void updateItem(IIttemProduct item, boolean empty) {
            super.updateItem(item, empty);
            if (item == null) return;

                setGraphic( (Node) item );
        }
    }
}
