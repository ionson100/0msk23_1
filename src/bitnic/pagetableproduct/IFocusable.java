package bitnic.pagetableproduct;

public interface IFocusable{
    void focus();
}
