package bitnic.model;

import bitnic.orm.Configure;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IFrontolAction;

@Frontol(command = "DELETETAXRATES")
public class MCommandDELETETAXRATES implements IFrontolAction {

    @Override
    public void action() {
        Configure.GetSession().deleteTable("taxrates");
    }
}
