package bitnic.model;


import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IndexFrontol;
import bitnic.table.DisplayName;
import bitnic.table.DisplayViewer;

import java.util.Date;

@DisplayViewer(nameTable = "Таблица штрихкодов", isShowButton = true)
@Frontol(command = "ADDMARKETINGACTIONS")
@Table("marketing_action")
public class MMarketingAction {

    @DisplayName(name_column = "id",width = 70)
    @PrimaryKey("id")
    public int id;

    @DisplayName(name_column = "code",width = 70)
    @IndexFrontol(index = 1)
    @Column("code")
    public int code;//1 Да Целое Код маркетинговой акции

    @DisplayName(name_column = "date_start",width = 70)
    @IndexFrontol(index = 2)
    @Column("date_start")
    public Date dateStart;//2 Да Дата Дата начала действия акции


    @DisplayName(name_column = "date_finish",width = 70)
    @IndexFrontol(index = 3)
    @Column("date_finish")
    public Date dateFinish;//3 Да Дата Дата окончания действия акции

    @DisplayName(name_column = "time_start",width = 70)
    @IndexFrontol(index = 4)
    @Column("time_start")
    public Date timeStart;//4 Нет Время Время начала действия акции (для значения «0» вполе No11)

    @DisplayName(name_column = "time_finish",width = 70)
    @IndexFrontol(index = 5)
    @Column("time_finish")
    public Date timeFinish;//5Время Время окончания действия акции (для значения «0» в поле No11)

    @DisplayName(name_column = "name",width = 70)
    @IndexFrontol(index = 6)
    @Column("name")
    public String name;//6 Нет Строка 100 Наименование

    @DisplayName(name_column = "name_check",width = 70)
    @IndexFrontol(index = 7)
    @Column("name_check")
    public String name_check;//7 Нет Строка 100 Текст для чека

    @DisplayName(name_column = "state",width = 70)
    @IndexFrontol(index = 8)
    @Column("state")
    public Integer state;//8 Состояние настройки «Активная»:0 – выключена; 1 – включена.

    @DisplayName(name_column = "priority",width = 70)
    @IndexFrontol(index = 9)
    @Column("priority")
    public Integer priorityAction;//9 Приоритет акции (от 0 до 99, где 0 – наивысший приоритет)

    @DisplayName(name_column = "state_setings",width = 70)
    @IndexFrontol(index = 10)
    @Column("state_setings")
    public Integer stateSettings;//10 Состояние настройки 0 - выкл 1 - вкл

    @DisplayName(name_column = "state_settings_deay",width = 70)
    @IndexFrontol(index = 11)
    @Column("state_settings_deay")
    public Integer statesettingsDeay;//11Состояние настройки «Весь день»: 0 - выкл 1 - вкл

    @DisplayName(name_column = "type_action",width = 70)
    @IndexFrontol(index = 12)
    @Column("type_action")
    public Integer typeAction;//12 Вид акции:1 – спеццены;2 – скидки;3 – прочее.


}
