package bitnic.model;

import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IndexFrontol;
import bitnic.table.DisplayName;
import bitnic.table.DisplayViewer;

/**
 * ion100 on 10.01.2018.
 */
@DisplayViewer(nameTable = "Таблица пользователей", isShowButton = true)
@Table("taxgrouprates")
@Frontol(command = "ADDTAXGROUPRATES")
public class MTaxGroupRates {

    @DisplayName(name_column = "id",width = 70)
    @PrimaryKey("id")
    public int id;

    // 1 Да Целое Код налоговой ставки группы
    @DisplayName(name_column = "cod_nalog",width = 70)
    @IndexFrontol(index = 1)
    @Column("cod_namlog")
    public int cod_namlog;

    // 2 Да Целое Код налоговой группы
    @DisplayName(name_column = "cod_nalog_group",width = 70)
    @IndexFrontol(index = 2)
    @Column("cod_nalog_group")
    public int cod_nalog_group;

    // 3 Да Целое Код налоговой ставки
    @DisplayName(name_column = "tax_rate",width = 70)
    @IndexFrontol(index = 3)
    @Column("tax_rate")
    public int tax_rate;

    // 4 Да Целое  Смена базы: 0 – нет;  1 – да
    @DisplayName(name_column = "isCheckBase",width = 70)
    @IndexFrontol(index = 4)
    @Column("isCheckBase")
    public int isCheckBase;

}
