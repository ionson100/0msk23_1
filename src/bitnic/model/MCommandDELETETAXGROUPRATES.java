package bitnic.model;

import bitnic.orm.Configure;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IFrontolAction;

@Frontol(command = "DELETETAXGROUPRATES")
public class MCommandDELETETAXGROUPRATES implements IFrontolAction {

    @Override
    public void action() {

        Configure.GetSession().deleteTable("taxgrouprates");
    }
}
