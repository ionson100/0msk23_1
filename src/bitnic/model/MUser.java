package bitnic.model;


import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IndexFrontol;
import bitnic.table.DisplayName;
import bitnic.table.DisplayViewer;

import java.io.Serializable;


/**
 * Created by USER on 10.01.2018.
 */
@DisplayViewer(nameTable = "Таблица пользователей", isShowButton = true)
@Frontol(command = "ADDUSERS")
@Table("user")
public class MUser implements IInsertValidator,Serializable {


    @PrimaryKey("id")
    public int id;


    // 1 Да Строка 10 Код
    @DisplayName(name_column = "код",width = 70)
    @IndexFrontol(index = 1)
    @Column("cod")
    public String cod;


    //2 Да Строка 100 Наименование
    @DisplayName(name_column = "имя",width = 70)
    @IndexFrontol(index = 2)
    @Column("name")
    public String name;

    //3 Нет Строка 100 Текст для чека
    @DisplayName(name_column = "имя_чек",width = 70)
    @IndexFrontol(index = 3)
    @Column("name_check")
    public String name_check;



    // 4 Нет Строка 10 Код профиля пользователя
    @DisplayName(name_column = "профиль",width = 70)
    @IndexFrontol(index = 4)
    @Column("profile")
    public String profile;


    // 5 Нет Строка 50 Пароль
    @DisplayName(name_column = "pwd",width = 70)
    @IndexFrontol(index = 5)
    @Column("password")
    public String password;


    //6 Нет Строка 255 Набор символов, соответствующих карте, штрих  коду либо мех. ключу, используемому для идентификации пользовател
    @DisplayName(name_column = "штрихкод pwd",width = 70)
    @IndexFrontol(index = 6)
    @Column("map_user")
    public String map_user;


    @Override
    public String getHashKey() {
        return cod==null?"":cod;
    }
}
