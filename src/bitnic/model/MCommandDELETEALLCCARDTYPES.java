package bitnic.model;

import bitnic.orm.Configure;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IFrontolAction;

@Frontol(command = "DELETEALLCCARDTYPES")
public class MCommandDELETEALLCCARDTYPES implements IFrontolAction {

    @Override
    public void action() {
        Configure.GetSession().deleteTable("card_type");
    }
}
