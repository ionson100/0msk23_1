package bitnic.model;

import bitnic.orm.Configure;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IFrontolAction;

@Frontol(command = "DELETETAXGROUPS")
public class MCommandDELETETAXGROUPS implements IFrontolAction {

    @Override
    public void action() {
        Configure.GetSession().deleteTable("taxgroup");
    }
}
