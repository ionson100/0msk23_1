package bitnic.model;

import bitnic.orm.Configure;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IFrontolAction;

/**
 * Created by USER on 10.01.2018.
 */

@Frontol(command = "REPLACEASPECTREMAINS")
public class MCommandREPLACEASPECTREMAINS implements IFrontolAction {
    @Override
    public void action() {
        Configure.GetSession().deleteTable("barcode");
    }
}
