package bitnic.model;

import java.util.ArrayList;
import java.util.List;

public class ModelCollection {
   public static List<Class> classes=new ArrayList<>();
   static {
       classes.add(MBarcodes.class);
       classes.add(MCardType.class);
       classes.add(MCheck.class);
       //classes.add(MError.class);
       classes.add(MProduct.class);
       classes.add(MTaxGroup.class);
       classes.add(MTaxGroupRates.class);
       classes.add(MTaxrates.class);
       classes.add(MUser.class);
       classes.add(MMarketingAction.class);

       classes.add(MCommandADDTAXGROUPRATES.class);
       classes.add(MCommandDELETEALLASPECTREMAINS.class);
       classes.add(MCommandDELETEALLBARCODES.class);
       classes.add(MCommandDELETEALLUSERS.class);
       classes.add(MCommandDELETEALLWARES.class);
       classes.add(MCommandREPLACEASPECTREMAINS.class);

       classes.add(MCommandDELETETAXGROUPRATES.class);
       classes.add(MCommandDELETETAXGROUPS.class);
       classes.add(MCommandDELETETAXRATES.class);
       classes.add(MCommandDELETEALLCCARDTYPES.class);
       classes.add(MCommandDELETEALLMARKETINGEVENTSBYACTIONCODE.class);
   }
}
