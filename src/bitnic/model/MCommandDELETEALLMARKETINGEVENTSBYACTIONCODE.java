package bitnic.model;

import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IndexFrontol;

@Frontol(command = "DELETEALLMARKETINGEVENTSBYACTIONCODE")
public class MCommandDELETEALLMARKETINGEVENTSBYACTIONCODE {

    @IndexFrontol(index = 1)
    public Integer idaction;

}
