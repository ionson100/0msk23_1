package bitnic.model;


import bitnic.orm.Column;
import bitnic.orm.PrimaryKey;
import bitnic.orm.Table;
import bitnic.parserfrontol.Frontol;
import bitnic.parserfrontol.IndexFrontol;
import bitnic.table.DisplayName;
import bitnic.table.DisplayViewer;

/**
 * ion100 10.01.2018.
 */
@DisplayViewer(nameTable = "Таблица пользователей", isShowButton = true)
@Frontol(command = "ADDTAXGROUPS")
@Table("taxgroup")
public class MTaxGroup {

    @DisplayName(name_column = "id",width = 70)
    @PrimaryKey("id")
    public int id;

    // 1 Да Целое Код налоговой группы
    @DisplayName(name_column = "croup_nalog",width = 70)
    @Column("croup_nalog")
    @IndexFrontol(index = 1)
    public int croup_nalog;

    // 2 Нет Строка 100 Наименование налоговой группы
    @DisplayName(name_column = "name",width = 70)
    @Column("name")
    @IndexFrontol(index = 2)
    public String name;

    // 3 Нет Строка 100 Текст
    @DisplayName(name_column = "text_",width = 70)
    @Column("text_")
    @IndexFrontol(index = 3)
    public String text_;

}
