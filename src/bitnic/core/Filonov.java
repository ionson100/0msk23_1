package bitnic.core;

import java.io.Serializable;

public class Filonov implements Serializable {
   public int id;
   public String name;
   public String des;
   public String param;

   public Filonov(){

   }

   public Filonov(int id, String name, String des,String param){

       this.id = id;
       this.name = name;
       this.des = des;
       this.param = param;
   }
}
