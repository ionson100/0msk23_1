package bitnic.core;

import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.question.ButtonAction;
import bitnic.header.Cheader;
import bitnic.pageerror.PageError;
import bitnic.senders.SenderLogToServer;
import bitnic.updateapp.StarterUpdate;
import bitnic.updateapp.UpdateApp;
import bitnic.utils.support;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import bitnic.kassa.StateKassaMoney;

import bitnic.pageadmin.TableAdmin;
import bitnic.pagechat2.FChat2;
import bitnic.pagecheck.FChesk;
import bitnic.pageconstrictor.FConsdtructorCheck;
import bitnic.pagesale.Cmainform;
import bitnic.pagesettings.FEditSettings;
import bitnic.pagesettingskkm.FSettingsKkm;
import bitnic.pagetableproduct.IRefrashLoader;
import bitnic.pagetableproduct.TableProduct;
import bitnic.parserfrontol.testfrontol;
import bitnic.senders.IAction;
import bitnic.senders.Sender_1C;
import bitnic.settingscore.SettingAppE;
import bitnic.settingscore.SettingAppMode;
import bitnic.settingscore.SettingsE;
import bitnic.updateapp.TimerAppUpdate;
import bitnic.transaction.Transaction;
import bitnic.utils.DesktopApi;
import bitnic.utils.Pather;
import bitnic.utils.PrintUserLabel;
import bitnic.updateapp.RollBackApp;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private ResourceBundle bundle;
    private static final Logger log = Logger.getLogger(Controller.class);
    public static TimerAppUpdate TimerUpdate;
    public static Controller GetInstans() {
        return Instans;
    }

    public HBox hbox_footer;
    public MenuItem bm1;
    public MenuItem bm2;
    public MenuItem bm3;
    public MenuItem bm4;
    public MenuItem bm5;
    public MenuItem bm6;

    public MenuItem bm8;
    public MenuItem bm9;
    public MenuItem bm10;
    public MenuItem bm11;
    public HBox panel_buttons;

    public static Node Curnode;
    public static Cmainform Ccmainform;
    public MenuButton bt_menu_almin, bt_menu_kassa;//bt_menu_settings, bt_menu_kassa_settings,;

    public Label label_profile, label_1, label_2, label_3,label_version,label_point_id;

    public void factoryPage(Node node) {

        Curnode = node;

        if (node instanceof Cmainform) {
            Ccmainform = (Cmainform) node;
        } else {
            Ccmainform = null;
        }
        if (grid_host.getChildren().size() == 0) {
            grid_host.getChildren().add(node);
        } else {
            Node o = grid_host.getChildren().get(0);
            if (o instanceof IDestroy) {
                ((IDestroy) o).destroy();
            }
            grid_host.getChildren().clear();
            grid_host.getChildren().add(node);
        }
    }

    public static void RefrachProfileApp() {
        Instans.label_profile.setText(String.format("Профиль: %s", SettingAppE.instance.getNameProfile()));
    }


    public static void WriteStateKkm(String s1, String s2, String s3) {
        // выручка
        //Сумма наличности в ККМ (Summ)
        //Cумма сменного итога приход

        if (Instans != null)
            Platform.runLater(() -> {
                Instans.label_1.setText("Выручка: " + s1);
                //Instans.label_1.setVisible(false);
                Instans.label_2.setText("Наличность в ККМ: " + s2);
                Instans.label_3.setText("Сменый итог: " + s3);
                //Instans.label_3.setVisible(false);
            });
    }

    public GridPane grid_host;
    public StackPane stack_panel;
    public static Controller Instans;
    private TextField text_status_app;
    @FXML
    public GridPane header;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        bundle = resources;
        String name = ManagementFactory.getRuntimeMXBean().getName();
        log.info("Process ID for this app = " + name);
        Instans = this;
        try{
            label_version.setText(bundle.getString("name120")+String.valueOf(SettingAppE.instance.getVersion()));
        }catch (Exception ex){
            log.error(ex);
        }

        refrashPointid();


        StarterUpdate.CheckUpdate(this);
        refrashMenu();
        RefrachProfileApp();

        TimerUpdate =new TimerAppUpdate();
        TimerUpdate.run(SettingAppE.instance.tumerIntervalUpdate,this);

        if (header != null) {
            Label label_user = (Label) header.lookup("#label_user");
            text_status_app = (TextField) header.lookup("#status_app");
            label_user.setText(SettingAppE.instance.getUserName());
            text_status_app.setText("");
        }
//        if(SettingAppE.instance.isTypeshow){
//            text_status_app.getStyleClass().add("delivery_3d");
//        }

        factoryPage(new Cmainform());
        WriteMessage(bundle.getString("name107"));


        new StateKassaMoney().print();
    }

    public static void WriteMessage(String message) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (Instans == null || Instans.text_status_app == null) return;
                Instans.text_status_app.setText(message);
            }
        });

    }


    public void onMenu(ActionEvent actionEvent) {

        String id = "";
        if (actionEvent.getSource() instanceof Node) {
            id = ((Node) actionEvent.getSource()).getId();
        } else if (actionEvent.getSource() instanceof MenuItem) {
            id = ((MenuItem) actionEvent.getSource()).getId();
        }
        if (id.equals("bt_menu_sale")) {
            factoryPage(new Cmainform());
        } else if (id.equals("bt_menu_admin")) {
            factoryPage(new TableAdmin());
        }
    }


    public void onRegistration(ActionEvent actionEvent) {

    }


    public void onUpdateDateFromServer() {

        if(SettingAppE.instance.getPointId()==0){
            DialogFactory.InfoDialog("Обновление",bundle.getString("name108"));
        }else {
            if (SettingAppE.instance.selectProduct.size() > 0) {
                DialogFactory.InfoDialog("Обновление",bundle.getString("name109"));
            } else {
                new testfrontol().test(new IAction() {
                    @Override
                    public boolean action(Object o) {
                        if (Controller.Curnode != null) {
                            if (Curnode instanceof IRefrashLoader) {
                                ((IRefrashLoader) Curnode).refrash();
                            }
                        }
                        return false;
                    }
                });
            }
        }




    }

    public void onFtpSettings() {
        DialogFactory.SettingsFtpDialog();
    }

    public void onRegUser() {
        if(SettingAppE.instance.selectProduct.size()>0){
            DialogFactory.InfoDialog("Смена пользователя",bundle.getString("name110"));
        }else {
            DialogFactory.RegUserDialog();
        }

    }

    public void onUnRegUser() {

        if(SettingAppE.instance.selectProduct.size()>0){
           DialogFactory.InfoDialog("Закрытие смены",bundle.getString("name111"));
        }else {
            SettingAppE.instance.user_id = null;
            SettingAppE.save();
            SettingAppE.RefrashInstance();
            Cheader.RefrashUser();
            refrashMenu();
            Controller.GetInstans().factoryPage(new Cmainform());
        }

        //Platform.exit();
    }

    public void onTest() {


       Main.MyWebSockerError.stop();
//        DialogFactory.ErrorDialog("sdsd");

//        if(SettingAppE.instance.getProfile()==1){
//            Charset cset = Charset.forName("windows-1251");
//            List<String> lines1 =new ArrayList<>();
//            try {
//                lines1= Files.readAllLines(Paths.Get(Pather.sender_report_file), cset);
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            }
//            new SenderFileBitnic().send(lines1);
//        }

    }

    public void onPrintTestCheck() {
        new bitnic.kassa.PrintTestCheck().print();
    }

    public void onSettingsKassa() {
        bitnic.kassa.SettingKassa.Print();
    }

    public void onTestConnectionKassa() {
        new bitnic.kassa.TestConnect().print();
    }

    public void onStateKassa() {

//        MStateKKM kkm=new MStateKKM();
//        kkm.notSenderOfd=1212;
//        kkm.mode=1;
//        kkm.notSenderFirstDate=new Date();
//        kkm.ofdUrl="sadasd";
//        kkm.serialNumer="sasdad";
//        kkm.username="sadasd";
//        kkm.userPassword="sadsadasd";
//        kkm.date=new Date();
//        kkm.inn="asdasdasd";
//        kkm.errorMessage="asdasd";
//
//
//        DialogFactory.StateKkmDialog(kkm);
        new bitnic.kassa.StateKassa().print(stateKKM -> {
            DialogFactory.StateKkmDialog(stateKKM);
            return false;
        });
    }

    public void onUserSettingsKassa() {
        new bitnic.kassa.UserSettungsKmm().print();
    }

    public void onCloseCheckKassa() {
        new bitnic.kassa.CloseCheckKmm().print();
    }

    public void onZReport() {

        new Transaction().reportZ();

    }

    public void onCheckOfd() {
        new bitnic.kassa.CheckOfd().print();
    }

    public void onGridProducts() {

        factoryPage(new TableProduct());
    }

    public void onSendErrorToServer() {


        new SenderLogToServer().send();
    }



    public void onSettingOfd() {
        DialogFactory.SettingsOfdDialog();
    }

    public void onAppMode() {
        DialogFactory.AppModeDialog();
    }

    public void onSend1C() {
        if(SettingAppE.instance.getProfile()==1){
            DialogFactory.InfoDialog("Отправка 1С",bundle.getString("name112"));
        }else {
            new Sender_1C().send();
        }

    }

    public void onChat() {

        //factoryPage(new FEditSettings());
        OpenChat();
    }

    public static void OpenChat() {
        FXMLLoader loader = new FXMLLoader(FChat2.class.getResource("f_chat2.fxml"));
        Node pane = null;
        try {
            pane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Controller.Instans.stack_panel.getChildren().add(pane);
    }

    public void onMenuWorkCheck() {
        factoryPage(new FChesk());
    }

    public void onCloseApp() {
        DialogFactory.QuestionDialog ( support.str ("name143"),support.str ( "name144" ),
                new ButtonAction ( support.str ( "name145" ) , new IAction <Object> () {
                    @Override
                    public boolean action ( Object o ) {
                        Platform.exit();
                        System.exit(0);
                        return false;
                    }
                } ) );

    }

    public void onShutdown(){
        if(DesktopApi.getOs ()==DesktopApi.EnumOS.linux){
            DialogFactory.QuestionDialog ( support.str ("name146"),support.str ( "name147" ),
                    new ButtonAction ( support.str ( "name148" ) , o -> {
                        //Shutdown
                        ExeScript testScript = new ExeScript();
                        testScript.runScript("sudo shutdown -h now");
                        return false;
                    } ),
                    new ButtonAction ( support.str ( "name149" ) , o -> {
                        ExeScript testScript = new ExeScript();
                        testScript.runScript("sudo  reboot");
                        return false;
                    } ) );
       }else {
            DialogFactory.InfoDialog ( support.str ("name146"),support.str ( "name151" ) );
        }

    }

    public void onSettingsKkm() {
        factoryPage(new FSettingsKkm());
    }

    public void onPrintBarcode() {
        DialogFactory.PrintBarcodeDialog();
    }

    public void onContributingMoney() {
        DialogFactory.ContributindMoneyDialog();
    }

    public void onEntryMoney() {
        DialogFactory.EntryMoneyDialog();
    }

    public void onPrintLastDoc() {
        new bitnic.kassa.PrintLastCheck().print();
    }

    public void onXReport() {
        new Transaction().reportX();

    }

    public void onOpenSmena() {

        new Transaction().openSession();

    }

    public void onRegFirma() {
        DialogFactory.RegCodeFirmDialig();
    }

    public void onPrintLabelUser() {
        PrintUserLabel.Print();
    }

    public void onConstructorCheck() {
        factoryPage(new FConsdtructorCheck());
    }

    public void onCheckStory() {
        DialogFactory.CheckStoryDialog();
    }

    public void onOpenInFile() {

        try {
            String patch = Pather.inFilesData;
            System.out.println(patch);
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.ErrorDialog(ex);
            ex.printStackTrace();
        }

    }

    public void onOpenOutFile() {
        try {
            String patch = Pather.sender_report_file;
            System.out.println(patch);
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.ErrorDialog(ex);
            ex.printStackTrace();
        }
    }

    public void onSettingsEShow(ActionEvent actionEvent) {
        try {
            SettingsE s = new SettingsE();
            String patch = s.getFileName();
            System.out.println(patch);
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.ErrorDialog(ex);
            ex.printStackTrace();
        }
    }

    public void onSettingAppModeShow(ActionEvent actionEvent) {
        try {
            SettingAppMode s = new SettingAppMode();
            String patch = s.getFileName();
            System.out.println(patch);
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.ErrorDialog(ex);
            ex.printStackTrace();
        }
    }

    public void onSettingAppEShow(ActionEvent actionEvent) {
        try {
            SettingAppE s = new SettingAppE();
            String patch = s.getFileName();
            System.out.println(patch);
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.ErrorDialog(ex);
            ex.printStackTrace();
        }
    }

    public void refrashMenu() {
        bm1.setVisible(true);
        bm2.setVisible(true);
        bm3.setVisible(true);
        bm4.setVisible(true);
        bm5.setVisible(true);
        bm6.setVisible(true);

        bm8.setVisible(true);
        bm9.setVisible(false);
        bm10.setVisible(true);
        bm11.setVisible(true);
        if (SettingAppE.instance.user_id == null) {
            Instans.bt_menu_almin.setVisible(false);
            Instans.bt_menu_kassa.setVisible(false);

            bm1.setVisible(false);
            bm2.setVisible(false);
            bm3.setVisible(false);
            bm4.setVisible(false);
            bm5.setVisible(false);
            bm6.setVisible(false);

            bm8.setVisible(false);
            bm9.setVisible(false);
            bm10.setVisible(false);
            bm11.setVisible(false);
            //Instans.bt_menu_kassa_settings.setVisible(false);
            //Instans.bt_menu_settings.setVisible(false);
        } else if (SettingAppE.instance.user_id.equals("1")) {
            Instans.bt_menu_almin.setVisible(true);
            Instans.bt_menu_kassa.setVisible(true);
            //Instans.bt_menu_kassa_settings.setVisible(true);
            //Instans.bt_menu_settings.setVisible(true);
        } else {
            Instans.bt_menu_almin.setVisible(false);
            Instans.bt_menu_kassa.setVisible(true);
            //Instans.bt_menu_kassa_settings.setVisible(true);
            //Instans.bt_menu_settings.setVisible(true);
        }

    }

    public void onDllDirShow(ActionEvent actionEvent) {
        DialogFactory.InfoDialog("java.library.path", System.getProperty("java.library.path"));
    }

    public void onDllDirShow2(ActionEvent actionEvent) {
        DialogFactory.InfoDialog("java.library.path2", Pather.dllDirectory);
    }

    public void onEditSettingsPage(ActionEvent actionEvent) {
        factoryPage(new FEditSettings());
    }

    public void onPrintOutFile(ActionEvent actionEvent) {
        DialogFactory.PrintOutFileDialog();
    }

    public void onDeletKeyBoard(ActionEvent actionEvent) {

         String MainClass = "sample.Main";

        try {
            File jar=new File("/home/ion100/test1.jar");

            URLClassLoader ucl = new URLClassLoader(new URL[] { jar.toURL() });
            Class cls = null; //ucl.close();
            cls = ucl.loadClass(MainClass);
            Method meth = cls.getMethod("main", new Class[] { String[].class });

            String[] s=new String[0];
            meth.invoke(null, s);
            Platform.exit();
        } catch (Exception e) {
            e.printStackTrace();
        }

       //


//
//            ExeScript testScript = new ExeScript();
//            String as="java -jar "+Pather.curdir+File.separator+"test1.jar";
//            testScript.runScript(as);//"sh scripts/key1.sh"
//
//            ProcessBuilder pb = new ProcessBuilder("scripts/key1.sh", "myArg1", "myArg2");
//
//
//            Process p = pb.start();

//            String target = new String("scripts/key1.sh");
//            String[] cmd = { "bash", "-c", "scripts/key1.sh" };
//            Process p = Runtime.getRuntime().exec(cmd);


    }

    public void onInstalKeyBoard(ActionEvent actionEvent) {
    }

    public void onRollBackApp(ActionEvent actionEvent) {

        RollBackApp backApp=new RollBackApp();
        backApp.run();
    }

    public void onOpenLog(ActionEvent actionEvent) {
        try {
            String patch = System.getProperty("user.home")+File.separator+"omsk.log";
            System.out.println(patch);
            DesktopApi.open(new File(patch));
        } catch (Exception ex) {
            DialogFactory.ErrorDialog(ex);
            ex.printStackTrace();
        }
    }

    public void onUpdateApp(ActionEvent event) {
        if (SettingAppE.instance.selectProduct.size() > 0) {
            DialogFactory.InfoDialog("Обновление", bundle.getString("name113"));
        } else {

            new UpdateApp().run(this);
        }
    }

    public void onHomeDirectory(ActionEvent actionEvent) {
        if (DesktopApi.getOs().isLinux()) {
            ExeScript testScript = new ExeScript();
            testScript.runScript("xdg-open "+Pather.curdir+"/");
        }else {
            Desktop desktop = Desktop.getDesktop();
            File dirToOpen = null;
            try {
                dirToOpen = new File(Pather.curdir);
                desktop.open(dirToOpen);
            } catch (Exception iae) {
                log.error(iae);
            }
        }

    }

    public void onOpenFolderApp(ActionEvent actionEvent) {
        if (DesktopApi.getOs().isLinux()) {
            ExeScript testScript = new ExeScript();
            testScript.runScript("xdg-open "+System.getProperty("user.dir"));
        }else {
            Desktop desktop = Desktop.getDesktop();
            File dirToOpen = null;
            try {
                dirToOpen = new File(System.getProperty("user.home"));
                desktop.open(dirToOpen);
            } catch (Exception iae) {
                log.error(iae);
            }
        }

    }

    public void refrashPointid() {
        label_point_id.setText("Точка: "+String.valueOf(SettingAppE.instance.getPointId()));
    }

    public void onShowErrorPage ( ActionEvent actionEvent ) {
        factoryPage ( new PageError () );
    }
}



