package bitnic.core;

import bitnic.appender.MyWebSocket;
import bitnic.dialogfactory.DialogFactory;
import bitnic.settings.Reanimation;
import bitnic.updateapp.RollBackApp;
import bitnic.utils.DesktopApi;
import bitnic.utils.Pather;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import bitnic.utils.Starter;
import javafx.stage.WindowEvent;
import org.apache.log4j.Logger;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.util.Locale;
import java.util.ResourceBundle;

public class Main extends Application {

    private static final Logger log = Logger.getLogger(RollBackApp.class);
    public static MyWebSocket MyWebSockerError;
    public static Stage MyStage;

    public static Scene MyScene;
    public static ResourceBundle MyBundle;


    static {
        new Starter().start();


    }

    @Override
    public void start(Stage primaryStage) throws Exception {
         MyWebSockerError =new MyWebSocket();
         MyWebSockerError.start();

//        PatternLayout layout = new PatternLayout();
//        String conversionPattern = "%-7p %d [%t] %c %x - %m%n";
//        layout.setConversionPattern(conversionPattern);
//
//        // creates console appender
//        ConsoleAppender consoleAppender = new ConsoleAppender();
//        consoleAppender.setLayout(layout);
//        consoleAppender.activateOptions();
//
//        // creates file appender
//
//        FileAppender fileAppender = new FileAppender();
//
//        fileAppender.setFile("/home/ion100/ion100.log");
//        fileAppender.setLayout(layout);
//        fileAppender.activateOptions();
//
//        // configures the root logger
//        Logger rootLogger = Logger.getRootLogger();
//        rootLogger.setLevel(Level.DEBUG);
//        rootLogger.addAppender(consoleAppender);
//        rootLogger.addAppender(fileAppender);


        Thread.setDefaultUncaughtExceptionHandler(Main::showError);
        Locale locale = new Locale("ru", "RU");

        MyBundle = ResourceBundle.getBundle("aa", locale);
        Reanimation.SetBaseSettingsPath(Pather.base_sqlite_settings);
        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"), MyBundle);
        primaryStage.setTitle("BS Retail");
        primaryStage.setScene(new Scene(root, 1100, 800));
        MyScene = primaryStage.getScene();
        primaryStage.show();
        MyStage = primaryStage;
        MyStage.getIcons().add(new Image(getClass().getResourceAsStream("/bitnic/image/logo-mini.png")));
        MyStage.requestFocus();

        //MyStage.setMaximized(true);

        MyScene.addEventFilter(KeyEvent.KEY_PRESSED,
                event -> {
                    System.out.println("Pressed: " + event.getCode());
                    if (Controller.Ccmainform != null) {
                        Controller.Ccmainform.refrashKeyBoard(event.getText());
                    }
                });

        MyStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                try {
                    MyWebSockerError.stop();
                }catch (Exception ex){

                    log.error(ex);
                }finally {
                    String name = ManagementFactory.getRuntimeMXBean().getName();
                    String pid=name.substring(0,name.indexOf("@"));
                    ExeScript testScript = new ExeScript();
                    if(DesktopApi.getOs()== DesktopApi.EnumOS.linux){

                        testScript.runScript("kill "+pid);
                    }
                    if(DesktopApi.getOs ()== DesktopApi.EnumOS.windows){
                        //Taskkill /PID 26356 /F
                        testScript.runScript("Taskkill /PID "+pid+" /F");
                    }
                }
            }
        });


    }


    public static void main(String[] args) {

//        if (args != null && args.length > 0 && args[0].equals("-c")) {
//
//            try {
//                File f = new File(Pather.outFile);
//                f.createNewFile();
//                PrintStream st = new PrintStream(new FileOutputStream(Pather.outFile));
//                System.setErr(st);
//                System.setOut(st);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
        launch(args);
    }


    private static void showError(Thread t, Throwable e) {
        System.err.println("***Default exception handler***");
        log.error(e);

        if (Platform.isFxApplicationThread()) {
            showErrorDialog(e);
        } else {
            System.err.println("An unexpected bitnic.pageerror occurred in " + t);
        }
    }

    private static void showErrorDialog(Throwable e) {
        StringWriter errorMsg = new StringWriter();
        e.printStackTrace(new PrintWriter(errorMsg));
        DialogFactory.ErrorDialog(errorMsg.toString());
    }

}
