package bitnic.pageerror;

import bitnic.model.MError;
import bitnic.utils.BuilderTable;
import bitnic.utils.Starter;
import bitnic.utils.UtilsOmsk;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class PageError extends GridPane implements Initializable {

    private static final Logger log = Logger.getLogger(PageError.class);

    public TableView table_error;
    public GridPane grid1;
    GridPane pane;

    public PageError () {
        try {
            InputStream inputStream = getClass ().getClassLoader ().getResource ( "aa_ru_RU.properties" ).openStream ();
            ResourceBundle bundle = new PropertyResourceBundle ( inputStream );
            FXMLLoader fxmlLoader = new FXMLLoader ( getClass ().getResource ( "page_eror.fxml" ) , bundle );
            fxmlLoader.setRoot ( this );
            fxmlLoader.setController ( this );

            pane = fxmlLoader.load ();
        } catch (IOException exception) {
            log.error ( exception );
            throw new RuntimeException ( exception );
        }
    }

    @Override
    public void initialize ( URL location , ResourceBundle resources ) {

        UtilsOmsk.resizeNode ( this , grid1 );
        List <MError> list = new ArrayList <> ();
        List <String> lines = new ArrayList <> ();
        try {
            lines = Files.readAllLines ( Paths.get ( System.getProperty ( "user.home" ) + File.separator + "omsk.log" ) );
        } catch (IOException e1) {
            e1.printStackTrace ();
        }

        for (String line : lines) {
            MError er = new MError ();
            er.msg = line;
            list.add ( er );
        }

        log.info ( "Строк ошибок - "+list.size () );
        new BuilderTable ().bubligum ( list , table_error );

    }
}
