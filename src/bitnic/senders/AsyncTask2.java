package bitnic.senders;

import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;

import java.io.IOException;

/**
 * Created by ion on 12.01.2018.
 */
public abstract class AsyncTask2<T1, T2, T3> extends Task {

    private boolean isDeamon = true;

    public void setDemon(boolean b) {


        isDeamon = b;
    }

    public boolean isDamon() {
        return isDeamon;
    }

    private T1[] params;

    protected abstract T3 doInBackground(T1... params) ;

    protected abstract void onPreExecute() throws IOException;

    protected abstract void onPostExecute(T3 params);


    //public abstract void onError(WorkerStateEvent event);

    T3 res = null;

    @Override
    protected T3 call() throws Exception {


        return doInBackground(params);
    }

    public void execute(T1[] params) {
        this.params = params;

        setOnScheduled((e) -> {
            try {
                onPreExecute();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });



        setOnFailed(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                Object ss=event;
            }
        });



        this.setOnSucceeded((e) -> {
            T3 returnValue = null;
            try {
                returnValue = (T3) this.get();
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                onPostExecute(returnValue);
            }
        });

//        setOnFailed(new EventHandler<WorkerStateEvent>() {
//            @Override
//            public void handle(WorkerStateEvent event) {
//                onError(event);
//            }
//        });

        Thread thread = new Thread(this);
        thread.start();


    }


}
