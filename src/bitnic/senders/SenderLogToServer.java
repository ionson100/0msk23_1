package bitnic.senders;

import bitnic.blenderloader.FactoryBlender;
import bitnic.dialogfactory.DialogFactory;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;

public class SenderLogToServer {
    private static final Logger log = Logger.getLogger(SenderLogToServer.class);
    Exception exception;

    String path=System.getProperty("user.home")+ File.separator+"omsk.log";
    public void send(){


        new inner().execute(null);

    }
    public class inner extends AsyncTask2<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            HttpsURLConnection connection=null;
            try{
                File file=new File(path);
                if(file.exists()==false){
                    throw new RuntimeException("Файл лога не создан! \n( "+path+" )");
                }
                String url="https://"+ SettingAppE.instance.getUrl()+"/pos_data";
                log.info("Отсылка файла лога на сервер - "+url);
                URL serverUrl =new URL(url);
                connection = (HttpsURLConnection) serverUrl.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                OutputStream outputStreamToRequestBody = connection.getOutputStream();
                BufferedWriter httpRequestBodyWriter =new BufferedWriter(new OutputStreamWriter(outputStreamToRequestBody));
                httpRequestBodyWriter.flush();
                InputStream stream =  new FileInputStream(file);
                int bytesRead;
                byte[] dataBuffer = new byte[1024];
                while((bytesRead = stream.read(dataBuffer)) != -1) {
                    outputStreamToRequestBody.write(dataBuffer, 0, bytesRead);
                }
                outputStreamToRequestBody.flush();
                httpRequestBodyWriter.flush();
                outputStreamToRequestBody.close();
                httpRequestBodyWriter.close();
                connection.connect();
                final int status = connection.getResponseCode();
                if (status != 200) {
                    throw new RuntimeException("Запрос goods, ответ сервера " + status);
                }
                log.info("Статус - "+status);

//                InputStream input = connection.getInputStream();
//
//                String result = new BufferedReader(new InputStreamReader(input)).lines().collect(Collectors.joining("\n"));;
//                log.info("Ответ сервера строка -  "+result);

//                input.close();
            }catch (Exception ex){
                exception=ex;
            }finally {
                if(connection!=null){
                    connection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() throws IOException {
            FactoryBlender.Run();
        }

        @Override
        protected void onPostExecute(Void params) {
            FactoryBlender.stop();
            if(exception!=null){
                DialogFactory.ErrorDialog(exception);
            }
        }
    }
}
