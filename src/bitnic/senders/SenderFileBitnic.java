package bitnic.senders;

import bitnic.dialogfactory.DialogFactory;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

public class SenderFileBitnic {

    private static final Logger log = Logger.getLogger(SenderFileBitnic.class);

    private List<String> strings;
    SettingAppE setting = SettingAppE.instance;

    public void send(List<String> strings) {

        this.strings = strings;

        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask2<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {


            String url="https://"+setting.getUrl()+"/pos_data";

            log.info("Отсылка файла на сервер - "+url);
            log.info("Выходной файл содержит - "+strings.size()+" строк");
            StringBuilder sb = new StringBuilder();

            sb.append("#\r\n");// + System.lineSeparator());
            sb.append("1\r\n");// + System.lineSeparator());
            sb.append("22\r\n");// + System.lineSeparator());
            int i=3;
            for (String line : strings) {

                if (line.trim().length() == 0) continue;
                if (line.indexOf(";") == -1) continue;
                String[] ss = line.split(";", -1);
                if(ss.length<5) continue;
                if (ss[3].equals("300")) continue;
                sb.append(line + "\r\n");//System.lineSeparator()
                i++;
            }

            log.info("Количество строк после обработки - "+i);


            HttpsURLConnection connection=null;
          try{

              log.info("Отсылка  сервер");
              URL serverUrl =new URL(url);
              connection = (HttpsURLConnection) serverUrl.openConnection();

              connection.setDoOutput(true);
              connection.setRequestMethod("POST");
              OutputStream outputStreamToRequestBody = connection.getOutputStream();
              BufferedWriter httpRequestBodyWriter =
                      new BufferedWriter(new OutputStreamWriter(outputStreamToRequestBody));

              httpRequestBodyWriter.flush();
              InputStream stream = new ByteArrayInputStream(sb.toString().getBytes());


              int bytesRead;
              byte[] dataBuffer = new byte[1024];
              while((bytesRead = stream.read(dataBuffer)) != -1) {
                  outputStreamToRequestBody.write(dataBuffer, 0, bytesRead);
              }
              outputStreamToRequestBody.flush();
              httpRequestBodyWriter.flush();
              outputStreamToRequestBody.close();
              httpRequestBodyWriter.close();
              connection.connect();
              final int status = connection.getResponseCode();
              if (status != 200) {
                  throw new RuntimeException("Запрос goods, ответ сервера " + status);
              }
              log.info("Статус - "+status);
              InputStream input = connection.getInputStream();
              String result = new BufferedReader(new InputStreamReader(input)).lines().collect(Collectors.joining("\n"));;
              log.info("Ответ сервера строка -  "+result);

              input.close();
          }catch (Exception ex){
              DialogFactory.ErrorDialog(ex.getMessage());
              log.error(ex);
             ex.printStackTrace();
          }finally {
              if(connection!=null){
                  connection.disconnect();
              }
          }

            return null;


        }

        @Override
        protected void onPreExecute() throws IOException {
        }

        @Override
        protected void onPostExecute(Void params) {

        }
    }
}
