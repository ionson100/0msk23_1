package bitnic.senders;


import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.utils.Pather;
import bitnic.utils.support;
import javafx.scene.control.ProgressBar;

import bitnic.model.IInsertValidator;
import bitnic.model.MCommandDELETEALLMARKETINGEVENTSBYACTIONCODE;
import bitnic.model.MMarketingAction;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import bitnic.orm.Configure;
import bitnic.orm.ISession;
import bitnic.parserfrontol.CacheMetaDateFrontol;
import bitnic.parserfrontol.CreatorInstace;
import bitnic.parserfrontol.IFrontolAction;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * ion100 on 11.01.2018.
 */


public class SenderFtp {
    private static final Logger log = Logger.getLogger(SenderFtp.class);

    private boolean success;

    private StringBuilder error = new StringBuilder();
    //   private String server = "178.62.249.118";
//   private int port = 21;
//   private String user = "tester";
//   private String pass = "rU6a2";
//   private String remoteFile2 = "goods2.txt";
//   private File downloadFile2=new File("d:/goods22.txt");
    private Map<String, CacheMetaDateFrontol> dateFrontolMap;
    private IAction refrash;
    private SettingAppE ftp = SettingAppE.instance;
    private boolean isUTF = true;


    public SenderFtp() {

        if (SettingAppE.instance.getUrl() == null || SettingAppE.instance.getUrl().trim().length() == 0) {
            DialogFactory.ErrorDialog("Не заданы настройки FTP");

        }
        if (ftp.ftp_cahrset != null && ftp.ftp_cahrset.toUpperCase().contains("UTF")) {
            isUTF = true;
        } else {
            isUTF = false;
        }
    }


    public void send(Map<String, CacheMetaDateFrontol> dateFrontolMap, IAction refrash) {
        this.dateFrontolMap = dateFrontolMap;
        this.refrash = refrash;
        new inner().execute(null);
    }

    public class inner extends AsyncTask2<String, Integer, Boolean> {

        ProgressBar bar;

        @Override
        public void onPreExecute() {
            Controller.WriteMessage( support.str ("name140"));
            FactoryBlender.Run();
        }

        @Override
        public Boolean doInBackground(String... params) {
            return activate();
        }


        @Override
        public void onPostExecute(Boolean params) {
            FactoryBlender.stop();
            if (params && error.length() == 0) {
                Controller.WriteMessage(support.str ( "name141" ));
                if (refrash != null) {
                    refrash.action(null);
                }
                // new Sender_1C_rewrite().Send();
            } else {
                DialogFactory.ErrorDialog(error.toString());
                Controller.WriteMessage(support.str ( "name142" ));
            }
            Controller.Ccmainform.watcherScaner.refrashList ();

        }

        boolean activate() {

            List<String> totalList = new ArrayList<>();




            if (SettingAppE.instance.getProfile() == 1) {
                senderBitnic(totalList);
            } else {
                ftp(totalList);
            }


            CacheMetaDateFrontol currentFrontol = null;

            List<Object> objects = new ArrayList<>();
            Set<String> set = new HashSet<>();
            log.info("totallist size - " +totalList.size());
            for (int i = 2; i < totalList.size(); i++) {


                String s = totalList.get(i);

                if (s.length() == 0 || s.trim().length() == 0) {
                    continue;
                }


                if (s.indexOf(";") == -1 && s.indexOf("$") == 0) {


                    if (objects.size() > 0) {
                        ISession ses = Configure.GetSession();
                        ses.beginTransaction();

                        if (currentFrontol.aClass == MCommandDELETEALLMARKETINGEVENTSBYACTIONCODE.class) {

                            List<MMarketingAction> mMarketingActions = ses.getList(MMarketingAction.class, null);
                            List<Integer> list = new ArrayList<>();
                            for (Object object : objects) {
                                MCommandDELETEALLMARKETINGEVENTSBYACTIONCODE d = (MCommandDELETEALLMARKETINGEVENTSBYACTIONCODE) object;
                                list.add(d.idaction);
                            }
                            List<MMarketingAction> delList = new ArrayList<>();
                            for (MMarketingAction ma : mMarketingActions) {
                                if (list.contains(ma.code)) {
                                    delList.add(ma);
                                }
                            }
                            for (MMarketingAction mMarketingAction : delList) {
                                ses.delete(mMarketingAction);
                            }

                        } else {
                            Configure.Bulk(currentFrontol.aClass, objects, ses);
                        }


                        ses.commitTransaction();
                        objects = new ArrayList<>();
                        set.clear();
                    }


                    String ss = s.substring(3);
                    currentFrontol = dateFrontolMap.get(ss);

                    Object sd = null;
                    if (currentFrontol == null) {
                        continue;
                    }
                    try {
                        sd = currentFrontol.aClass.newInstance();
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (sd instanceof IFrontolAction) {
                        ((IFrontolAction) sd).action();
                    }
                } else {

                    Object sd;
                    try {
                        if (currentFrontol != null && currentFrontol.aClass != null) {
                            sd = currentFrontol.aClass.newInstance();
                            if (sd != null) {
                                CreatorInstace.Create(sd, currentFrontol, totalList.get(i));
                                if (sd instanceof IInsertValidator) {
                                    IInsertValidator validator = (IInsertValidator) sd;
                                    String key = validator.getHashKey();
                                    if (set.contains(key)) {

                                    } else {
                                        objects.add(sd);
                                        set.add(key);
                                    }
                                } else {
                                    objects.add(sd);
                                }
                            }
                        }


                    } catch (Exception e) {
                        String dd = totalList.get(i);
                        log.error(e);
                        e.printStackTrace();
                        return false;
                    }

                }


            }
            if (objects.size() > 0) {
                ISession ses = Configure.GetSession();
                ses.beginTransaction();
                FactoryBlender.AddMessage("Вставка : " + currentFrontol.aClass.getName());
                Configure.Bulk(currentFrontol.aClass, objects, ses);
                ses.commitTransaction();

            }
            FactoryBlender.AddMessage("Успех заргузки");
            return true;

        }

    }

    private void senderBitnic(List<String> totalList) {


        HttpsURLConnection conection = null;
        int count = 0;
        try {
            String ss="https://" + SettingAppE.instance.getUrl() + "/pos_goods?point=" + SettingAppE.instance.getPointId();
            log.info("dowload goods.txt - "+ss);
            URL url = new URL(ss);
            conection = (HttpsURLConnection) url.openConnection();
            conection.setInstanceFollowRedirects(false);
            conection.setReadTimeout(15000 /*milliseconds*/);
            conection.setConnectTimeout(20000 /* milliseconds */);

            conection.setRequestMethod("GET");
            conection.connect();
            final int status = conection.getResponseCode();
            if (status != 200) {
                error.append("Запрос goods, ответ сервера " + status);
            }
            log.info("status - "+status);
            int lenghtOfFile = conection.getContentLength() + 100;
            InputStream input = conection.getInputStream();
            OutputStream output = new FileOutputStream(Pather.inFilesData);
            byte data[] = new byte[lenghtOfFile];
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();


        } catch (Exception ex) {
            error.append(ex.getMessage());
            log.error(ex);
        } finally {
            if (conection != null) {
                conection.disconnect();
            }
        }
        if (error.length() > 0) {
            return;
        }

        BufferedReader br = null;
        FileReader fr = null;


        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(Pather.inFilesData), "windows-1251"));
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                totalList.add(sCurrentLine);
            }

        } catch (IOException e) {
            log.error(e);
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                log.error(ex);
            }
        }

        if (totalList.size() == 0) {
            error.append(" Ошибка, отсутствие данных в файле");
        }

    }

    private boolean ftp(List<String> totalList) {
        FactoryBlender.AddMessage("Подготовка к передаче");
        FTPClient ftpClient = new FTPClient();
        try {

            log.info("sender ftp "+ftp.getUrl());
            ftpClient.connect(ftp.getUrl(), ftp.ftp_port);
            ftpClient.login(ftp.ftp_user, ftp.ftp_password);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(new File(Pather.inFilesData)));
            String fileftp = ftp.getPointId() + "/" + ftp.ftp_remote_file;
            System.out.println("ftp - " + fileftp);
            success = ftpClient.retrieveFile(fileftp, outputStream1);
            outputStream1.close();

            log.info("sender ftp success "+success);
            if (success) {

                String ss = "Успешная загрузка файла ftp: " + ftp.ftp_remote_file;
                FactoryBlender.AddMessage(ss);
                log.info(ss);
                System.out.println(ss);

            } else {
                String er = "Ошибка загрузки файла ftp: " + ftp.ftp_remote_file;
                FactoryBlender.AddMessage(er);
                System.out.println(er);
                log.info(er);
                DialogFactory.ErrorDialog(er);
            }

        } catch (IOException ex) {
            log.error(ex);
            error.append(ex.getMessage());
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
            DialogFactory.ErrorDialog(ex);
            return true;
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                log.error(ex);
                ex.printStackTrace();
                DialogFactory.ErrorDialog(ex);
            }
        }

        BufferedReader br = null;
        FileReader fr = null;


        try {

            if (isUTF) {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(Pather.inFilesData)));
            } else {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(Pather.inFilesData), "windows-1251"));
            }


            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                if (isUTF) {

                } else {
                    sCurrentLine = new String(sCurrentLine.getBytes(), 0, sCurrentLine.getBytes().length, "UTF-8");
                }

                totalList.add(sCurrentLine);

            }

        } catch (IOException e) {
            error.append("\r\n");
            error.append(e.getMessage());
            e.printStackTrace();
            log.error(e);
            DialogFactory.ErrorDialog(e);
            return true;
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                log.error(ex);
                ex.printStackTrace();
                DialogFactory.ErrorDialog(ex);
            }
        }

        if (totalList.size() == 0) {
            String s = "код предприятия - " + ftp.getPointId() + System.lineSeparator() +
                    "файл на ftp - " + ftp.ftp_remote_file + System.lineSeparator() +
                    "файл на локали- " + Pather.inFilesData + System.lineSeparator() +
                    "успешность запроса - " + String.valueOf(success);
            error.append(new RuntimeException(s));
            return true;
        }

        FactoryBlender.AddMessage("Получено : " + totalList.size() + " строк");
        FactoryBlender.AddMessage(" Определено : " + dateFrontolMap.size() + " типов");
        return false;
    }


}
