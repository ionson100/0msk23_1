package bitnic.utils;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import bitnic.model.MBarcodes;
import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.pagesale.ICleatTextField;
import bitnic.pagesale.stacksearcher.FStackSearcher;
import bitnic.pagetableproduct.Finder;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class WatcherScaner {

    private List<Field> fields = new ArrayList<>();
    private List<MProduct> curlist;
    private StackPane stackPane;
    private TextField textField;
    private FStackSearcher searcher;
    private IAction addIAction;

    public void refrashList(){
        curlist = Configure.GetSession().getList(MProduct.class, " isGroup <> 0 ");
    }

    public WatcherScaner(StackPane stackPane, IAction addIAction) {
        this.addIAction = addIAction;

        curlist = Configure.GetSession().getList(MProduct.class, " isGroup <> 0 ");
        Collections.sort(curlist, Comparator.comparing(o -> o.name));
        this.stackPane = stackPane;
        for (Field field : MProduct.class.getFields()) {

            Finder f = field.getAnnotation(Finder.class);
            if (f != null) {
                if(field.getName().equals("name")){
                    if(SettingAppE.instance.isSearchProductName){
                        fields.add(field);
                    }
                }else {
                    fields.add(field);
                }

            }
        }
    }

    public void clear() {
        textField.setText("");

    }

    private void close() {
        if (stackPane.getChildren().size() > 1)
            stackPane.getChildren().remove(1);
        searcher = null;
        Controller.WriteMessage("В ожидании ввода");
    }

    long first = 0, stop = 0;
    private boolean isScaner = true;

    public void run(TextField textField) {
        this.textField = textField;

        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (textField.getText().trim().length() > 0) {
                close();

                if (textField.getText().trim().length() == 1) {
                    first = System.nanoTime();
                    isScaner = true;
                }
                if (textField.getText().trim().length() == 2) {
                    stop = System.nanoTime();
                    isScaner = (stop - first) < SettingAppE.instance.scanerDelau;
                }
                if (isScaner) return;


                searcher = new FStackSearcher(addIAction);
                searcher.setPrefHeight(stackPane.getHeight());
                searcher.setPrefWidth(stackPane.getWidth());
                stackPane.getChildren().add(searcher);
                List<MProduct> productList = new ArrayList<>();
                if (textField.getText().trim().length() >= 6 && isNimberAll(textField.getText().trim())) {
                    getListProductBarcodesContext(textField.getText(), productList, curlist);
                } else {
                    for (MProduct mProduct : curlist) {
                        for (Field field : fields) {
                            String f = null;
                            try {
                                f = field.get(mProduct).toString();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                                System.out.println(field.getName());
                            }
                            if (f.toUpperCase().contains(newValue.toUpperCase())) {
                                productList.add(mProduct);
                            }
                        }
                    }
                }
                if (searcher != null) {
                    searcher.setSource(productList);
                }

            } else {
                isScaner = true;
                close();
            }
        });

        textField.setOnKeyPressed(event -> {
            String text = event.getText();
            if (text.equals("\r") && textField.getText().length() > 6) {
                addProduct(textField.getText());
            }
        });
    }


    private void getListProductBarcodesContext(String text, List<MProduct> list, List<MProduct> total) {
        List<MBarcodes> mBarcodes = Configure.GetSession().getList(MBarcodes.class, " barcode LIKE '%" + text + "%'");
        for (MBarcodes mBarcode : mBarcodes) {
            if (SettingAppE.instance.isCheckFinishProduct) {
                if (list.size() > 50) {
                    break;
                }
            }
            for (MProduct mProduct : total) {
                if (mProduct.code_prod.equals(mBarcode.id_product)) {
                    list.add(mProduct);
                }
            }
        }

    }

    private boolean isNimberAll(String trim) {
        for (char c : trim.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    public void addProduct(String text) {


        StackPane node = Controller.Instans.stack_panel;
        if (node.getChildren().size() > 1) {
            return;
        }
        if (Controller.Ccmainform == null) {
            return;
        }
        List<MBarcodes> bargodes = Configure.GetSession().getList(MBarcodes.class, " barcode = ? ", text.trim());
        if (bargodes.size() == 0) {
            DialogFactory.ErrorDialog("Barcod  не найден: " + text);
            return;
        }
        MBarcodes b = bargodes.get(0);
        List<MProduct> mProducts = Configure.GetSession().getList(MProduct.class, " id_core = ? ", b.id_product);
        if (mProducts.size() == 0) {
            DialogFactory.ErrorDialog("Продукт с кодом:" + text + " не найден!", new IAction() {
                @Override
                public boolean action(Object o) {
                    if(Controller.Curnode instanceof ICleatTextField){
                        ((ICleatTextField) Controller.Curnode).clearTextField();
                    }
                    return false;
                }
            });
            return;
        } else {
            MProduct pr = mProducts.get(0);
             if(pr.price==0d){
                 DialogFactory.ErrorDialog("Продукт : " + pr.name_check + " имеет нулевую цену, исключен из продажи !! ", new IAction() {
                     @Override
                     public boolean action(Object o) {
                         if(Controller.Curnode instanceof ICleatTextField){
                             ((ICleatTextField) Controller.Curnode).clearTextField();
                         }
                         return false;
                     }
                 });
                 return;
             }
            boolean es = false;
            for (MProduct mProduct : SettingAppE.instance.selectProduct) {
                if (mProduct.code_prod.equals(pr.code_prod)) {
                    mProduct.selectAmount = mProduct.selectAmount + 1;
                    es = true;
                }
            }
            if (es == false) {
                SettingAppE.instance.selectProduct.add(pr);
                pr.selectAmount = 1;
            }
        }
        SettingAppE.save();
        Controller.Ccmainform.refrachCheck();
    }
}
