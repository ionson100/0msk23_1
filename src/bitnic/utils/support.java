package bitnic.utils;

import org.jetbrains.annotations.PropertyKey;
import org.jetbrains.annotations.NonNls;
import java.util.ResourceBundle;
import java.text.MessageFormat;
public class support {
    @NonNls
    private static final ResourceBundle bundle = ResourceBundle.getBundle ("aa");
    public static String str ( @PropertyKey(resourceBundle ="aa") String key, Object... params) {
        String value =bundle.getString(key);
        if (params.length >0) return MessageFormat.format(value, params);
        return value;
    }
}