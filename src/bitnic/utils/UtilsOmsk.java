package bitnic.utils;

import bitnic.core.Controller;
import bitnic.settingscore.SettingAppE;
import javafx.css.Styleable;
import org.apache.commons.lang3.SystemUtils;
import com.google.gson.*;
import bitnic.pagetableproduct.IFocusable;
import bitnic.dialogfactory.DialogFactory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;


import bitnic.transaction.eventfrontol.EventBaseE;
import org.slf4j.Logger;

import java.io.*;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

public class UtilsOmsk {

    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UtilsOmsk.class);
    private static final String nodate = "Данные отсутствуют";
    public static double deltaScrol=5;


    public static <T> T getChildByID(Parent parent, String id) {

        String nodeId = null;

        if (parent instanceof TitledPane) {
            TitledPane titledPane = (TitledPane) parent;
            Node content = titledPane.getContent();
            nodeId = content.idProperty().get();

            if (nodeId != null && nodeId.equals(id)) {
                return (T) content;
            }

            if (content instanceof Parent) {
                T child = getChildByID((Parent) content, id);

                if (child != null) {
                    return child;
                }
            }
        }

        for (Node node : parent.getChildrenUnmodifiable()) {
            nodeId = node.idProperty().get();
            if (nodeId != null && nodeId.equals(id)) {
                return (T) node;
            }

            if (node instanceof SplitPane) {
                SplitPane splitPane = (SplitPane) node;
                for (Node itemNode : splitPane.getItems()) {
                    nodeId = itemNode.idProperty().get();

                    if (nodeId != null && nodeId.equals(id)) {
                        return (T) itemNode;
                    }

                    if (itemNode instanceof Parent) {
                        T child = getChildByID((Parent) itemNode, id);

                        if (child != null) {
                            return child;
                        }
                    }
                }
            } else if (node instanceof Accordion) {
                Accordion accordion = (Accordion) node;
                for (TitledPane titledPane : accordion.getPanes()) {
                    nodeId = titledPane.idProperty().get();

                    if (nodeId != null && nodeId.equals(id)) {
                        return (T) titledPane;
                    }

                    T child = getChildByID(titledPane, id);

                    if (child != null) {
                        return child;
                    }
                }
            } else if (node instanceof Parent) {
                T child = getChildByID((Parent) node, id);

                if (child != null) {
                    return child;
                }
            }
        }
        return null;
    }

    public static void openKeyBoard() {
        if(SystemUtils.IS_OS_LINUX){
            try{
                Process proc = Runtime.getRuntime().exec("onboard");
            }catch (Exception ex){
                ex.printStackTrace();
            }
           if(Controller.Curnode instanceof IFocusable){
               ((IFocusable) Controller.Curnode).focus();
           }
        }
    }

    public static String getKey2() {

        final String spath = "C:\\Windows\\storma";

        String res = null;
        File f = new File(spath);
        if (f.exists() == false) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                DialogFactory.ErrorDialog(e);
            }

            StringBuilder sb = new StringBuilder();
            res = UUID.randomUUID().toString();
            sb.append(res).append("\r\n");
            sb.append(UUID.randomUUID().toString()).append("\r\n");
            sb.append(UUID.randomUUID().toString()).append("\r\n");
            sb.append(UUID.randomUUID().toString()).append("\r\n");
            sb.append(UUID.randomUUID().toString()).append("\r\n");
            sb.append(UUID.randomUUID().toString()).append("\r\n");
            sb.append(UUID.randomUUID().toString());
            try {
                writeToFile(spath, sb.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return res;
        }
        List<String> lines = new ArrayList<>();
        try {
            lines = Files.readAllLines(Paths.get(spath), Charset.forName("utf8"));
        } catch (IOException e) {
            e.printStackTrace();
            DialogFactory.ErrorDialog(e);
        }
        if (lines.size() == 0) {
            return "";
        } else {
            return lines.get(0);
        }
    }

    public static String getMyKey() {

        final String spath = "C:\\Windows\\stormaticus";
        File field = new File(spath);
        if (field.exists() == false) {
            return "";
        }
        List<String> lines = new ArrayList<>();
        try {
            lines = Files.readAllLines(Paths.get(spath), Charset.forName("utf8"));
        } catch (IOException e) {
            e.printStackTrace();
            DialogFactory.ErrorDialog(e);
        }
        if (lines.size() == 0) {
            return "";
        } else {
            return lines.get(0);
        }
    }

    public static void writeToFile(String patch, String content) throws IOException {

        BufferedWriter bw = null;
        FileWriter fw = null;
        try {
            fw = new FileWriter(patch);
            bw = new BufferedWriter(fw);
            bw.write(content);

        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                DialogFactory.ErrorDialog(ex);
                ex.printStackTrace();
            }

        }
    }

    public static Gson getGson() {

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, (JsonDeserializer<Date>) (json, typeOfT, context) -> {

            String ss = json.getAsJsonPrimitive().getAsString();
            if (ss.equals("null")) {
                return null;
            }else {
                long dd = json.getAsJsonPrimitive().getAsLong();
                return new Date(dd);
            }
        });

        builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
            @Override
            public JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {

                if (date == null) {
                    return new JsonPrimitive("null");
                }
                int dd = (int) (date.getTime() / 1000);
                return new JsonPrimitive(dd);
            }
        });

        return builder.serializeNulls().create();
    }

    public static String simpleDateFormatE(Date date) {

        if (date == null) {
            return nodate;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return dateFormat.format(date);
    }

    public static String simpleDateFormatforFileNameStory(Date date) {

        if (date == null) {
            return nodate;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        return dateFormat.format(date);
    }


    public static String simpleDateFormatForCommetsE(Date date) {
        if (date == null) {
            return nodate;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(date);
    }

    public static void copyDirectoryOneLocationToAnotherLocation(File sourceLocation, File targetLocation)
            throws IOException {

        try (InputStream in = new FileInputStream(sourceLocation)) {
            try (OutputStream out = new FileOutputStream(targetLocation)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }

    }

    public static Date curDate() {
        return Calendar.getInstance().getTime();
    }


    public void getDialogProgressBar(Alert alert) {

        ProgressBar progressBar = new ProgressBar();
        GridPane.setVgrow(progressBar, Priority.ALWAYS);
        GridPane.setHgrow(progressBar, Priority.ALWAYS);
        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(progressBar, 0, 0);
        alert.getDialogPane().setExpandableContent(expContent);
        alert.showAndWait();

    }


    public static void resizeNode(GridPane thise, GridPane inject) {

        thise.widthProperty().addListener((observableValue, oldSceneWidth, newSceneWidth) -> {
            System.out.println("Width: " + newSceneWidth);
            inject.setPrefWidth((Double) newSceneWidth);
            //Здесь мы можем отправить к любому нашему объекту новое значение Width
        });


        thise.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue observableValue, Number oldSceneHeight, Number newSceneHeight) {
                System.out.println("Height: " + newSceneHeight);
                inject.setPrefHeight((Double) newSceneHeight);
                //Здесь мы можем отправить к любому нашему объекту новое значение Height
            }
        });

    }


    public static void serializer(Object o, String path) {
        FileOutputStream fileOut = null;
        ObjectOutputStream out = null;
        File f = new File(path);

        try {
            fileOut = new FileOutputStream(path);
            out = new ObjectOutputStream(fileOut);
            out.writeObject(o);
        } catch (IOException e) {
            throw new RuntimeException("reanimator:" + e.getMessage());
        } finally {
            if (fileOut != null) {
                try {
                    fileOut.close();
                } catch (IOException ex) {
                    DialogFactory.ErrorDialog(ex);
                    ex.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    DialogFactory.ErrorDialog(ex);
                    ex.printStackTrace();
                }
            }
        }
    }


    public static Object deserializer(String path) throws ClassNotFoundException {
        File f = new File(path);
        if (f.exists()) {
            Object e;
            FileInputStream fileIn = null;
            ObjectInputStream in = null;
            try {
                try {
                    fileIn = new FileInputStream(path);
                } catch (FileNotFoundException e1) {
                    throw new RuntimeException("Setting FileNotFoundException:" + e1.getMessage());
                }
                in = new ObjectInputStream(fileIn);
                e = in.readObject();
                return e;
            } catch (IOException i) {
                i.printStackTrace();
                return null;
            } finally {
                if (fileIn != null) {
                    try {
                        fileIn.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
        return null;
    }



    private static Object lock=new Object();






    public static void appenderReportFile(List<EventBaseE> list) {

//        List<String> strings=new ArrayList<>(list.size());
        StringBuilder sb=new StringBuilder();
        for (EventBaseE eventBaseE : list) {
            if(eventBaseE==null) continue;
            try {
                String s=eventBaseE.join();
                sb.append(s);
                sb.append(System.lineSeparator());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        try {

            File f=new File(Pather.sender_report_file);
            if(f.exists()==false){
                f.createNewFile();
            }

            Files.write(Paths.get(Pather.sender_report_file), sb.toString().getBytes(), StandardOpenOption.APPEND);

        }catch (IOException e) {
            e.printStackTrace();
            DialogFactory.ErrorDialog(e);
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static Date getDateKKM(Date date, Date time) {


        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(time);
        int hour = cal1.get(Calendar.HOUR_OF_DAY);
        int min = cal1.get(Calendar.MINUTE);

        Calendar cal2 = Calendar.getInstance();
        cal2.set(Calendar.YEAR, year);
        cal2.set(Calendar.MONTH, month);
        cal2.set(Calendar.DAY_OF_MONTH, day);
        cal2.set(Calendar.HOUR_OF_DAY, hour);
        cal2.set(Calendar.MINUTE, min);
        Date datee = cal2.getTime();

        return datee;

    }

    public static String readFile(String filepatch) {

            StringBuilder contentBuilder = new StringBuilder();

            try (Stream<String> stream = Files.lines( Paths.get(filepatch), StandardCharsets.UTF_8))
            {
                stream.forEach(s -> contentBuilder.append(s).append("\n"));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return contentBuilder.toString();

    }

    public static void rewriteFile(String fileName, String s) throws Exception {
        File myFoo = new File(fileName);
        FileOutputStream fooStream = new FileOutputStream(myFoo, false); // true to append
        // false to overwrite.
        byte[] myBytes = s.getBytes();
        fooStream.write(myBytes);
        fooStream.close();
    }

    public static void painter3d(Styleable ... nodes){
        for (Object node : nodes) {
            if(SettingAppE.instance.isTypeshow){
                if(node instanceof TextField){
                    ((TextField)node).getStyleClass().add("delivery_3d");
                }
                if(node instanceof Button){
                    ((Button)node).getStyleClass().add("button_dialog_3d");
                }
            }else {
                if(node instanceof TextField){
                    ((TextField)node).getStyleClass().add("delivery");
                }
                if(node instanceof Button){
                    ((Button)node).getStyleClass().add("button_dialog");
                }
            }

        }
    }


    public static Object getStringTimeForError(Date date) {
        //2018-03-14 09:12:46

        //SimpleDateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);

    }
    public static boolean runWithPrivileges(String command) {
        InputStreamReader input;
        OutputStreamWriter output;

        try {
            //Create the process and start it.
            Process pb = new ProcessBuilder(new String[]{"/bin/bash", "-c", "sudo "+command}).start();
            output = new OutputStreamWriter(pb.getOutputStream());
            input = new InputStreamReader(pb.getInputStream());

            int bytes, tryies = 0;
            char buffer[] = new char[1024];
            while ((bytes = input.read(buffer, 0, 1024)) != -1) {
                if(bytes == 0)
                    continue;
                //Output the data to console, for debug purposes
                String data = String.valueOf(buffer, 0, bytes);
                System.out.println(data);
                // Check for password request
                if (data.contains("[sudo] password")) {
                    // Here you can request the password to user using JOPtionPane or System.console().readPassword();
                    // I'm just hard coding the password, but in real it's not good.
                    char password[] = new char[]{'3','1','2','8','7','3'};
                    output.write(password);
                    output.write('\n');
                    output.flush();
                    // erase password data, to avoid security issues.
                    Arrays.fill(password, '\0');
                    tryies++;
                }
            }

            return tryies < 3;
        } catch (IOException ex) {
            log.error(ex);
        }

        return false;
    }
}
