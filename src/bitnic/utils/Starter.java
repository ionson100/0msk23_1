package bitnic.utils;


import bitnic.core.Main;
import bitnic.dialogfactory.DialogFactory;
import javafx.application.Platform;

import bitnic.orm.Configure;

import org.apache.log4j.Logger;

import javax.net.ssl.*;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.channels.FileLock;
import java.security.cert.X509Certificate;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public class Starter {

    public static   String Pathid = Pather.curdir+File.separator+"pointid"+File.separator+"pointid.txt";
    public static   String Pathprofile = Pather.curdir+File.separator+"profile"+File.separator+"profile.txt";
    private static final Logger log = Logger.getLogger(Starter.class);


    public Starter() {

        System.out.println(System.getProperty("java.library.path"));
    }

    public void start() {

        disableSslVerification();



        {

            String path = Pather.settingsFolder;
            File f = new File(path);
            if (f.exists()) {
            } else {
                if (f.mkdir()) {
                    log.info(path + " settings - Create");
                } else {
                    log.info(path + " Не могу созать");
                }
            }
        }


        boolean isrunung = isFileshipAlreadyRunning();// если false то запущена вторая программа и мы закрываем приложение
        if (isrunung == false) {
            Platform.exit();
        }

        {
            String path = Pather.curdir+File.separator+"pointid";
            File f = new File(path);
            if (f.exists()==false) {
                f.mkdir();
            }

        }
        {

            File f = new File(Pathid);
            if (f.exists()==false) {
                try {
                    f.createNewFile();
                    UtilsOmsk.writeToFile(Pathid,"0");
                } catch (IOException e) {
                    log.error(e);
                }
            }

        }

        {
            String path = Pather.curdir+File.separator+"profile";
            File f = new File(path);
            if (f.exists()==false) {
                f.mkdir();
            }

        }
        {

            File f = new File(Pathprofile);
            if (f.exists()==false) {
                try {
                    f.createNewFile();
                    UtilsOmsk.writeToFile(Pathprofile,"0");
                } catch (IOException e) {
                    log.error(e);
                }
            }

        }


        new Configure(Pather.base_sqlte, "bitnic/model");



        File ff = new File(Pather.directoryBuilder2);
        if (ff.exists()) {
            for (File myFile : ff.listFiles())
                if (myFile.isFile()) myFile.delete();
        }



        {
            File file = new File(Pather.versionFolder);
            if (file.exists() == false) {
                try {
                    file.mkdir();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (file.exists()) {
                try {
                    File file1 = new File(Pather.versionFolder + File.separator + "version.txt");
                    if (file1.exists() == false) {
                        file1.createNewFile();
                    }
                    String version = GetStringVersion();

                    System.out.println("Implementation-Version: " + version);
                    FileOutputStream fooStream = new FileOutputStream(file1, false); // true to append// false to overwrite.

                    byte[] myBytes = version.getBytes();
                    fooStream.write(myBytes);
                    fooStream.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static String GetStringVersion() throws IOException {
        String classPath = Main.class.getResource(Main.class.getSimpleName() + ".class").toString();
        String libPath = classPath.substring(0, classPath.lastIndexOf("bitnic/core"));
        String filePath = libPath + "META-INF/MANIFEST.MF";
        System.out.println("File:  " + filePath);
        Manifest manifest = null;
        manifest = new Manifest(new URL(filePath).openStream());
        Attributes attr = manifest.getMainAttributes();
        return attr.getValue("Implementation-Version");
    }

    private static boolean isFileshipAlreadyRunning() {

        try {
            File file = new File(Pather.fileShipReserved);
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            FileLock fileLock = randomAccessFile.getChannel().tryLock();
            if (fileLock != null) {
                Runtime.getRuntime().addShutdownHook(new Thread() {
                    public void run() {
                        try {
                            fileLock.release();
                            randomAccessFile.close();
                            file.delete();
                        } catch (Exception e) {
                            DialogFactory.ErrorDialog(e);
                            log.error("Unable to remove lock file: " + Pather.fileShipReserved);
                        }
                    }
                });
                return true;
            }
        } catch (Exception e) {

            log.error("Unable to Create and/or lock file: " + Pather.fileShipReserved);
        }
        return false;
    }

    // добавление путей для java
    public static void AddDir(String s) throws IOException {
        try {
            Field field = ClassLoader.class.getDeclaredField("usr_paths");
            field.setAccessible(true);
            String[] paths = (String[]) field.get(null);
            for (int i = 0; i < paths.length; i++) {
                if (s.toUpperCase().equals(paths[i].toUpperCase())) {
                    return;
                }
            }
            String[] tmp = new String[paths.length + 1];
            System.arraycopy(paths, 0, tmp, 0, paths.length);
            tmp[paths.length] = s;
            field.set(null, tmp);
            String sd = System.getProperty("java.library.path") + File.pathSeparator + s;
            System.setProperty("java.library.path", sd);
            log.info(sd + " dll загружена");
        } catch (IllegalAccessException e) {
            log.info(s + " проблемы заргузки ddl Failed to Get permissions to set library path");
            throw new IOException("Failed to Get permissions to set library path");
        } catch (NoSuchFieldException e) {
            log.info(s + " проблемы заргузки ddl Failed to Get field handle to set library path");
            throw new IOException("Failed to Get field handle to set library path");
        }
    }


    private static void disableSslVerification() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (Exception e) {
            e.printStackTrace();
            DialogFactory.ErrorDialog(e);
        }
    }


}
