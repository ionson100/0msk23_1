package bitnic.utils;


import bitnic.dialogfactory.DialogFactory;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static bitnic.utils.UtilsSettings.recursion;

public class FactorySettings {

    public static <T> T GetSettingsE(Class<T> aClass) {
        T res = null;
        try {
            res = (T) UtilsSettings.deserializerE(aClass);
        } catch (Exception e) {
            e.printStackTrace();
            DialogFactory.ErrorDialog(e);
        }
        if (res == null) {
            throw new RuntimeException(" не могу получить настройки: " + aClass.getName());
        }
        return res;
    }

    public static <T> void saveE(T t) {

        try {

            UtilsSettings.serializerE(t,null);
            List<Class> classes=new ArrayList<>();
            recursion(classes,t.getClass());
            for (Class aClass : classes) {
                UtilsSettings.serializerE(t,aClass);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }






}
