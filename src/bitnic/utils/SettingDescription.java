package bitnic.utils;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface SettingDescription {
    int Index() default 0;
    int TypeControl() default 0;
    String name() default "";
    String description() default "";
    int  type() default  1;
}
