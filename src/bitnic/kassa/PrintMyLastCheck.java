package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MCheck;
import bitnic.model.MProduct;
import bitnic.senders.AsyncTask2;
import bitnic.utils.UtilsOmsk;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.Comparator;

public class PrintMyLastCheck extends BaseKassa {
    private static final Logger log = Logger.getLogger(PrintMyLastCheck.class);
    public static void PrintLast(MCheck mCheck) {
        new AsyncTask2<Void, Void, Void>() {
            private IFptr fptr;
            Exception exception;
            private void printText(String text, int alignment, int wrap) throws Exception {
                if (fptr.put_Caption(text) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_TextWrap(wrap) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Alignment(alignment) < 0) {
                    checkError(fptr);
                }
                if (fptr.PrintString() < 0) {
                    checkError(fptr);
                }
            }

            private void printTextItogo(String text, int alignment, int wrap) throws Exception {


                if (fptr.put_FontDblHeight(true) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_FontDblWidth(true) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_FontBold(true) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Caption(text) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_TextWrap(wrap) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Alignment(alignment) < 0) {
                    checkError(fptr);
                }
                fptr.AddTextField();

                fptr.PrintFormattedText();

            }

            @Override
            protected Void doInBackground(Void... params) {

                fptr = new Fptr();
                try {
                    fptr.create();
                    SetWorkDir(fptr);

                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError(fptr);
                    }

                    if (fptr.GetStatus() < 0) {
                        checkError(fptr);
                    }

                    try {
                        if (fptr.CancelCheck() < 0) {
                            checkError(fptr);
                        }
                    } catch (Exception e) {
                        int rc = fptr.get_ResultCode();
                        if (rc != -16 && rc != -3801) {
                            throw e;
                        }
                    }

                    if (fptr.put_UserPassword("00000030") < 0) {
                        checkError(fptr);
                    }
                    printText("ДУБЛИКАТ ДОКУМЕНТА", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                    printText("КАССОВЫЙ ЧЕК", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                    printText("ПРИХОД", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);


                    Collections.sort(mCheck.mProducts, Comparator.comparing(lhs -> lhs.name_check));
                    double total = 0;
                    double totalNds = 0;
                    for (MProduct product : mCheck.mProducts) {
                        printText(product.name_check, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText(String.valueOf(product.selectAmount) + "х" + String.valueOf(product.price)
                                        + " =" + String.valueOf(UtilsOmsk.round(product.selectAmount * product.price, 2)),
                                IFptr.ALIGNMENT_RIGHT, IFptr.WRAP_WORD);
                        total = total + product.selectAmount * product.price;
                        double nds = ((product.selectAmount * product.price) / 100) * 18;
                        totalNds = totalNds + nds;
                        printText(" Сумма НДС 18%      =" + String.valueOf(UtilsOmsk.round(nds, 2)),
                                IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                        printText("", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    }
                    String tota = String.valueOf(UtilsOmsk.round(total, 2));
                    printTextItogo("ИТОГ =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("ОПЛАТА", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(" НАЛИЧНЫМИ          =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("ВСЕГО ОПЛАЧЕНО", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("НАЛИЧНЫМИ           =" + tota, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("ЭЛЕКТРОННЫМИ        =0.00", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("Сумма НДС           =" + String.valueOf(UtilsOmsk.round(totalNds, 2)),
                            IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("Кассир:" + SettingAppE.instance.getUserName(), IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("Торговая точка:", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    //ss = " не определена";
                    // MPoint dd = Configure.GetSession().Get(MPoint.class, tempCheck.mVisitStory.point_id);
                    // if (dd != null) {
                    //     ss = dd.name;
                    //  }
                    // printText(" " + ss, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText(mCheck.date, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("Номер чека:" + mCheck.id, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("Номер  KKM:" + mCheck.kmmNumber, IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                    printText("", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);


                } catch (Exception e) {
                    log.error(e);
                    exception = e;
                    fptr.ResetMode();
                    fptr.destroy();
                } finally {
                    fptr.destroy();
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                FactoryBlender.Run();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                FactoryBlender.stop();
                if (exception != null) {
                    DialogFactory.ErrorDialog(exception);
                }


            }


        }.execute(null);


    }
}
