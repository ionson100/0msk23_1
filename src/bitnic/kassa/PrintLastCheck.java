package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask2;
import org.apache.log4j.Logger;

import java.io.IOException;

public class PrintLastCheck extends BaseKassa {

    private static final Logger log = Logger.getLogger(PrintLastCheck.class);
    public void print(){
        new MyWorker().execute(null);
    }
    private class MyWorker extends AsyncTask2<Void, Void, Boolean> {

        @Override
        public Boolean doInBackground(Void... params)  {
            IFptr fptr = new Fptr();
            try {
                fptr.create();
                SetWorkDir(fptr);

                if (fptr.put_DeviceEnabled(true) < 0) {
                    checkError(fptr);
                }

                if (fptr.GetStatus() < 0) {
                    checkError(fptr);
                }

                try {
                    if (fptr.CancelCheck() < 0) {
                        checkError(fptr);
                    }
                } catch (Exception e) {
                    int rc = fptr.get_ResultCode();
                    if (rc != -16 && rc != -3801) {
                        throw e;
                    }
                }

                if (fptr.PrintLastCheckCopy() < 0) {
                    checkError(fptr);
                }
                fptr.destroy();

                return true;

            } catch (Exception e) {
                log.error(e);
                exception =e;
                e.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() throws IOException {

            Controller.WriteMessage("Печать последнего чека");
            FactoryBlender.Run();
        }

        @Override
        public void onPostExecute(Boolean params) {

            FactoryBlender.stop();
            if(exception!=null){
                DialogFactory.ErrorDialog(exception);
            }else {
                Controller.WriteMessage("");
            }
        }
    }
}

