package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask2;
import org.apache.log4j.Logger;

import java.math.BigDecimal;

public class PrintTestCheck extends BaseKassa {

    private static final Logger log = Logger.getLogger(PrintTestCheck.class);
    private static Exception exception;
    private static void registration(IFptr fptr, String name, double price, double quantity) throws DriverException {
        if (fptr.put_Quantity(quantity) < 0)
            checkError(fptr);
        if (fptr.put_Price(price) < 0)
            checkError(fptr);
        if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0)
            checkError(fptr);
        if (fptr.put_Name(name) < 0)
            checkError(fptr);
        if (fptr.Registration() < 0)
            checkError(fptr);
    }

    private static void registrationFZ54(IFptr fptr, String name, double price, double quantity,
                                         double positionSum, int taxNumber) throws DriverException {
        if (fptr.put_PositionSum(positionSum) < 0)
            checkError(fptr);
        if (fptr.put_Quantity(quantity) < 0)
            checkError(fptr);
        if (fptr.put_Price(price) < 0)
            checkError(fptr);
        if (fptr.put_TaxNumber(taxNumber) < 0)
            checkError(fptr);
        if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0)
            checkError(fptr);
        if (fptr.put_Name(name) < 0)
            checkError(fptr);
        if (fptr.Registration() < 0)
            checkError(fptr);
    }

    private static void payment(IFptr fptr, double sum, int type) throws DriverException {
        if (fptr.put_Summ(sum) < 0)
            checkError(fptr);
        if (fptr.put_TypeClose(type) < 0)
            checkError(fptr);
        if (fptr.Payment() < 0)
            checkError(fptr);
        System.out.println(String.format("Remainder: %.2f, Change: %.2f", fptr.get_Remainder(), fptr.get_Change()));
    }

    static void reportZ(IFptr fptr) throws DriverException {
        if (fptr.put_Mode(IFptr.MODE_REPORT_CLEAR) < 0)
            checkError(fptr);
        if (fptr.SetMode() < 0)
            checkError(fptr);
        if (fptr.put_ReportType(IFptr.REPORT_Z) < 0)
            checkError(fptr);
        if (fptr.Report() < 0)
            checkError(fptr);
    }

    private static void printFooter(IFptr fptr) throws DriverException {
        if (fptr.put_Mode(IFptr.MODE_REPORT_NO_CLEAR) < 0)
            checkError(fptr);
        if (fptr.SetMode() < 0)
            checkError(fptr);
        if (fptr.PrintFooter() < 0)
            checkError(fptr);
    }

    private static void printBarcode(IFptr fptr, int type, String barcode, double scale) throws DriverException {
        if (fptr.put_Alignment(IFptr.ALIGNMENT_CENTER) < 0)
            checkError(fptr);
        if (fptr.put_BarcodeType(type) < 0)
            checkError(fptr);
        if (fptr.put_Barcode(barcode) < 0)
            checkError(fptr);
        if (fptr.put_Height(0) < 0)
            checkError(fptr);
        if (fptr.put_BarcodeVersion(0) < 0)
            checkError(fptr);
        if (fptr.put_BarcodePrintType(IFptr.BARCODE_PRINTTYPE_AUTO) < 0)
            checkError(fptr);
        if (fptr.put_PrintBarcodeText(false) < 0)
            checkError(fptr);
        if (fptr.put_BarcodeControlCode(true) < 0)
            checkError(fptr);
        if (fptr.put_Scale(scale) < 0)
            checkError(fptr);
        if (fptr.put_BarcodeCorrection(0) < 0)
            checkError(fptr);
        if (fptr.put_BarcodeColumns(3) < 0)
            checkError(fptr);
        if (fptr.put_BarcodeRows(1) < 0)
            checkError(fptr);
        if (fptr.put_BarcodeProportions(50) < 0)
            checkError(fptr);
        if (fptr.put_BarcodeUseProportions(true) < 0)
            checkError(fptr);
        if (fptr.put_BarcodePackingMode(IFptr.BARCODE_PDF417_PACKING_MODE_DEFAULT) < 0)
            checkError(fptr);
        if (fptr.put_BarcodePixelProportions(300) < 0)
            checkError(fptr);
        if (fptr.PrintBarcode() < 0)
            checkError(fptr);
    }

    private static void discount(IFptr fptr, double sum, int type, int destination) throws DriverException {
        if (fptr.put_Summ(sum) < 0)
            checkError(fptr);
        if (fptr.put_DiscountType(type) < 0)
            checkError(fptr);
        if (fptr.put_Destination(destination) < 0)
            checkError(fptr);
        if (fptr.Discount() < 0)
            checkError(fptr);
    }

    private static void charge(IFptr fptr, double sum, int type, int destination) throws DriverException {
        if (fptr.put_Summ(sum) < 0)
            checkError(fptr);
        if (fptr.put_DiscountType(type) < 0)
            checkError(fptr);
        if (fptr.put_Destination(destination) < 0)
            checkError(fptr);
        if (fptr.Charge() < 0)
            checkError(fptr);
    }

    public static boolean InnerPrint() {

        IFptr fptr = null;

        try {
            fptr = new Fptr();
            fptr.create();

            SetWorkDir(fptr);



            if (fptr.put_DeviceEnabled(true) < 0)
                checkError(fptr);


            if (fptr.GetStatus() < 0)
                checkError(fptr);

            try {
                if (fptr.CancelCheck() < 0)
                    checkError(fptr);
            } catch (DriverException e) {
                int rc = fptr.get_ResultCode();
                if (rc != -16 && rc != -3801)
                    throw e;
            }

            if (PRINT_FISCAL_CHECK) {

                try {
                    openCheck(fptr, IFptr.CHEQUE_TYPE_SELL);
                } catch (DriverException e) {

                    if (fptr.get_ResultCode() == -3822) {
                        reportZ(fptr);
                        openCheck(fptr, IFptr.CHEQUE_TYPE_SELL);
                    } else {
                        throw e;
                    }
                }

                BigDecimal sum = new BigDecimal(0);
                for (int i = -2; i < 5; ++i) {
                    if (USE_FZ54) {
                        double price = Math.pow(10, i), quantity = 1;
                        registrationFZ54(fptr, String.format("тестовый чек %d", i + 3), price, quantity, price * quantity, IFptr.TAX_VAT_18);
                        sum = sum.add(new BigDecimal(price).multiply(new BigDecimal(quantity)));
                    } else {
                        double price = Math.pow(10, i), quantity = 1;
                        registration(fptr, String.format("регистрация %d", i + 3), price, quantity);
                        sum = sum.add(new BigDecimal(price).multiply(new BigDecimal(quantity)));

                        if (i % 2 == 0) {
                            discount(fptr, 1, IFptr.DISCOUNT_SUMM, IFptr.DESTINATION_POSITION);
                        } else {
                            charge(fptr, 1, IFptr.DISCOUNT_PERCENT, IFptr.DESTINATION_POSITION);
                        }
                    }
                }

                if (USE_FZ54) {

                    discount(fptr, 0, IFptr.DISCOUNT_SUMM, IFptr.DESTINATION_CHECK);
                } else {
                    discount(fptr, 1, IFptr.DISCOUNT_PERCENT, IFptr.DESTINATION_CHECK);
                }

                payment(fptr, sum.divide(new BigDecimal(4)).doubleValue(), 1);
                payment(fptr, sum.divide(new BigDecimal(4)).doubleValue(), 2);
                payment(fptr, sum.divide(new BigDecimal(4)).doubleValue(), 3);
                payment(fptr, sum.doubleValue(), 0);

                closeCheck(fptr, 0, null);
            }


            if (PRINT_NONFISCAL_CHECK) {
                printText(fptr, "Я помню чудное мгновенье:\n"
                        + "Передо мной явилась ты,\n"
                        + "Как мимолетное виденье,", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                printText(fptr, "Просто", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);
                printText(fptr, "Просто текст", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_WORD);
                printText(fptr, "Текст простой", IFptr.ALIGNMENT_RIGHT, IFptr.WRAP_WORD);
                printText(fptr, "Много халвы", IFptr.ALIGNMENT_LEFT, IFptr.WRAP_WORD);

                printText(fptr, "\nEAN8", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                printBarcode(fptr, IFptr.BARCODE_TYPE_EAN8, "40182735", 100);
                printText(fptr, "");
                printText(fptr, "\nEAN13", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                printBarcode(fptr, IFptr.BARCODE_TYPE_EAN13, "4007630000116", 200);
                printText(fptr, "");
                printText(fptr, "\nUPCA", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                printBarcode(fptr, IFptr.BARCODE_TYPE_UPCA, "697929110035", 100);
                printText(fptr, "");
                printText(fptr, "\nCODE39", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                printBarcode(fptr, IFptr.BARCODE_TYPE_CODE39, "ATOL.RU", 100);
                printText(fptr, "");
                printText(fptr, "\nPDF417", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                printBarcode(fptr, IFptr.BARCODE_TYPE_PDF417, "1234567", 100);
                printText(fptr, "");
                printText(fptr, "\nQR", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                printBarcode(fptr, IFptr.BARCODE_TYPE_QRCODE, "РђРўРћР›.Р РЈ", 500);
                printText(fptr, "");
                printText(fptr, "\nITF-14", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                printBarcode(fptr, IFptr.BARCODE_TYPE_ITF14, "00012345600012", 100);
                printText(fptr, "");
                printText(fptr, "\nInterleaved 2 of 5", IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
                printBarcode(fptr, IFptr.BARCODE_TYPE_INTERLEAVED_2_OF_5, "04062300106659", 100);
                printText(fptr, "");
                printFooter(fptr);
            }
            return true;
        } catch (Exception e) {

            log.error(e);
            exception=e;
            System.out.println(e);
            return false;

        } finally {
            fptr.ResetMode();
            fptr.destroy();
        }
    }

    public void print() {


        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask2<Void, Integer, Boolean> {


        @Override
        public Boolean doInBackground(Void... params) {
            return InnerPrint();

        }

        @Override
        public void onPreExecute() {
            Controller.WriteMessage("Тестовый чек");
            FactoryBlender.Run();
        }

        @Override
        public void onPostExecute(Boolean params) {
            FactoryBlender.stop();
            if(exception!=null){
                DialogFactory.ErrorDialog(exception);
                Controller.WriteMessage("Тестовый чек - ошибка");
            }
            Controller.WriteMessage("Печать окончена");
        }
    }


}
