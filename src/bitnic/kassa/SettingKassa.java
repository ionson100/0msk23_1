package bitnic.kassa;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;


public class SettingKassa extends BaseKassa {

    private static final Logger log = Logger.getLogger(SettingKassa.class);
    public static void Print() {
        IFptr fptr =null;
        try {
            fptr = new Fptr();
            fptr.create();
            //SetWorkDir(fptr);

            if (fptr.ShowProperties() < 0)
                checkError(fptr);

            if (fptr.put_DeviceEnabled(true) < 0)
                checkError(fptr);

            // Проверка связи
            if (fptr.GetStatus() < 0)
                checkError(fptr);

            // Убедились, что настройки подходят и касса отвечает - вытаскиваем актуальные настройки из драйвера

            String settings = fptr.get_DeviceSettings();

            SettingAppE.instance.settings_kassa=settings;
            SettingAppE.save();


        } catch (Exception e) {

            log.error(e);
            System.out.println(e);
        } finally {
            if(fptr!=null){
                fptr.ResetMode();
                fptr.destroy();
            }

        }

    }
}
