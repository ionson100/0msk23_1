package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.pagetableproduct.IRefrashLoader;
import bitnic.senders.AsyncTask2;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_60_61_62_63_64;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

public class XReport extends BaseKassa {

    private static final Logger log = Logger.getLogger(XReport.class);
    private Transaction transaction;

    public void print(Transaction transaction) {
        this.transaction = transaction;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask2<Void, Void, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();
            try {

                fptr = new Fptr();
                fptr.create();
                SetWorkDir(fptr);

                if (fptr.put_DeviceEnabled(true) < 0) {
                    checkError(fptr);
                }

                if (fptr.GetStatus() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }

                if (fptr.put_Mode(IFptr.MODE_REPORT_NO_CLEAR) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_ReportType(IFptr.REPORT_X) < 0) {
                    checkError(fptr);
                }
                if (fptr.Report() < 0) {
                    checkError(fptr);
                }
                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();
                transaction.numberDoc = GetNumberDoc(fptr);
                transaction.numberSession = GetNumberSession(fptr);
                transaction.summSmena = fptr.get_Summ();
                transaction.numberCheck=getNumberCheck(fptr);

                transaction.summSmena = fptr.get_Summ();

                if (fptr.put_RegisterNumber(11) < 0) {// выручка
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.выручка_отчет=fptr.get_Summ();


                if (fptr.put_RegisterNumber(12) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_OperationType(0) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сменный_итог = fptr.get_Summ();



                if (fptr.put_RegisterNumber(10) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }

                transaction.сумма_нал_ккм = fptr.get_Summ();


                return true;
            } catch (Exception ex) {
                log.error(ex);
                exception = ex;
                ex.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {
            FactoryBlender.Run();
            Controller.WriteMessage("Отчет без гашения");
        }

        @Override
        public void onPostExecute(Boolean params) {

            if (exception != null) {
                Controller.WriteMessage("Отчет без гашения - ошибка");
                DialogFactory.ErrorDialog(exception);
            }else {
                Controller.WriteMessage("");
                try {
                    Event_60_61_62_63_64 z = new Event_60_61_62_63_64();
                    z.int_1_4=60;
                    z.cod_print_group_17=1;
                    z.number_smena_14=transaction.numberSession;
                    z.nimberDoc_6 = transaction.numberDoc;
                    z.sum_smena_10 = transaction.выручка_отчет;
                    z.numberAction_1 = Iterator.getId();
                    z.codeKassir_7 = Integer.parseInt(SettingAppE.instance.user_id);
                    z.code_firma_27 = SettingAppE.instance.getPointId();
                    z.dateAction_2 = transaction.getDate();
                    z.timeAction_3 = transaction.getTime();
                    z.code_pm_5 = 1;
                    z.int_1_13=9;// операция в ккм
                    z.sum_kassa_11   = transaction.сумма_нал_ккм;
                    z.itogo_smena_12 = transaction.сменный_итог;
                    z.info_doc_26=""+transaction.numberCheck+"/"+transaction.numberDoc+"/"+transaction.numberSession;
                    transaction.list.add(z);
                }catch (Exception ex){
                    ex.printStackTrace();
                    DialogFactory.ErrorDialog(ex);
                }

                Controller.WriteMessage("Отчет без гашения - успешно");
                UtilsOmsk.appenderReportFile(transaction.list);
                if(Controller.Curnode instanceof IRefrashLoader){
                    ((IRefrashLoader) Controller.Curnode).refrash();
                }
                PrintCheck2.SenfBitnic();
            }
            FactoryBlender.stop();
        }
    }
}
