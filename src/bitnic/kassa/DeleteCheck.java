package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MProduct;
import bitnic.pagetableproduct.IRefrashLoader;
import bitnic.senders.AsyncTask2;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_56;
import bitnic.transaction.eventfrontol.RemoveDoc;
import bitnic.utils.UtilsOmsk;
import bitnic.utils.support;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DeleteCheck extends BaseKassa {
    private static List<MProduct> mProductList;
    private static int numderDoc;
    private static String code_doc;
    private static IAction<Integer> iAction;

    public static void DeleteReturn(List<MProduct> mProductList, int numderDoc, Transaction transaction, String code_doc, IAction<Integer> iAction) {
        DeleteCheck.mProductList = mProductList;

        DeleteCheck.numderDoc = numderDoc;
        DeleteCheck.code_doc = code_doc;
        DeleteCheck.iAction = iAction;

        new AsyncTask2<Void, String, Void>() {
            Exception exception;

            private Fptr fptr;

            private void checkError() throws Exception {
                int rc = fptr.get_ResultCode();
                if (rc < 0) {
                    String rd = fptr.get_ResultDescription(), bpd = null;
                    if (rc == -6) {
                        bpd = fptr.get_BadParamDescription();
                    }
                    if (bpd != null) {
                        throw new Exception(String.format("[%d] %s (%s)", rc, rd, bpd));
                    } else {
                        throw new Exception(String.format("[%d] %s", rc, rd));
                    }
                }
            }

            private void openCheck() throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }
                if (fptr.put_CheckType(IFptr.CHEQUE_TYPE_RETURN) < 0) {
                    checkError();
                }
                if (fptr.OpenCheck() < 0) {
                    checkError();
                }
            }


            private void reportZ() throws Exception {
                if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                    checkError();
                }
                if (fptr.SetMode() < 0) {
                    checkError();
                }
                if (fptr.put_ReportType(IFptr.REPORT_Z) < 0) {
                    checkError();
                }
                if (fptr.Report() < 0) {
                    checkError();
                }
                throw new Exception("Отмена отклоненна, пробит z отчет!");

            }

            private void registrationFZ54(String name, double price, double quantity) throws Exception {

                if (fptr.put_TaxNumber(3) < 0) {
                    checkError();
                }
                if (fptr.put_Quantity(quantity) < 0) {
                    checkError();
                }
                if (fptr.put_Price(price) < 0) {
                    checkError();
                }
                if (fptr.put_PositionSum(quantity * price) < 0) {
                    checkError();
                }
                if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0) {
                    checkError();
                }
                if (fptr.put_Name(name) < 0) {
                    checkError();
                }
                if (fptr.Return() < 0) {
                    checkError();
                }
            }

            @Override
            public Void doInBackground(Void... params) {

                fptr = new Fptr();
                try {
                    double max = 0;
                    for (MProduct product : mProductList) {
                        max = max + product.price + product.selectAmount;
                    }
                    if (max > SettingAppE.instance.maxSummPrice) {
                        throw new Exception(support.str("name119"));
                    }
                    fptr.create();
                    BaseKassa.SetWorkDir(fptr);

                    if (fptr.put_DeviceEnabled(true) < 0) {
                        checkError();
                    }
                    if (fptr.GetStatus() < 0) {
                        checkError();
                    }
                    try {
                        if (fptr.CancelCheck() < 0) {
                            checkError();
                        }
                    } catch (Exception e) {
                        int rc = fptr.get_ResultCode();
                        if (rc != -16 && rc != -3801) {
                            throw e;
                        }
                    }

                    try {
                        openCheck();
                    } catch (Exception e) {
                        // Проверка на превышение смены
                        if (fptr.get_ResultCode() == -3822) {
                            reportZ();
                            openCheck();
                        } else {
                            throw e;
                        }
                    }

                    Collections.sort(mProductList, Comparator.comparing(o -> o.name));

                    double totalAmount = 0;
                    double totalPrice = 0;
                    for (MProduct product : mProductList) {

                        totalAmount = totalAmount + product.selectAmount;
                        totalPrice = totalPrice + (product.price * product.selectAmount);
                        registrationFZ54(product.name_check, product.price, product.selectAmount);

                    }

                    closeCheck(fptr, 0, transaction);
                    BaseKassa.Printstate(fptr);

//                    checksData.checkNumber = fptr.get_CheckNumber();
//                    checksData.docNumber = fptr.get_DocNumber();
//                    checksData.kmmNumber = fptr.get_SerialNumber();

                    transaction.date = fptr.get_Date();
                    transaction.time = fptr.get_Time();
                    transaction.numberDoc = GetNumberDoc(fptr);
                    transaction.numberCheck = getNumberCheck(fptr);

                } catch (Exception e) {
                    exception = e;
                    e.printStackTrace();
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
                return null;
            }

            @Override
            public void onPreExecute() {
                FactoryBlender.Run();
                Controller.WriteMessage("Отмена чека");
            }

            @Override
            public void onPostExecute(Void aVoid) {

                FactoryBlender.stop();
                if (exception != null) {
                    Controller.WriteMessage("Отмена чека - ошибка");
                    DialogFactory.ErrorDialog(exception);
                } else {


                    try {
                        Controller.WriteMessage("");
                        RemoveDoc removeDoc = new RemoveDoc();
                        removeDoc.int_1_4 = 300;
                        removeDoc.code_pm_5 = numderDoc;
                        transaction.list.add(removeDoc);
                        removeDoc.numberAction_1 = Iterator.getId();
                        removeDoc.dateAction_2 = transaction.getDate();
                        removeDoc.timeAction_3 = transaction.getTime();
                        removeDoc.info_doc_26=code_doc;


                        Event_56 e = new Event_56();
                        e.numberAction_1 = Iterator.getId();
                        e.dateAction_2 = transaction.getDate();
                        e.timeAction_3 = transaction.getTime();
                        e.int_1_4 = 56;
                        e.code_pm_5 = 0;
                        e.nimberDoc_6 = transaction.numberDoc;
                        e.codeKassir_7 = Integer.parseInt(SettingAppE.instance.user_id);
                        e.kkm_operation_13=1;//возврат
                        e.number_smena_14=transaction.numberSession;
                        e.code_print_group_17=1;
                        e.type_doc_23=2;
                        e.info_doc_26=code_doc;
                        transaction.list.add(e);

                        UtilsOmsk.appenderReportFile(transaction.list);
                        if (iAction != null) {
                            iAction.action(transaction.numberDoc);
                        }
                        if (Controller.Curnode instanceof IRefrashLoader) {
                            ((IRefrashLoader) Controller.Curnode).refrash();
                        }
                        PrintCheck2.SenfBitnic();

                    } catch (Exception ex) {
                        DialogFactory.ErrorDialog(ex);
                        throw new RuntimeException(ex);

                    }


                }
            }


        }.execute(null);
    }
}
