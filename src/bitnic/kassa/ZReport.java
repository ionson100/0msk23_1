package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.pagetableproduct.IRefrashLoader;
import bitnic.senders.AsyncTask2;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_60_61_62_63_64;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

public class ZReport extends BaseKassa {

    private static final Logger log = Logger.getLogger(ZReport.class);
    private Transaction transaction;

    private Transaction transaction2 = new Transaction();

    public void print(Transaction transaction) {
        this.transaction = transaction;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask2<Void, Integer, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {

                fptr.create();
                SetWorkDir(fptr);

                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);

                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);


                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }

                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();
                transaction.numberDoc = GetNumberDoc(fptr);
                transaction.numberSession = GetNumberSession(fptr);
                transaction.summSmena = fptr.get_Summ();

                if (fptr.put_RegisterNumber(11) < 0) {// выручка
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.выручка_отчет=fptr.get_Summ();
                if (fptr.put_RegisterNumber(12) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_OperationType(0) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сменный_итог = fptr.get_Summ();
                if (fptr.put_RegisterNumber(10) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetRegister() < 0) {
                    checkError(fptr);
                }
                transaction.сумма_нал_ккм = fptr.get_Summ();
                transaction.numberCheck=getNumberCheck(fptr);



                //("Отмена чека...");
                try {
                    if (fptr.CancelCheck() < 0) {
                        checkError(fptr);
                    }
                } catch (Exception e) {
                    int rc = fptr.get_ResultCode();
                    if (rc != -16 && rc != -3801) {
                        throw e;
                    }
                }
                if (fptr.put_Mode(IFptr.MODE_REPORT_CLEAR) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_ReportType(IFptr.REPORT_Z) < 0) {
                    checkError(fptr);
                }

                if (fptr.Report() < 0) {
                    checkError(fptr);
                }

                {
                    transaction2.date = fptr.get_Date();
                    transaction2.time = fptr.get_Time();
                    transaction2.numberDoc = GetNumberDoc(fptr);
                    transaction2.numberSession = GetNumberSession(fptr);
                    transaction2.summSmena = fptr.get_Summ();

                    if (fptr.put_RegisterNumber(11) < 0) {// выручка
                        checkError(fptr);
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError(fptr);
                    }
                    transaction2.выручка_отчет=fptr.get_Summ();
                    if (fptr.put_RegisterNumber(12) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.put_OperationType(0) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError(fptr);
                    }
                    transaction2.сменный_итог = fptr.get_Summ();
                    if (fptr.put_RegisterNumber(10) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError(fptr);
                    }
                    transaction2.сумма_нал_ккм = fptr.get_Summ();
                    transaction2.numberCheck=getNumberCheck(fptr);
                }

                Printstate(fptr);

                ///////////////////////////////

                return true;

            } catch (Exception ex) {
                log.error(ex);
                exception = ex;
                ex.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {
            Controller.WriteMessage("Отчет c гашением");
            FactoryBlender.Run();
        }

        @Override
        public void onPostExecute(Boolean params) {

            FactoryBlender.stop();
            if (exception != null) {
                DialogFactory.ErrorDialog(exception);
                Controller.WriteMessage("Отчет c гашением - ошибка");
            }
            if (params == true) {

                 try {

                     {
                         Event_60_61_62_63_64 z = new Event_60_61_62_63_64();
                         z.int_1_4=63;
                         z.cod_print_group_17=2;
                         z.number_smena_14=transaction.numberSession;
                         z.nimberDoc_6 = transaction.numberDoc;
                         z.sum_smena_10 = transaction.выручка_отчет;
                         z.numberAction_1 = Iterator.getId();
                         z.codeKassir_7 = Integer.parseInt(SettingAppE.instance.user_id);
                         z.code_firma_27 = SettingAppE.instance.getPointId();
                         z.dateAction_2 = transaction.getDate();
                         z.timeAction_3 = transaction.getTime();
                         z.code_pm_5 = 1;
                         z.int_1_13=9;// операция в ккм
                         z.sum_kassa_11   = transaction.сумма_нал_ккм;
                         z.itogo_smena_12 = transaction.сменный_итог;
                         z.info_doc_26=""+transaction.numberCheck+"/"+transaction.numberDoc+"/"+transaction.numberSession;
                         transaction.list.add(z);
                     }
                     {
                         Event_60_61_62_63_64 z = new Event_60_61_62_63_64();
                         z.int_1_4=63;
                         z.cod_print_group_17=2;
                         z.number_smena_14=transaction2.numberSession;
                         z.nimberDoc_6 = transaction2.numberDoc;
                         z.sum_smena_10 = transaction2.выручка_отчет;
                         z.numberAction_1 = Iterator.getId();
                         z.codeKassir_7 = Integer.parseInt(SettingAppE.instance.user_id);
                         z.code_firma_27 = SettingAppE.instance.getPointId();
                         z.dateAction_2 = transaction2.getDate();
                         z.timeAction_3 = transaction2.getTime();
                         z.code_pm_5 = 1;
                         z.int_1_13=9;// операция в ккм
                         z.sum_kassa_11   = transaction2.сумма_нал_ккм;
                         z.itogo_smena_12 = transaction2.сменный_итог;
                         z.info_doc_26=""+transaction2.numberCheck+"/"+transaction2.numberDoc+"/"+transaction2.numberSession;
                         transaction.list.add(z);
                     }
                     {
                         Event_60_61_62_63_64 z=new Event_60_61_62_63_64();// закрытие смены
                         z.int_1_4=61;

                         z.int_6_23=9;
                         z.number_smena_14=transaction.numberSession;
                         z.nimberDoc_6 = transaction.numberDoc;
                         z.sum_smena_10 = transaction.выручка_отчет;
                         z.numberAction_1 = Iterator.getId();
                         z.codeKassir_7 = Integer.parseInt(SettingAppE.instance.user_id);
                         z.code_firma_27 = SettingAppE.instance.getPointId();
                         z.dateAction_2 = transaction.getDate();
                         z.timeAction_3 = transaction.getTime();
                         z.code_pm_5 = 1;
                         z.int_1_13=10;// операция в ккм
                         z.sum_kassa_11   = transaction.сумма_нал_ккм;
                         z.itogo_smena_12 = transaction.сменный_итог;
                         //z.info_doc_26=""+transaction.numberCheck+"/"+transaction.numberDoc+"/"+transaction.numberSession;
                         transaction.list.add(z);
                     }





                 }catch (Exception ex){
                     ex.printStackTrace();
                     DialogFactory.ErrorDialog(ex);
                 }

                Controller.WriteMessage("Отчет c гашением - успешно");
                UtilsOmsk.appenderReportFile(transaction.list);
                if(Controller.Curnode instanceof IRefrashLoader){
                    ((IRefrashLoader) Controller.Curnode).refrash();
                }
                PrintCheck2.SenfBitnic();
            }

        }
    }
}
