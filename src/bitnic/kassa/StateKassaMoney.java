package bitnic.kassa;

import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.senders.AsyncTask2;
import org.apache.log4j.Logger;

public class StateKassaMoney extends BaseKassa {

    private static final Logger log = Logger.getLogger(StateKassaMoney.class);
    public void print() {
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask2<Void, Integer, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();
                SetWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);


                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }
                //("Отмена чека...");
                try {
                    if (fptr.CancelCheck() < 0) {
                        checkError(fptr);
                    }
                } catch (Exception e) {
                    int rc = fptr.get_ResultCode();
                    if (rc != -16 && rc != -3801) {
                        throw e;
                    }
                }
                if (fptr.put_Mode(IFptr.MODE_PROGRAMMING) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }


                Printstate(fptr);


                return true;

            } catch (Exception ex) {
                log.error(ex);
                exception = ex;
                ex.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {
            setDemon(true);
        }

        @Override
        public void onPostExecute(Boolean params) {


        }
    }


}
