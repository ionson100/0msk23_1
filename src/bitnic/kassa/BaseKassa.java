package bitnic.kassa;

import bitnic.utils.support;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Transaction;
import bitnic.utils.DesktopApi;


public class BaseKassa {


    protected Exception exception;
    static final boolean USE_SHOWPROPERTIES = false;
    static final boolean USE_FZ54 = true;
    static final boolean PRINT_FISCAL_CHECK = true;
    static final boolean PRINT_NONFISCAL_CHECK = true;

    public static int GetNumberDoc(IFptr fptr) {
        return fptr.get_DocNumber();
    }

    public static int getNumberCheck(IFptr fptr){
        return fptr.get_CheckNumber();
    }

    public static int GetNumberSession(IFptr fptr) {
        return fptr.get_Session()+1;
    }


    public static void CheckUser(IFptr fptr) throws Exception {
        if (fptr.put_CaptionPurpose(118) < 0) {
            checkError(fptr);
        }
        if (fptr.GetCaption() < 0) {
            checkError(fptr);
        }
        String user = fptr.get_Caption();
        String user_real = SettingAppE.instance.getUserCheck();
        if (user.equals(user_real) == false) {
            Controller.WriteMessage( support.str ( "name117" ));
        }
    }

    public static void Printstate(IFptr fptr) throws DriverException {
        //////////////////////
        String выручка = null, нал = null, приходсмена = null;
        if (fptr.put_RegisterNumber(11) < 0) {// выручка
            checkError(fptr);
        }
        if (fptr.GetRegister() < 0) {
            checkError(fptr);
        }
        выручка = String.valueOf(fptr.get_Summ());
        ///////////////////////////////////////////////////////////////////////////////////Сумма наличности в ККМ (Summ)
        if (fptr.put_RegisterNumber(10) < 0) {
            checkError(fptr);
        }
        if (fptr.GetRegister() < 0) {
            checkError(fptr);
        }

        нал = String.valueOf(fptr.get_Summ());
        /////////////////
        //////////////////////////////////////////////////////////////////////////////////  сумма сменного итога приход
        if (fptr.put_RegisterNumber(12) < 0) {
            checkError(fptr);
        }
        if (fptr.put_OperationType(0) < 0) {
            checkError(fptr);
        }
        if (fptr.GetRegister() < 0) {
            checkError(fptr);
        }
        приходсмена = String.valueOf(fptr.get_Summ());
        ///////////////////////////////
        Controller.WriteStateKkm(выручка, нал, приходсмена);
    }


    public static void SetWorkDir(IFptr fptr) throws DriverException {

        if (DesktopApi.getOs().isWindows()) {

            if (SettingAppE.instance.settings_kassa != null && SettingAppE.instance.settings_kassa.length() > 0) {
                if (fptr.put_DeviceSettings(SettingAppE.instance.settings_kassa) < 0)
                    checkError(fptr);
            }else {
                DialogFactory.InfoDialog("Касса",support.str ( "name118" ));
            }
        } else {
            if (fptr.put_DeviceSingleSetting(IFptr.SETTING_PORT, IFptr.SETTING_PORT_USB) < 0)
                checkError(fptr);
            if (fptr.put_DeviceSingleSetting(IFptr.SETTING_VID, 0x2912) < 0)
                checkError(fptr);
            if (fptr.put_DeviceSingleSetting(IFptr.SETTING_PID, 0x0005) < 0)
                checkError(fptr);

            if (fptr.put_DeviceSingleSetting(IFptr.SETTING_MODEL, IFptr.MODEL_ATOL_11F) < 0)
                checkError(fptr);

            if (fptr.put_DeviceSingleSetting(IFptr.SETTING_BAUDRATE, 115200) < 0)
                checkError(fptr);
            if (fptr.ApplySingleSettings() < 0)
                checkError(fptr);
        }
//
        fptr.put_DeviceSingleSetting(fptr.SETTING_SEARCHDIR, System.getProperty("java.library.path"));//
        fptr.ApplySingleSettings();
    }

    static void checkError(IFptr fptr) throws PrintTestCheck.DriverException {
        int rc = fptr.get_ResultCode();
        if (rc < 0) {
            String rd = fptr.get_ResultDescription(), bpd = null;
            if (rc == -6) {
                bpd = fptr.get_BadParamDescription();
            }
            if (bpd != null)
                throw new PrintTestCheck.DriverException(String.format("[%d] %s (%s)", rc, rd, bpd));
            else
                throw new PrintTestCheck.DriverException(String.format("[%d] %s", rc, rd));
        }
    }

    static void printText(IFptr fptr, String text, int alignment, int wrap) throws PrintTestCheck.DriverException {
        if (fptr.put_Caption(text) < 0)
            checkError(fptr);
        if (fptr.put_TextWrap(wrap) < 0)
            checkError(fptr);
        if (fptr.put_Alignment(alignment) < 0)
            checkError(fptr);
        if (fptr.PrintString() < 0)
            checkError(fptr);
    }

    static void printText(IFptr fptr, String text) throws PrintTestCheck.DriverException {
        printText(fptr, text, IFptr.ALIGNMENT_CENTER, IFptr.WRAP_LINE);
    }


    static void openCheck(IFptr fptr, int type) throws PrintTestCheck.DriverException {
        if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0)
            checkError(fptr);
        if (fptr.SetMode() < 0)
            checkError(fptr);
        if (fptr.put_CheckType(type) < 0)
            checkError(fptr);
        if (fptr.OpenCheck() < 0)
            checkError(fptr);
    }

    static void closeCheck(IFptr fptr, int typeClose, Transaction transaction) throws PrintTestCheck.DriverException {
        if (fptr.put_TypeClose(typeClose) < 0)
            checkError(fptr);
        if (fptr.CloseCheck() < 0)
            checkError(fptr);

    }

    static class DriverException extends Exception {
        private static final long serialVersionUID = 6164921645357791803L;

        public DriverException(String msg) {
            super(msg);
        }
    }

    static void reportZ(IFptr fptr) throws DriverException {
        if (fptr.put_Mode(IFptr.MODE_REPORT_CLEAR) < 0)
            checkError(fptr);
        if (fptr.SetMode() < 0)
            checkError(fptr);
        if (fptr.put_ReportType(IFptr.REPORT_Z) < 0)
            checkError(fptr);
        if (fptr.Report() < 0)
            checkError(fptr);

    }

}

