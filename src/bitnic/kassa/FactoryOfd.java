package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask2;
import bitnic.senders.IAction;
import org.apache.log4j.Logger;

public class FactoryOfd extends BaseKassa{
    private static final Logger log = Logger.getLogger(FactoryOfd.class);
    private  IAction<Ofd> ofdIAction;
    public void saveOfd(Ofd ofd,IAction<String> ofdIAction) {
        IAction<String> finalOfdIAction = ofdIAction;
        new AsyncTask2<Void, Void, Void>() {
            @Override
            public Void doInBackground(Void... params) {
                IFptr fptr = new Fptr();
                try {
                    fptr.create();
                    SetWorkDir(fptr);
                    // Подключаемся к устройству
                    if (fptr.put_DeviceEnabled(true) < 0)
                        checkError(fptr);
                    // Проверка связи
                    if (fptr.GetStatus() < 0)
                        checkError(fptr);
                    if (fptr.put_UserPassword("00000030") < 0) {
                        checkError(fptr);
                    }
                    if (fptr.put_Mode(IFptr.MODE_PROGRAMMING) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.SetMode() < 0) {
                        checkError(fptr);
                    }

                    //////////////////////////////
                    if (fptr.put_ValuePurpose(301) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.put_Value(ofd.ofd_port) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.SetValue() < 0) {
                        checkError(fptr);
                    }
                    ///////////////////////////////////
                    if (fptr.put_Caption(ofd.ofd_ip) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.put_CaptionPurpose(256) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.SetCaption() < 0) {
                        checkError(fptr);
                    }
                    /////////////////////////////////////////////
                    finalOfdIAction.action("Настройки успешно изменены");

                    return null;
                }catch (Exception ex){
                    log.error(ex);
                    exception=ex;
                    System.out.println(ex);
                    return null;
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
            }

            @Override
            public void onPreExecute() {
                FactoryBlender.Run();
                Controller.WriteMessage("Настройка офд");

            }

            @Override
            public void onPostExecute(Void params) {

                FactoryBlender.stop();
                if(exception!=null){
                    Controller.WriteMessage("Настройка офд - ошибка");
                    DialogFactory.ErrorDialog(exception);
                }else {
                    Controller.WriteMessage("Настройка офд - успешно");
                }
            }
        }.execute(null);

    }

    public static class Ofd{
        public final String ofd_ip;
        public final int ofd_port;

        public Ofd(String ofd_ip, int ofd_port){

            this.ofd_ip = ofd_ip;
            this.ofd_port = ofd_port;
        }
    }


    public  void getOfd(IAction<Ofd> ofdIAction){
        IAction<Ofd> finalOfdIAction = ofdIAction;
        new AsyncTask2<Void, Void, Void>() {
            @Override
            public Void doInBackground(Void... params) {
                IFptr fptr = new Fptr();
                try {
                    fptr.create();
                    SetWorkDir(fptr);
                    fptr.ApplySingleSettings();
                    // Подключаемся к устройству
                    if (fptr.put_DeviceEnabled(true) < 0)
                        checkError(fptr);
                    // Проверка связи
                    if (fptr.GetStatus() < 0)
                        checkError(fptr);
                    if (fptr.put_UserPassword("00000030") < 0) {
                        checkError(fptr);
                    }
                    if (fptr.put_Mode(IFptr.MODE_PROGRAMMING) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.SetMode() < 0) {
                        checkError(fptr);
                    }
                    if (fptr.put_CaptionPurpose(256) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.GetCaption() < 0) {
                        checkError(fptr);
                    }

                    String ofdip = fptr.get_Caption();
                    if (fptr.put_ValuePurpose(301) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.GetValue() < 0) {
                        checkError(fptr);
                    }
                    int ofdport = (int) fptr.get_Value();

                    Ofd ofd=new Ofd(ofdip,ofdport);
                    finalOfdIAction.action(ofd);


                    return null;
                }catch (Exception ex){
                    DialogFactory.ErrorDialog(ex);
                    System.out.println(ex);
                    return null;
                } finally {
                    fptr.ResetMode();
                    fptr.destroy();
                }
            }

            @Override
            public void onPreExecute() {


            }

            @Override
            public void onPostExecute(Void params) {

            }
        }.execute(null);
    }
}
