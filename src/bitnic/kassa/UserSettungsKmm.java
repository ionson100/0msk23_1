package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask2;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import java.util.Date;

public class UserSettungsKmm extends BaseKassa {

    private static final Logger log = Logger.getLogger(UserSettungsKmm.class);
    public void print() {

        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask2<Void, Integer, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();

                SetWorkDir(fptr);
                FactoryBlender.AddMessage("Подключаемся к устройству");
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
                //("Ввод паспорта...");
                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }
                FactoryBlender.AddMessage("Ввод пароля");
                //("Установка программирования...");
                if (fptr.put_Mode(4) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                //("Сохранение состояния...");
                if (fptr.put_ValuePurpose(337) < 0) {// печать штрих кода
                    checkError(fptr);
                }
                FactoryBlender.AddMessage("Печать штрих кода");
                if (fptr.put_Value(1) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }
                //("Печатать штрихкод...");
                if (fptr.put_ValuePurpose(57) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Value(1) < 0) {// печать сквозного номера документа
                    checkError(fptr);
                }
                FactoryBlender.AddMessage("Печать скводного номера дукумента");
                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_ValuePurpose(83) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Value(3) < 0) {// печать при помощи PrintString
                    checkError(fptr);
                }
                FactoryBlender.AddMessage("Разрешить печать PrintString");
                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }
                //("порт офд...");
                ////////////////////////////////////порт офд

                if (fptr.put_ValuePurpose(301) < 0) {
                    checkError(fptr);
                }
                // if (Utils.OFD) {
                if (fptr.put_Value(SettingAppE.instance.getPortOfd()) < 0) {
                    checkError(fptr);
                }


                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }
                FactoryBlender.AddMessage("Установка порта ОФД");
////////////////////////////////////////////////////////////////////////////////////////////////
                //("wifi офд...");
                if (fptr.put_ValuePurpose(302) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Value(1) < 0) {// EoU
                    checkError(fptr);
                }
                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }

                if (fptr.put_ValuePurpose(277) < 0) {// повторная печать
                    checkError(fptr);
                }
                FactoryBlender.AddMessage("Разрешить повторную печать");
                if (fptr.put_Value(1) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetValue() < 0) {
                    checkError(fptr);
                }
                ///////////////////
                Date dat = UtilsOmsk.curDate();
                if (fptr.put_Time(dat) < 0) {
                    checkError(fptr);
                }
                int dd1 = fptr.SetTime();
                if (dd1 == -3893) {
                    dd1 = fptr.SetTime();
                }
                if (dd1 < 0) {
                    checkError(fptr);
                }
                ////////////////////
                if (fptr.put_Date(UtilsOmsk.curDate()) < 0) {
                    checkError(fptr);
                }
                int dd = fptr.SetDate();
                if (dd == -3893) {
                    dd = fptr.SetDate();
                }
                if (dd < 0) {
                    checkError(fptr);
                }
                //("Set Date");
                //////////////////////// dns ofd
                FactoryBlender.AddMessage("Синхронизация даты");

                if (fptr.put_Caption("0.0.0.0") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_CaptionPurpose(257) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                //////////////////////// url ofd
                FactoryBlender.AddMessage("Установка DNS сервера ОФД");

                //("url офд...");

                if (fptr.put_Caption(SettingAppE.instance.getOfdUrl()) < 0) {
                    checkError(fptr);
                }

                if (fptr.put_CaptionPurpose(256) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                ////////////////////////////////
                FactoryBlender.AddMessage("Установка адреса сервера ОФД");
                if (fptr.put_Caption(SettingAppE.instance.getUserCheck()) < 0) {
                    checkError(fptr);
                }
                FactoryBlender.AddMessage("Установка имени кассира");
                if (fptr.put_CaptionPurpose(118) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                //("Save username...");
                if (fptr.put_Caption(" ") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_CaptionPurpose(72) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                ////////////////////////////////////////////////////////////////
                //("нах титул1");
                if (fptr.put_Caption(" ") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_CaptionPurpose(73) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                //("нах титул2");
                ///////////////////////////////////////
                if (fptr.put_Caption(" ") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_CaptionPurpose(69) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                //("нах титул3");
                if (fptr.put_Caption(" ") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_CaptionPurpose(70) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetCaption() < 0) {
                    checkError(fptr);
                }
                //("нах тит4");
                fptr.destroy();
                FactoryBlender.AddMessage("Закончили");

                return true;
            } catch (Exception ex) {
                log.error(ex);
                exception = ex;
                System.out.println(ex);
                return false;
            } finally {
                fptr.destroy();
            }
        }


        @Override
        public void onPreExecute() {
            FactoryBlender.Run();
            Controller.WriteMessage("Настройки ккм");

        }

        @Override
        public void onPostExecute(Boolean params) {
            FactoryBlender.stop();
            if (params) {
                Controller.WriteMessage("Настройки ккм - успешно");
            } else {
                if (exception != null) {
                    Controller.WriteMessage("Настройки ккм - ошибка");
                    DialogFactory.ErrorDialog(exception);
                }
            }
        }
    }
}
