package bitnic.kassa;

import bitnic.dialogfactory.DialogFactory;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import bitnic.senders.AsyncTask2;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.Pather;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.List;


public class SenderGetStringStatusFtp {

    private static final Logger log = Logger.getLogger(SenderGetStringStatusFtp.class);

    private StringBuilder error = new StringBuilder();
    String res="";
    private SettingAppE ftp = SettingAppE.instance;
    private boolean isUTF = true;

    public String getStringCore() {


        List<String> listFiles=new SenderGetFilesFtpDirectory().getNameFile();


        if (SettingAppE.instance.getUrl() == null || SettingAppE.instance.getUrl().trim().length() == 0) {
            DialogFactory.ErrorDialog("Не заданы настройки FTP");
            log.error("Не заданы настройки FTP");
        }
        if (ftp.ftp_cahrset != null && ftp.ftp_cahrset.toUpperCase().contains("UTF")) {
            isUTF = true;
        } else {
            isUTF = false;
        }

//        try {
//            new MyWorker().Get();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return null;

    }
    private class MyWorker extends AsyncTask2<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            FTPClient ftpClient = new FTPClient();
            try {
                ftpClient.connect(ftp.getUrl(), ftp.ftp_port);
                ftpClient.login(ftp.ftp_user, ftp.ftp_password);
                ftpClient.enterLocalPassiveMode();
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

                OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(new File(Pather.inFilesData)));
                boolean success = ftpClient.retrieveFile(ftp.ftp_remote_file, outputStream1);
                outputStream1.close();

                if (success) {



                    System.out.println("File #1 has been downloaded successfully.");

                } else {
                    String er="Ошибка загрузки файла ftp: " + ftp.ftp_remote_file;
                    System.out.println(er);
                    log.info(er);
                    DialogFactory.ErrorDialog(er);
                }

            } catch (IOException ex) {
                log.error(ex);
                error.append(ex.getMessage());
                System.out.println("Error: " + ex.getMessage());
                ex.printStackTrace();
                DialogFactory.ErrorDialog(ex);
                return null;
            } finally {
                try {
                    if (ftpClient.isConnected()) {
                        ftpClient.logout();
                        ftpClient.disconnect();
                    }
                } catch (IOException ex) {
                    log.error(ex);
                    ex.printStackTrace();
                    DialogFactory.ErrorDialog(ex);
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() throws IOException {

        }

        @Override
        protected void onPostExecute(Void params) {

        }
    }
}
