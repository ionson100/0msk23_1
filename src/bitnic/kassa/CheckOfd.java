package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;

import bitnic.senders.AsyncTask2;
import org.apache.log4j.Logger;

public class CheckOfd extends BaseKassa {
    private static final Logger log = Logger.getLogger(CheckOfd.class);
    public void print() {
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask2<Void, Integer, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();
                SetWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
                // Отменяем чек, если уже открыт. Ошибки "Неверный режим" и "Чек уже закрыт"
                // не являются ошибками, если мы хотим просто отменить чек
                //("Отмена чека...");
//                try {
//                    if (fptr.CancelCheck() < 0) {
//                        checkError(fptr);
//                    }
//                } catch (Exception e) {
//                    int rc = fptr.get_ResultCode();
//                    if (rc != -16 && rc != -3801) {
//                        throw e;
//                    }
//                }

                if (fptr.put_CommandBuffer("82 01 06 00") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_NeedResultFlag(false) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_TimeoutACK(500) < 0) {
                    checkError(fptr);
                }
                if (fptr.put_TimeoutENQ(5000) < 0) {
                    checkError(fptr);
                }
                if (fptr.RunCommand() < 0) {
                    checkError(fptr);
                }

                Printstate(fptr);
                return true;
            } catch (Exception e) {
                log.error(e);
                exception=e;
                e.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {

            Controller.WriteMessage("Проверка офд");
            FactoryBlender.Run();
        }

        @Override
        public void onPostExecute(Boolean params) {
            FactoryBlender.stop();
            if(params==false&&exception!=null){
                DialogFactory.ErrorDialog(exception);
                Controller.WriteMessage("Проверка офд - ошибка");
            }else {
                Controller.WriteMessage("");
            }
        }
    }

}
