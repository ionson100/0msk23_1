package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask2;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_50_51;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import java.io.IOException;

public class EntryMoney extends BaseKassa {
    private static final Logger log = Logger.getLogger(EntryMoney.class);
    private double aDouble;
    private Transaction transaction;

    public void run(double aDouble, Transaction transaction) {

        this.aDouble = aDouble;
        this.transaction = transaction;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask2<Void, Void, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();
                SetWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Mode(1) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Summ(aDouble) < 0) {
                    checkError(fptr);
                }
                if (fptr.CashOutcome() < 0) {
                    checkError(fptr);
                }
                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();
                transaction.numberDoc = GetNumberDoc(fptr);
                transaction.numberCheck=getNumberCheck(fptr);
                transaction.numberSession= GetNumberSession(fptr);
                Printstate(fptr);
                return true;
            } catch (Exception e) {
                log.error(e);
                exception = e;
                e.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() throws IOException {

            FactoryBlender.Run();
            Controller.WriteMessage("Выдача денег из кассы");
        }

        @Override
        public void onPostExecute(Boolean params) {


            if (params == false && exception != null) {
                Controller.WriteMessage("Выдача денег из кассы - ошибка");
                DialogFactory.ErrorDialog(exception);
            } else {
                Controller.WriteMessage("");
                Event_50_51 e = new Event_50_51();
                e.numberAction_1 = Iterator.getId();
                e.dateAction_2 = transaction.getDate();
                e.timeAction_3 = transaction.getTime();
                e.int_1_4 = 51;//вынесение денег
                e.code_pm_5 = 1;
                e.code_print_group_17=1;
                e.int_4_23=6;
                e.number_smena_14=transaction.numberSession;
                e.nimberDoc_6 = transaction.numberDoc;
                e.codeKassir_7 = Integer.parseInt(SettingAppE.instance.user_id);
                e.double_3_12 = aDouble;
                e.info_doc_26 = "1/"+transaction.numberCheck+"/" + transaction.numberDoc;
                e.code_firma_27 = SettingAppE.instance.getPointId();
                e.kkm_operation_13 = 5; //вынесение денег
                transaction.list.add(e);
                UtilsOmsk.appenderReportFile(transaction.list);
                Controller.WriteMessage("");
                PrintCheck2.SenfBitnic();
            }
            FactoryBlender.stop();
        }
    }
}
