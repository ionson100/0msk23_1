package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask2;
import org.apache.log4j.Logger;

public class UpdateItemKkm extends BaseKassa {
    private static final Logger log = Logger.getLogger(UpdateItemKkm.class);
    private ItemSettingKkm item;
    private Object value;

    public void update(ItemSettingKkm item, Object o) {
        this.item = item;
        this.value = o;
        new MyWorker().execute(null);
    }
    private class MyWorker extends AsyncTask2<Void, Void, Boolean> {

        @Override
        public Boolean doInBackground(Void... params)  {
            IFptr fptr = new Fptr();
            try {

                fptr.create();
                SetWorkDir(fptr);
                if (fptr.put_DeviceEnabled(true) < 0) {
                    checkError(fptr);
                }
                if (fptr.GetStatus() < 0) {
                    checkError(fptr);
                }
                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }
                if (fptr.put_Mode(IFptr.MODE_PROGRAMMING) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                if(item.param.equals(SettingsKkmTable.stroka)){
                    ////////////////////////////////////////////////////////
                    if (fptr.put_CaptionPurpose(item.index) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.put_Caption(String.valueOf(value)) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.SetCaption() < 0) {
                        checkError(fptr);
                    }
                    /////////////////////////////////////////////////////////
                }else {
                    if (fptr.put_ValuePurpose(item.index) < 0) {// печать рекламы как клише
                        checkError(fptr);
                    }
                    if (fptr.put_Value(Integer.parseInt(value.toString())) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.SetValue() < 0) {
                        checkError(fptr);
                    }
                }

                return true;
            } catch (Exception ex) {

                log.error(ex);
                ex.printStackTrace();
                DialogFactory.ErrorDialog(ex);
                return false;

            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute()  {

            FactoryBlender.Run();
        }

        @Override
        public void onPostExecute(Boolean params) {

            FactoryBlender.stop();
        }
    }
}
