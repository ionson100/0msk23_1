package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.utils.Pather;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MProduct;
import bitnic.model.MTaxrates;
import bitnic.orm.Configure;
import bitnic.pagesale.Cmainform;
import bitnic.senders.AsyncTask2;
import bitnic.senders.SenderFileBitnic;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Iterator;
import bitnic.transaction.Transaction;
import bitnic.transaction.eventfrontol.Event_1_11_2_12_4_14;
import bitnic.transaction.eventfrontol.Event_40_41;
import bitnic.transaction.eventfrontol.Event_42_55;
import bitnic.transaction.eventfrontol.Event_60_61_62_63_64;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PrintCheck2 extends BaseKassa {

    private static final Logger log = Logger.getLogger(PrintCheck2.class);

    private boolean isTranaction;
    private Exception exception;
    private List<MProduct> selectListProduct;
    private Transaction transaction;
    private Double payment_type;
    private String numberCard;
    private String codePayment;
    double totalPriceE;

    public void print(List<MProduct> selectListProduct, Transaction trans, Double PAYMENT_TYPE, String numberCard, String codePayment) {

        this.selectListProduct = selectListProduct;
        transaction = trans;
        payment_type = PAYMENT_TYPE;
        this.numberCard = numberCard;
        this.codePayment = codePayment;
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask2<Void, Integer, Boolean> {

        private void registrationFZ54E(IFptr fptr, MProduct mProduct) throws Exception {


            List<MTaxrates> mTaxrates = Configure.GetSession().getList(MTaxrates.class, "cod_nalog = ?", mProduct.cod_nalog_group);

            if (mTaxrates.size() > 0) {
                if (fptr.put_TaxNumber(mTaxrates.get(0).nalog_number_kkm) < 0) {
                    checkError(fptr);
                }
            } else {
                if (fptr.put_TaxNumber(Fptr.TAX_VAT_18) < 0) {
                    checkError(fptr);
                }
            }

            if (fptr.put_Quantity(mProduct.selectAmount) < 0) {
                checkError(fptr);
            }
            if (fptr.put_Price(mProduct.price) < 0) {
                checkError(fptr);
            }
            if (fptr.put_PositionSum(mProduct.selectAmount * mProduct.price) < 0) {
                checkError(fptr);
            }
            if (fptr.put_TextWrap(IFptr.WRAP_WORD) < 0) {
                checkError(fptr);
            }
            if (fptr.put_Name(mProduct.name_check) < 0) {
                checkError(fptr);
            }
            if (fptr.Registration() < 0) {
                checkError(fptr);
            }
        }

        private void openCheckE(IFptr fptr, int type) throws Exception {
            if (fptr.put_Mode(IFptr.MODE_REGISTRATION) < 0) {
                checkError(fptr);
            }
            if (fptr.SetMode() < 0) {
                checkError(fptr);
            }

            if (fptr.put_CheckType(type) < 0) {
                checkError(fptr);
            }
            if (fptr.OpenCheck() < 0) {
                checkError(fptr);
            }
        }

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            FactoryBlender.AddMessage("Создание объекта принтера чеков");
            try {
                fptr.create();

                SetWorkDir(fptr);

                FactoryBlender.AddMessage("Инициализация настроек");
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);

                if (fptr.GetStatus() < 0)
                    checkError(fptr);

                FactoryBlender.AddMessage("Изменение статуса");
                double max = 0;
                for (MProduct product : selectListProduct) {
                    max = max + product.price * product.selectAmount;
                }
                FactoryBlender.AddMessage("Расчет суммы чека");
                if (max > SettingAppE.instance.mapSumm) {
                    throw new Exception("Сумма чека превышает допустимую: " + String.valueOf(SettingAppE.instance.mapSumm));
                }

                try {
                    if (fptr.CancelCheck() < 0) {
                        checkError(fptr);
                    }
                } catch (Exception e) {
                    int rc = fptr.get_ResultCode();
                    if (rc != -16 && rc != -3801) {
                        throw e;
                    }
                }


                /////////////////////////////////////////// открытие сменыж

                String s = UtilsOmsk.readFile(Pather.numberSession).replace("\n","");
                int storeId=0;
                try{
                    storeId=Integer.parseInt(s);
                }catch (Exception ex){

                }
                int coreId= GetNumberSession(fptr);
                if (storeId!=coreId) {
                    UtilsOmsk.rewriteFile(Pather.numberSession, String.valueOf(GetNumberSession(fptr)));
                    isTranaction = true;
                }
                if (isTranaction) {


                    if (fptr.put_RegisterNumber(11) < 0) {// выручка
                        checkError(fptr);
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError(fptr);
                    }
                    transaction.выручка_отчет = fptr.get_Summ();
                    if (fptr.put_RegisterNumber(12) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.put_OperationType(0) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError(fptr);
                    }
                    transaction.сменный_итог = fptr.get_Summ();
                    if (fptr.put_RegisterNumber(10) < 0) {
                        checkError(fptr);
                    }
                    if (fptr.GetRegister() < 0) {
                        checkError(fptr);
                    }
                    transaction.сумма_нал_ккм = fptr.get_Summ();
                }

                ///////////////////////////////////////////


                try {
                    openCheckE(fptr, IFptr.CHEQUE_TYPE_SELL);
                } catch (Exception e) {
                    // Проверка на превышение смены
                    if (fptr.get_ResultCode() == -3822) {
                        Controller.WriteMessage("Требуется z - отчет.");
                        throw e;
                    } else {
                        throw e;
                    }
                }

                FactoryBlender.AddMessage("Открытие чека");
                Collections.sort(selectListProduct, new Comparator<MProduct>() {
                    @Override
                    public int compare(MProduct lhs, MProduct rhs) {
                        return lhs.name.compareTo(rhs.name);
                    }
                });

                double totalAmount = 0;
                double totalPrice = 0;
                for (MProduct product : selectListProduct) {

                    totalAmount = totalAmount + product.selectAmount;
                    totalPrice = totalPrice + (product.selectAmount * product.price);
                    registrationFZ54E(fptr, product);

                }
                FactoryBlender.AddMessage("Инициализация чека продуктами");
                totalPriceE = totalPrice;
                transaction.date = fptr.get_Date();
                transaction.time = fptr.get_Time();

                transaction.numberDoc = GetNumberDoc(fptr);
                transaction.numberSession = GetNumberSession(fptr);
                transaction.numberCheck = getNumberCheck(fptr);

                closeCheck(fptr, 0, transaction);
                Printstate(fptr);
                FactoryBlender.AddMessage("Чек отпечатан");
                return true;

            } catch (Exception ex) {
                exception = ex;
                log.error(ex);
                //DialogFactory.ErrorDialog(ex);
                ex.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }


        @Override
        public void onPreExecute() {

            FactoryBlender.Run();
            Controller.WriteMessage("Печать чека");
        }

        @Override
        public void onPostExecute(Boolean params) {


            if (params == true) {
                Controller.WriteMessage("");
                if (isTranaction) {
                    Event_60_61_62_63_64 z = new Event_60_61_62_63_64();
                    z.int_1_4 = 64;
                    z.number_smena_14 = transaction.numberSession;
                    z.nimberDoc_6 = transaction.numberDoc;
                    z.sum_smena_10 = transaction.выручка_отчет;
                    z.numberAction_1 = Iterator.getId();
                    z.codeKassir_7 = Integer.parseInt(SettingAppE.instance.user_id);
                    z.code_firma_27 = SettingAppE.instance.getPointId();
                    z.dateAction_2 = transaction.getDate();
                    z.timeAction_3 = transaction.getTime();
                    z.code_pm_5 = 1;
                    z.int_1_13 = 9;// операция в ккм
                    z.sum_kassa_11 = transaction.сумма_нал_ккм;
                    z.itogo_smena_12 = transaction.сменный_итог;
                    z.info_doc_26 = "" + transaction.numberCheck + "/" + transaction.numberDoc + "/" + transaction.numberSession;
                    transaction.list.add(z);

                }

                for (MProduct mProduct : selectListProduct) {
                    Event_1_11_2_12_4_14 e = new Event_1_11_2_12_4_14();
                    e.codeProd_8 = mProduct.code_prod;
                    e.price_not_discount_10 = mProduct.price;
                    e.amount_prod_11 = mProduct.selectAmount;

                    e.numberAction_1 = Iterator.getId();
                    e.dateAction_2 = transaction.getDate();
                    e.timeAction_3 = transaction.getTime();
                    e.int_1_4 = 11;
                    e.code_pm_5 = 1;
                    e.nimberDoc_6 = transaction.numberDoc;
                    e.codeKassir_7 = Integer.parseInt(SettingAppE.instance.user_id);
                    e.info_doc_26 = "1/" + transaction.numberCheck + "/" + transaction.numberDoc;
                    e.code_firma_27 = SettingAppE.instance.getPointId();
                    double sum = UtilsOmsk.round(mProduct.price * mProduct.selectAmount, 2);
                    e.amount_rount_prod_12 = sum;
                    e.price_discount_rub_16 = sum;
                    e.summ_itogo_20 = sum;
                    e.priee_discount_15 = mProduct.price;
                    e.kkm_operation_13 = 0;
                    e.code_group_print_17 = 1;
                    e.kmm_section_21 = 1;
                    e.code_vid_code_23 = 1;
                    transaction.list.add(e);
                }


                Event_40_41 e = new Event_40_41();//куплено за нал
                e.numberAction_1 = Iterator.getId();
                e.dateAction_2 = transaction.getDate();
                e.timeAction_3 = transaction.getTime();
                e.int_1_4 = 41;
                e.code_pm_5 = 1;
                e.nimberDoc_6 = transaction.numberDoc;
                e.codeKassir_7 = Integer.parseInt(SettingAppE.instance.user_id);
                //e.number_card_8 = numberCard;
                if(payment_type==0){
                    e.code_payment_9 = "1";// codePayment;nal
                }else {
                    e.code_payment_9 = "4";// codePayment;безнал
                }

                if (payment_type == null) {
                    payment_type = 0d;
                }
                e.type_operation_payment_10 = payment_type;
                e.summ_doc_11 = totalPriceE;
                e.summ_client_12 = totalPriceE;
                e.info_doc_26 = "1/" + transaction.numberCheck + "/" + transaction.numberDoc;
                e.code_firma_27 = SettingAppE.instance.getPointId();
                e.kkm_operation_13 = 0;
                e.type_doc_23 = 1;
                transaction.list.add(e);

                Event_42_55 ee = new Event_42_55();// закрытие документа

                double d = 0d, summ = 0d;

                for (MProduct mProduct : selectListProduct) {
                    d = d + mProduct.selectAmount;
                    summ = summ + mProduct.selectAmount * mProduct.price;
                }

                ee.amount_prod_11 = d;
                ee.summ_doc_12 = UtilsOmsk.round(summ, 2);
                ee.number_smena_14 = transaction.numberSession;
                ee.code_group_print_doc_17 = 1;
                ee.sum_bonusl_18 = "0";
                ee.numberAction_1 = Iterator.getId();
                ee.dateAction_2 = transaction.getDate();
                ee.timeAction_3 = transaction.getTime();
                ee.int_1_4 = 55;
                ee.code_pm_5 = 1;
                ee.nimberDoc_6 = transaction.numberDoc;
                ee.codeKassir_7 = Integer.parseInt(SettingAppE.instance.user_id);
                //ee.number_cate_8 = numberCard;
                ee.operation_kkm_13 = 0;
                ee.code_doc_23 = 1;
                ee.summ_doc_12 = totalPriceE;
                ee.info_doc_26 = "1/" + transaction.numberCheck + "/" + transaction.numberDoc;
                ee.code_firma_27 = SettingAppE.instance.getPointId();
                transaction.list.add(ee);


                FactoryBlender.AddMessage("Создание данных для отсылки на сервер");

                UtilsOmsk.appenderReportFile(transaction.list);
                FactoryBlender.AddMessage("Сохранение данных на диск.");



                SettingAppE.instance.selectProduct.clear();
                SettingAppE.save();
                if (Controller.Curnode instanceof Cmainform) {
                    ((Cmainform) Controller.Curnode).refrachCheck();
                }
                FactoryBlender.stop();
                SenfBitnic();
            } else {
                if (exception != null) {
                    Controller.WriteMessage("Печать чека - ошибка");
                    DialogFactory.ErrorDialog(exception);
                }
            }

        }
    }

    public static void SenfBitnic() {
        if(SettingAppE.instance.getProfile()==1){
            Charset cset = Charset.forName("windows-1251");
            List<String> lines1 =new ArrayList<>();
            try {
                lines1= Files.readAllLines(Paths.get(Pather.sender_report_file), cset);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            if(lines1.size()==0){
                log.error("ВНИМАНИЕ: файл отправки данных на сервер, не содержит строк, или нарушен его формат,проерьте выходной файл.");
                return;
            }
            new SenderFileBitnic().send(lines1);
        }
    }
}
