package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask2;
import org.apache.log4j.Logger;


public class TestConnect extends BaseKassa {

    private static final Logger log = Logger.getLogger(TestConnect.class);

    public void print(){


        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask2<Void,Integer,Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();
            try {
                fptr.create();
                SetWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
                fptr.destroy();

                return true;
            }catch (Exception ex){
                log.error(ex);
                exception=ex;
                System.out.println(ex);
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {


            FactoryBlender.Run();
            Controller.WriteMessage("Тестирование соединения");
        }

        @Override
        public void onPostExecute(Boolean params) {

            FactoryBlender.stop();
            if(params){
                Controller.WriteMessage("Тестирование соединения - успешно");
            }else {

                if(exception!=null){
                    Controller.WriteMessage("Тестирование соединения - щшибка");
                    DialogFactory.ErrorDialog(exception);
                }
            }
        }
    }

}
