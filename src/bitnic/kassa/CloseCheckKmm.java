package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;;
import bitnic.senders.AsyncTask2;
import org.apache.log4j.Logger;

public class CloseCheckKmm extends BaseKassa {
    private static final Logger log = Logger.getLogger(CloseCheckKmm.class);
    public void print() {
        new MyWorker().execute(null);
    }

    private class MyWorker extends AsyncTask2<Void, Integer, Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();
                SetWorkDir(fptr);
                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);
                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);
                // Отменяем чек, если уже открыт. Ошибки "Неверный режим" и "Чек уже закрыт"
                // не являются ошибками, если мы хотим просто отменить чек
                //("Отмена чека...");
                try {
                    if (fptr.CancelCheck() < 0) {
                        checkError(fptr);
                    }
                } catch (Exception e) {
                    int rc = fptr.get_ResultCode();
                    if (rc != -16 && rc != -3801) {
                        throw e;
                    }
                }

                Printstate(fptr);
                return true;
            } catch (Exception e) {
                log.error(e);
                exception=e;
                e.printStackTrace();
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {

            Controller.WriteMessage("Закрытие чека");
            FactoryBlender.Run();
        }

        @Override
        public void onPostExecute(Boolean params) {
            FactoryBlender.stop();
            if(params==false&&exception!=null){
                DialogFactory.ErrorDialog(exception);
                Controller.WriteMessage("Закрытие чека - ошибка");
            }else {
                Controller.WriteMessage("");
            }
        }
    }
}
