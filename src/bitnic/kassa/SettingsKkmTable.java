package bitnic.kassa;

import bitnic.blenderloader.FactoryBlender;
import bitnic.utils.support;
import com.atol.drivers.fptr.Fptr;
import com.atol.drivers.fptr.IFptr;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.senders.AsyncTask2;
import bitnic.senders.IAction;
import bitnic.utils.tt31;
import bitnic.utils.tt32;
import org.apache.log4j.Logger;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SettingsKkmTable extends BaseKassa {

    private static final Logger log = Logger.getLogger(SettingsKkmTable.class);
    public static final String stroka="строка",chislo="число";
    private List<ItemSettingKkm> res=new ArrayList<>();
    private IAction<List<ItemSettingKkm>> iAction;
    private List<ItemSettingKkm> listString=new ArrayList<>();
    private List<ItemSettingKkm> listValue=new ArrayList<>();


    public void getSettings(IAction<List<ItemSettingKkm>> iAction) {
        this.iAction = iAction;

        Type listType = new TypeToken<ArrayList<ItemSettingKkm>>(){}.getType();
        listString = new Gson().fromJson(tt31.json, listType);
        listValue = new Gson().fromJson(tt32.json, listType);



        new MyWorker().execute(null);





    }
    private class MyWorker extends AsyncTask2<Void,Integer,Boolean> {

        @Override
        public Boolean doInBackground(Void... params) {
            IFptr fptr = new Fptr();

            try {
                fptr.create();

                SetWorkDir(fptr);


                // Подключаемся к устройству
                if (fptr.put_DeviceEnabled(true) < 0)
                    checkError(fptr);

                // Проверка связи
                if (fptr.GetStatus() < 0)
                    checkError(fptr);

                if (fptr.put_UserPassword("00000030") < 0) {
                    checkError(fptr);
                }

                if (fptr.put_Mode(IFptr.MODE_PROGRAMMING) < 0) {
                    checkError(fptr);
                }
                if (fptr.SetMode() < 0) {
                    checkError(fptr);
                }
                ///////////////////////////////////// url




                int i=0;
                for (i = 0; i < listString.size(); i++) {

                    int ss = fptr.put_CaptionPurpose(listString.get(i).index);
                    if (ss == 0) {
                        if (fptr.GetCaption() == 0) {
                            ItemSettingKkm tity = new ItemSettingKkm();
                            tity.name = fptr.get_CaptionName();
                            tity.table = 1;
                            tity.index = listString.get(i).index;
                            tity.param=stroka;
                            tity.description=listString.get(i).description;
                            tity.value = fptr.get_Caption();
                            res.add(tity);
                        }

                    }
                }//374

                for (i = 0; i < listValue.size(); i++) {
                    if (fptr.put_ValuePurpose(listValue.get(i).index) == 0) {
                        if (fptr.GetValue() == 0) {

                            ItemSettingKkm tity = new ItemSettingKkm();
                            tity.name = fptr.get_ValueName();
                            tity.table = 2;
                            tity.param=chislo;
                            tity.index = listValue.get(i).index;
                            tity.value = String.valueOf((int) fptr.get_Value());
                            tity.description=listValue.get(i).description;
                            res.add(tity);
                        }
                    }
                }
///////////////////////



                return true;
            }catch (Exception ex){
                log.error(ex);
                exception=ex;
                System.out.println(ex);
                return false;
            } finally {
                fptr.ResetMode();
                fptr.destroy();
            }
        }

        @Override
        public void onPreExecute() {

            Controller.WriteMessage(support.str("name35"));
            FactoryBlender.Run();

        }

        @Override
        public void onPostExecute(Boolean params) {

            FactoryBlender.stop();
            if(params){
                Controller.WriteMessage(support.str("name35"));
                iAction.action(res);
            }else {

                if(exception!=null){
                    Controller.WriteMessage("Настройки кассы - ошибка");
                    DialogFactory.ErrorDialog(exception);
                }
            }
        }
    }
}
