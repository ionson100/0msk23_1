package bitnic.updateapp;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.DesktopApi;
import bitnic.utils.Pather;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UpdateApp {
    private static final Logger log = Logger.getLogger(UpdateApp.class);

    public void run(Controller controller) {
        String patch = Pather.curdir + File.separator + "static/share/app";
        File file = new File(patch);
        if (file.exists() == false) {
            DialogFactory.InfoDialog("Обновление", "Папки: static/share/app не обнаружено\nОбновление прервано!");
            return;
        }

        File[] files = file.listFiles();
        List<File> filesApp = new ArrayList<>();
        int curVersion = 100000;
        try {
            curVersion = Integer.parseInt(SettingAppE.instance.getVersion().replace(".", ""));
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e);
        }
        for (File f : files) {
            int i = f.getName().lastIndexOf('.');
            if (i > 0) {
                String ext = f.getName();
                System.out.println(ext);
                if (DesktopApi.getOs().isLinux()) {
                    if (ext.indexOf(".tar.gz") != -1) {

                        String ss = f.getName().replace(".tar.gz", "").replace(".", "");
                        int ver = Integer.parseInt(ss);
                        if (curVersion < ver) {
                            filesApp.add(f);
                        }
                    }
                } else {
                    if (ext.indexOf(".rar") != -1) {
                        String ss = f.getName().replace(".rar", "").replace(".", "");
                        int ver = Integer.parseInt(ss);
                        if (curVersion < ver) {
                            filesApp.add(f);
                        }
                    }

                }
            }
        }
            if (filesApp.size() == 0) {
                DialogFactory.InfoDialog("Обновление", "Файлы для обновления отсутствуют");
                return;
            }
            DialogFactory.RollBackDialog(filesApp, o -> {

                if (DesktopApi.getOs().isLinux()) {
                    RollBackApp.UpdateCoreLinux((File) o, log);
                } else {
                    RollBackApp.UpdateCoreWindows((File) o, log);
                }

                return false;
            });
        }
    }
