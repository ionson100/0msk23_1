package bitnic.updateapp;

import bitnic.core.Controller;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.DesktopApi;
import bitnic.utils.Pather;
import javafx.scene.control.Button;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class StarterUpdate {
    private static final Logger log = Logger.getLogger(StarterUpdate.class);

    public static void CheckUpdate(Controller controller){
        String patch = Pather.curdir + File.separator + "static/share/app";
        File file = new File(patch);
        if (file.exists() == false) {
            log.info("Обновление Папки: static/share/app не обнаружено");
            return;
        }

        File[] files = file.listFiles();
        List<File> filesApp = new ArrayList<>();
        for (File f : files) {
            int i = f.getName().lastIndexOf('.');
            if (i > 0) {

                if(DesktopApi.getOs().isLinux()){
                    if (f.getName().indexOf("tar.gz")!=-1) {
                        filesApp.add(f);
                    }
                }else {
                    if (f.getName().indexOf("rar")!=-1) {
                        filesApp.add(f);
                    }
                }
            }
        }
        if (filesApp.size() == 0) {
            log.info("Обновление Файлы для обновления отсутствуют");
            return;
        }

        for (File f : filesApp) {
            String name=f.getName().replace(".rar","").replace(".tar.gz","");
            try {

                int cur=Integer.parseInt(SettingAppE.instance.getVersion().replace(".",""));
                int app=Integer.parseInt(name.replace(".",""));
                if(cur<app){
                    Button button=new Button("Обновление");
                    button.getStyleClass().add("update_button");
                    button.setUserData("312873");
                    button.setOnAction(event -> controller.onUpdateApp(event));
                    controller.panel_buttons.getChildren().add(button);
                    button.setPrefHeight(controller.panel_buttons.getPrefHeight());

                    break;
                }
            }catch (Exception e){
                log.error(e);
            }
        }
    }


}
