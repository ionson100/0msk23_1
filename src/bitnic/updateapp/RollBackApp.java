package bitnic.updateapp;

import bitnic.core.ExeScript;
import bitnic.dialogfactory.DialogFactory;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;

import bitnic.utils.DesktopApi;
import bitnic.utils.Pather;
import org.apache.log4j.*;

public class RollBackApp {
    private static final Logger log = Logger.getLogger(RollBackApp.class);

    public void run() {


        //log.info("dsdkjds djskdjksjdsjd sjd");

        String patch = Pather.curdir + File.separator + "static/share/app";
        File file = new File(patch);
        if (file.exists() == false) {
            DialogFactory.InfoDialog("Обновление", "Папки: static/share/app не обнаружено\nОбновление прервано!");
            return;
        }


        File[] files = file.listFiles();
        List<File> filesApp = new ArrayList<>();
        for (File f : files) {
            int i = f.getName().lastIndexOf('.');
            if (i > 0) {
                String ext = f.getName();
                System.out.println(ext);

                if (DesktopApi.getOs().isLinux()){
                    if (ext.indexOf(".tar.gz")!=-1) {
                        filesApp.add(f);
                    }
                }else {
                    if (ext.indexOf(".rar")!=-1) {
                        filesApp.add(f);
                    }
                }


            }
        }

        if (filesApp.size()== 0) {
            DialogFactory.InfoDialog("Обновление", "Файлы для обновления отсутствуют");
            return;
        }

        DialogFactory.RollBackDialog(filesApp, o -> {
            if (DesktopApi.getOs().isLinux()){
                UpdateCoreLinux((File) o, log);
            }else {
                UpdateCoreWindows( (File) o, log);
            }

            return false;
        });
    }

    public static void UpdateCoreWindows(File file, Logger logger) {
        if (file == null) return;
        String OUTPUT_FOLDER_RAR = Pather.curdir+"\\temp";
        ExtractArchive.ExtractArchive(file.getPath(), OUTPUT_FOLDER_RAR);


        String name = ManagementFactory.getRuntimeMXBean().getName();
        String pid=name.substring(0,name.indexOf("@"));
        logger.info("pid application - "+pid);
        String source=OUTPUT_FOLDER_RAR+File.separator+file.getName().substring(0,file.getName().lastIndexOf("."))+"/*";
        logger.info("source directory - "+source);
        String target="E:\\omsk1\\0msk23_1\\out\\artifacts\\0msk23_1";//Pather.targetAppDirectory;
        String executr="E:\\omsk1\\0msk23_1\\out\\artifacts\\0msk23_1\\0msk23_1.jar";
        logger.info("exe rar  - "+executr);

        String exe="e:\\assa.bat ";
        logger.info("exe sh  - "+exe);
        String ss=exe +pid+ " "+source+" "+target+" "+executr;
        logger.info("runer string - "+ss);
        ExeScript testScript = new ExeScript();
        testScript.runScript(ss);

    }

    public static void UpdateCoreLinux(File file, Logger logger) {
        if (file == null) return;


        String name = ManagementFactory.getRuntimeMXBean().getName();
        String pid=name.substring(0,name.indexOf("@"));
        logger.info("pid application - "+pid);
        String source=file.getPath();
        logger.info("source directory - "+source);
        String target=Pather.curdir+"/omsk";
        String executr=Pather.curdir+"/omsk/omsk.jar";
        logger.info("exe rar  - "+executr);



        String exe="sh "+Pather.curdir+"/appinstall.sh ";
        logger.info("exe sh  - "+exe);
        String ss=exe +pid+ " "+source+" "+target+" "+executr+" &";
        logger.info("runer string - "+ss);
        ExeScript testScript = new ExeScript();

        testScript.runScript(ss);

    }
}


