package bitnic.updateapp;

import bitnic.core.Controller;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.util.Duration;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.DesktopApi;
import bitnic.utils.Pather;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class TimerAppUpdate {
    private static final Logger log = Logger.getLogger(StarterUpdate.class);

    Timeline timeline;
    private Controller controller;

    public void run(int millis, Controller controller){
        timeline = new Timeline (
                new KeyFrame(
                        Duration.millis(millis), //1000 мс * 60 сек = 1 мин
                        ae -> {
                            activate();

                        }
                )
        );
        this.controller = controller;
        timeline.play();
    }

    public void stop(){
        timeline.stop();
    }

    private synchronized void activate(){

        HttpsURLConnection conn;
        try{
            String idpoint= String.valueOf(SettingAppE.instance.getPointId());
            String version=SettingAppE.instance.getVersion();
            URL url = new URL("https://174.138.11.176/pos_info?point="+idpoint+"&ver="+version);
            conn = (HttpsURLConnection) url.openConnection();
            conn.setInstanceFollowRedirects(false);
            conn.setReadTimeout(15000 /*milliseconds*/);
            conn.setConnectTimeout(20000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
            conn.connect();
            final int status = conn.getResponseCode();
            int ch;
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                while ((ch = reader.read()) != -1) {
                    sb.append((char) ch);
                }
            } catch (Exception ignored) {

            }
            String ssres = sb.toString();

            BodyFiles bodyFiles=null;
            try{
                 bodyFiles = UtilsOmsk.getGson().fromJson(ssres,BodyFiles.class);
            }catch (Exception ex){
                log.error(ssres);
                log.error(ex);
                return;
            }
            for (String file : bodyFiles.files) {
                log.info("file update - "+file);
            }

            for (String file : bodyFiles.files) {

                String s=file.substring(23);
                s= Pather.curdir+File.separator+s;
                if (DesktopApi.getOs().isLinux()){
                    s=s.replace("\\","/");
                }else {
                    s=s.replace("/","\\");
                }
                File dir=new File(s);
                if(dir.exists()==false){
                    String finalS = s;
                    new Thread(() -> downloadFile(file, finalS)).start();
                }
            }
        }catch (Exception ex){
            log.error(ex);
           ex.printStackTrace();
        }
    }

    private  void downloadFile(String urlstr,String path){

        HttpsURLConnection con=null;
        String folder =null;
        if(DesktopApi.getOs().isLinux()){
            folder =path.substring(0,path.lastIndexOf("/"));
        }else {
            folder =path.substring(0,path.lastIndexOf("\\"));
        }

        if(folder==null) return;

        File dir=new File(folder);

        if(dir.exists()==false){
           dir.mkdirs();
        }

        if(new File(folder).exists()==false){
            return;
        }

        try {
            int count;
            URL url = new URL(urlstr);
            con = (HttpsURLConnection) url.openConnection();
            con.setInstanceFollowRedirects(false);
            con.setReadTimeout(15000 /*milliseconds*/);
            con.setConnectTimeout(20000 /* milliseconds */);
            con.setRequestMethod("GET");
            con.connect();
            int lenghtOfFile =  con.getContentLength() + 100;
            InputStream input = con.getInputStream();
            OutputStream output = new FileOutputStream(path);
            byte data[] = new byte[lenghtOfFile];
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();

            int res=path.indexOf("static"+File.separator+"share"+ File.separator+"app"+File.separator);
            if(res==-1) return;
            String filename=new File(path).getName();
            String intstr=filename.replace(".rar","").replace(".tar.gz","").replace(".","");
            String intcur=SettingAppE.instance.getVersion().replace(".","");

            try {
                int ser=Integer.parseInt(intstr);
                int cur=Integer.parseInt(intcur);
                if(ser>cur){
                    boolean isButton = false;
                    for (Node node : controller.panel_buttons.getChildren()) {
                        if(node.getUserData()!=null&&node.getUserData().toString().equals("312873")){
                            isButton=true;
                            break;
                        }
                    }
                    if(isButton==false){
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                Button button=new Button("Обновление");
                                button.getStyleClass().add("update_button");
                                button.setUserData("312873");
                                button.setOnAction(event -> controller.onUpdateApp(event));
                                controller.panel_buttons.getChildren().add(button);
                                button.setPrefHeight(controller.panel_buttons.getPrefHeight());
                            }
                        });
                    }
                }
            }catch (Exception ex){
               log.error(ex);
            }
        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
        }finally {
            if (con != null) {
                con.disconnect();
            }
        }
    }
    public class BodyFiles{
        public List<String> files=new ArrayList<>();
    }

}
