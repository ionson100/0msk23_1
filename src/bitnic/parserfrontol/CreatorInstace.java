package bitnic.parserfrontol;


import bitnic.dialogfactory.DialogFactory;
import bitnic.model.MMarketingAction;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by USER on 10.01.2018.
 */
public class CreatorInstace {
    public static void Create(Object sd, CacheMetaDateFrontol currentFrontol, String s) throws IllegalAccessException {

//        if(currentFrontol.aClass== MProduct.class){
//            int dd=4;
//        }

        try{
            if(s==null||s.trim().length()==0) return;
            String[] ss=s.split(";",-1);

            for (int i = 0; i < ss.length; i++) {

                if(currentFrontol.fieldMap.containsKey(i+1)){
                    Field f= (Field) currentFrontol.fieldMap.get(i+1);
                    Class d=f.getType();
                    if(d == String.class){
                        f.set(sd,ss[i]);
                    }
                    else if(d == Integer.class||d == int.class){
                        String x=ss[i];
                        try{
                            f.set(sd,Integer.parseInt(ss[i]));
                        }catch (Exception ex){
                           // System.out.println(ex.getMessage());
                            f.set(sd,0);
                        }
                    }
                    else if(d == Double.class||d == double.class){
                        String x=ss[i];
                        try {
                            f.set(sd, Double.parseDouble(x));
                        }catch (Exception ex){
                           // System.out.println(ex.getMessage());
                            f.set(sd,0d);
                        }
                    }
                    else if(d == Date.class){
                        String x=ss[i];
                        //26.09.2017;26.09.2018
                        if (x==null||x.trim().length()==0){
                            f.set(sd,null);
                        }else {

                            if(sd.getClass()==MMarketingAction.class){
                                if(i==1||i==2){//dd MM yyyy HH:mm:ss
                                    SimpleDateFormat parser = new SimpleDateFormat("dd MM yyyy HH:mm:ss");
                                    try {
                                        x=x.replace("."," ")+" 00:00:01";
                                        Date date = parser.parse(x);
                                        f.set(sd,date);
                                    }catch (Exception ex){
                                        System.out.println(x+"to date "+ex.getMessage());
                                        f.set(sd,null);
                                    }
                                }
                            }

                        }

                    }
                }
                else {
                }
            }
        }catch (Exception ex){
            DialogFactory.ErrorDialog(ex);
        }

    }
}
