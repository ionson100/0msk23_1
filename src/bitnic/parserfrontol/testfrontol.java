package bitnic.parserfrontol;


import bitnic.model.ModelCollection;
import bitnic.senders.IAction;
import bitnic.senders.SenderFtp;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ion100 on 10.01.2018.
 */
public class testfrontol {

    Map<String, CacheMetaDateFrontol> dateFrontolMap = new HashMap<>();

   List<Class> classes = null;

    public void test(IAction refrash) {
        try {
            classes = ModelCollection.classes;// CreatorTable.getClasses("bitnic.model");
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Class aClass : classes) {
            Annotation f = aClass.getAnnotation(Frontol.class);
            if (f != null) {
                CacheMetaDateFrontol ff = CacheDictionaryFrontol.GetCacheMetaDate(aClass);
                dateFrontolMap.put(ff.command, ff);
            }
        }


        SenderFtp senderFtp = new SenderFtp();
        senderFtp.send(dateFrontolMap,refrash);

    }
}
