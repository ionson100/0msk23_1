package bitnic.parserfrontol;



import java.util.Dictionary;
import java.util.Hashtable;

public class CacheDictionaryFrontol {

    private static final Object lock = new Object();

    private static final Dictionary<String, CacheMetaDateFrontol> dic = new Hashtable();

    public static CacheMetaDateFrontol GetCacheMetaDate(Class aClass) {
        if (dic.get(aClass.getName()) == null) {
            synchronized (lock) {
                if (dic.get(aClass.getName()) == null) {
                    dic.put(aClass.getName(), new CacheMetaDateFrontol<>(aClass));
                }
            }
        }
        return dic.get(aClass.getName());
    }


}
