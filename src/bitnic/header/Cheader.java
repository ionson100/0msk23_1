package bitnic.header;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogFactory;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Created by ion on 20.01.2018.
 */
public class Cheader implements Initializable {
    private static final Logger log = Logger.getLogger(Cheader.class);
    public Label label_time;
    public TextField status_app;
    public Label label_user;
    public Button bt_menu_help;
    private static Cheader instance;
    public static void RefrashUser(){
        instance.label_user.setText(SettingAppE.instance.getUserName());
    }

    public void onAction(ActionEvent actionEvent) {
        String id = "";
        if (actionEvent.getSource() instanceof Node) {
            id = ((Node) actionEvent.getSource()).getId();
        } else if (actionEvent.getSource() instanceof MenuItem) {
            id = ((MenuItem) actionEvent.getSource()).getId();
        }
        if (id.equals("bt_menu_keyboard")) {
           // DialogKeyBoard.show();
            UtilsOmsk.openKeyBoard();
        } else if (id.equals("bt_menu_help")) {

        }else if(id.equals("bt_menu_chaat")){
            Controller.OpenChat();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        instance=this;
        bt_menu_help.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                DialogFactory.InfoDialog("Страница помощи","Не имеет реализации.");
            }
        });
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Date date = new Date();
                        SimpleDateFormat formatForTimeNow = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                 label_time.setText(formatForTimeNow.format(date));
                            }
                        });
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        log.error(e);
                        e.printStackTrace();
                        DialogFactory.ErrorDialog(e);
                    }
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }
}
