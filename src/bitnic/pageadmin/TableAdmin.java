package bitnic.pageadmin;

import bitnic.pagetableproduct.IRefrashLoader;
import bitnic.utils.BuilderTable;
import bitnic.utils.BuilderTable2;
import bitnic.core.Main;
import bitnic.dialogfactory.DialogFactory;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;

import bitnic.model.*;
import bitnic.orm.*;
import bitnic.utils.UtilsOmsk;
import bitnic.table.DisplayViewer;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class TableAdmin extends GridPane implements Initializable, IRefrashLoader {

    private static final Logger log = Logger.getLogger(TableAdmin.class);

    BuilderTable builderTable=new BuilderTable();
    ListChangeListener listener;

    ArrayList<Class> classes = new ArrayList<Class>() {{
        add(MProduct.class);
        add(MUser.class);
        add(MTaxGroupRates.class);
        add(MTaxrates.class);
        add(MTaxGroup.class);
        add(MBarcodes.class);
        add(MMarketingAction.class);
    }};
    @FXML
    public ComboBox com_box;
    @FXML
    public TextArea text_sql;
    @FXML
    public Button bt_execute;
    @FXML
    public javafx.scene.control.TableView table;
    @FXML
    public GridPane grid1;
    GridPane pane;

    private Class currentClass;

    public TableAdmin() {
        try {
        InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
        ResourceBundle bundle = new PropertyResourceBundle(inputStream);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("table_admin.fxml"),bundle);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        com_box.getSelectionModel().selectedItemProperty()
                .addListener((ChangeListener<Class>) (observable, oldValue, newValue) -> {
                    currentClass = newValue;
                    System.out.println("Value is: " + newValue);
                    List<Object> list = Configure.GetSession().getList(newValue, null);
                    builderTable.bubligum(list, table);

                });
        ObservableList<Class> list1 = FXCollections.observableArrayList();
        for (Class<?> aClass : classes) {
            DisplayViewer viewer = aClass.getAnnotation(DisplayViewer.class);
            if (viewer != null) {
                list1.add(aClass);
            }
        }
        com_box.setItems(list1);
        com_box.getSelectionModel().select(0);
        UtilsOmsk.resizeNode(this, grid1);
        bt_execute.setOnAction(event -> {
            if (builderTable.listener != null) {
                table.getColumns().removeListener(builderTable.listener);
            }
            if (text_sql.getText().trim().length() == 0) return;
            try {
                currentClass = null;
                Connection c = DriverManager.getConnection(Configure.CON_STR);
                c.setAutoCommit(false);
                Statement stmt = c.createStatement();
                ResultSet rs = stmt.executeQuery(text_sql.getText());//"select * from product"text_sql.getText()
                List<Object> list = new ArrayList<>();
                ResultSetMetaData rsmd = rs.getMetaData();
                List<BuilderTable2.MyField> fields = new ArrayList<>(rsmd.getColumnCount());
                for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
                    String s1 = rsmd.getColumnName(i);
                    String s3 = rsmd.getColumnTypeName(i);
                    if (s3.equals("INTEGER")) {
                        fields.add(new BuilderTable2.MyField(s1, Integer.class));
                    } else if (s3.equals("TEXT")) {
                        fields.add(new BuilderTable2.MyField(s1, String.class));
                    } else if (s3.equals("REAL")) {
                        fields.add(new BuilderTable2.MyField(s1, Double.class));
                    }
                }

                BuilderTable2 sbt = new BuilderTable2();

                if (currentClass == null) {
                    currentClass = sbt.createClasse(fields, Main.class);
                }
                Field[] fieldsser=currentClass.getDeclaredFields();
                while (rs.next()) {
                    Object o = currentClass.newInstance();
                    for (Field field : fieldsser) {
                        Object val = rs.getObject(field.getName());
                        field.set(o,val);
                    }
                    list.add(o);
                }
                rs.close();
                stmt.close();
                c.close();
                sbt.build(list, table);
            } catch (Exception e) {
                log.error(e);
                e.printStackTrace();
                DialogFactory.ErrorDialog(e.getMessage());
            }
        });
    }

    @Override
    public void refrash() {
        int i=com_box.getSelectionModel().getSelectedIndex();
        com_box.getSelectionModel().select(i);
        System.out.println("select combo- "+i);
    }
}
