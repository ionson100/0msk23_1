package bitnic.keyboard;

import bitnic.dialogfactory.DialogFactory;
import bitnic.utils.UtilsOmsk;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import bitnic.model.MProduct;
import bitnic.pagesale.Cmainform;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;

import bitnic.transaction.Transaction;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class KeyKeyB implements Initializable {

    private static final Logger log = Logger.getLogger(KeyKeyB.class);
    public IAction<String> iActionKeyPress;
    public IAction<String> iActionDeletecheck;
    public Button bt_print_check,bt_clear_check,bt_print_check2;
    public Button b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14;

    public KeyKeyB(){

    }
    public TextField kb_text_field;

    public  void keypress(ActionEvent actionEvent) {


        try {




            Button button = (Button) actionEvent.getSource();
            String ss = button.getUserData().toString();
            if (ss.equals("0")) {
               iActionKeyPress.action("0");
            } else if (ss.equals("1")) {
                iActionKeyPress.action("1");
            } else if (ss.equals("2")) {
                iActionKeyPress.action("2");
            } else if (ss.equals("3")) {
                iActionKeyPress.action("3");
            } else if (ss.equals("4")) {
                iActionKeyPress.action("4");
            } else if (ss.equals("5")) {
                iActionKeyPress.action("5");
            } else if (ss.equals("6")) {
                iActionKeyPress.action("6");
            } else if (ss.equals("7")) {
                iActionKeyPress.action("7");
            } else if (ss.equals("8")) {
                iActionKeyPress.action("8");
            } else if (ss.equals("9")) {
                iActionKeyPress.action("9");
            } else if (ss.equals(",")) {

            } else if (ss.equals(",,")) {
                iActionKeyPress.action("88");

            } else if (ss.equals(",,,")) {
                iActionKeyPress.action("888");

            } else if (ss.equals("8888")) {
                iActionKeyPress.action("8888");
            }


        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            DialogFactory.ErrorDialog(e);
        }


    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        SettingAppE setting=SettingAppE.instance;
        Cmainform.keyKeyB=this;

        String css="-fx-font-size: "+setting.fontSizeBigButton;


        UtilsOmsk.painter3d(bt_print_check,bt_print_check2,bt_clear_check);


        if(setting.isTypeshow){

            b1.getStyleClass().add("button_key_big_3d");
            b2.getStyleClass().add("button_key_big_3d");
            b3.getStyleClass().add("button_key_big_3d");
            b4.getStyleClass().add("button_key_big_3d");
            b5.getStyleClass().add("button_key_big_3d");
            b6.getStyleClass().add("button_key_big_3d");
            b7.getStyleClass().add("button_key_big_3d");
            b8.getStyleClass().add("button_key_big_3d");
            b9.getStyleClass().add("button_key_big_3d");
            b10.getStyleClass().add("button_key_big_3d");
            b11.getStyleClass().add("button_key_big_3d");
            b12.getStyleClass().add("button_key_big_3d");
            b13.getStyleClass().add("button_key_big_3d");
            b14.getStyleClass().add("button_key_big_3d");


        }else {
            b1.getStyleClass().add("button_key_big");
            b1.getStyleClass().add("button_key_big");
            b2.getStyleClass().add("button_key_big");
            b3.getStyleClass().add("button_key_big");
            b4.getStyleClass().add("button_key_big");
            b5.getStyleClass().add("button_key_big");
            b6.getStyleClass().add("button_key_big");
            b7.getStyleClass().add("button_key_big");
            b8.getStyleClass().add("button_key_big");
            b9.getStyleClass().add("button_key_big");
            b10.getStyleClass().add("button_key_big");
            b11.getStyleClass().add("button_key_big");
            b12.getStyleClass().add("button_key_big");
            b13.getStyleClass().add("button_key_big");
            b14.getStyleClass().add("button_key_big");

        }

        b1.setStyle(css);
        b2.setStyle(css);
        b3.setStyle(css);
        b4.setStyle(css);
        b5.setStyle(css);
        b6.setStyle(css);
        b7.setStyle(css);
        b8.setStyle(css);
        b9.setStyle(css);
        b10.setStyle(css);
        b11.setStyle(css);
        b12.setStyle(css);
        b13.setStyle(css);

        bt_clear_check.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                iActionDeletecheck.action(null);
            }
        });

    }

    public void aaaaaa(ActionEvent actionEvent) {
        //  if(SettingsE.Get().selectProduct.size()==0) return;
        List<MProduct> selectProduct=new ArrayList<>(SettingAppE.instance.selectProduct);
        new Transaction().saleCache(selectProduct);

    }
}
