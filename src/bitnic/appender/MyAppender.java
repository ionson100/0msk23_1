package bitnic.appender;

import bitnic.core.Main;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;

import java.util.Date;

public /*static*/ class MyAppender extends AppenderSkeleton {


    public void close() {
    }

    public boolean requiresLayout() {
        return false;
    }

    @Override
    protected void append(LoggingEvent loggingEvent) {
        //2018-03-14 09:12:46 ERROR StateKassaMoney:60 - bitnic.kassa.BaseKassa$DriverException: [-3] Порт недоступен
        if (loggingEvent.getLevel() != Level.ERROR)
            return;
        Platform.runLater(() -> {
            String msg = String.format("%s#  %s  %s  %s  -  %s ",
                    SettingAppE.instance.getPointId(),
                    UtilsOmsk.getStringTimeForError(new Date(loggingEvent.timeStamp)),
                    loggingEvent.getLevel(),
                    loggingEvent.getLocationInformation().fullInfo,
                    loggingEvent.getMessage());
            if (Main.MyWebSockerError != null) {
                Main.MyWebSockerError.send(msg);
            }

        });
    }
}
