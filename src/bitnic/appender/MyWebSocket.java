package bitnic.appender;

import org.apache.log4j.Logger;
import org.java_websocket.client.DefaultSSLWebSocketClientFactory;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.URI;
import java.security.cert.X509Certificate;

public class MyWebSocket {

    private static final Logger log = Logger.getLogger(MyWebSocket.class);

    private WebSocketClient webSocketClien;


    public void start() {

        URI uri;
        try {
            uri = new URI("wss://174.138.11.176/pos_error");
        } catch (Exception e) {
            log.error(e);
            return;
        }
        webSocketClien = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                System.out.println("open");
                log.info("открытие сокета  ошибок");

            }

            @Override
            public void onMessage(String s) {
                System.out.println("message " + s);
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                log.info("закрытие сокета  ошибок");

            }

            @Override
            public void onError(Exception e) {
                log.error(e);
                System.out.println(e.getMessage());
            }
        };

        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }}, null);

            webSocketClien.setWebSocketFactory(new DefaultSSLWebSocketClientFactory(sslContext));
            webSocketClien.connect();

        } catch (Exception e) {
            log.error(e);
            e.printStackTrace();
        }


    }

    public synchronized void send(String error_msg) {
        if (webSocketClien.getConnection ().isOpen ()) {
            webSocketClien.send(error_msg);
        }
    }

    public void stop() {
        webSocketClien.getConnection().close ();
        webSocketClien.close();

    }
}
