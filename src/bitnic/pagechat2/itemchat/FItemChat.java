package bitnic.pagechat2.itemchat;

import bitnic.dialogfactory.DialogFactory;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import bitnic.pagechat2.MItemChat;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FItemChat extends GridPane implements Initializable {
    //    public Label label_date;
//    public Label label_message;
    public ColumnConstraints column;
    private MItemChat item;
    public GridPane host_chat;
    GridPane pane;

    public FItemChat(MItemChat item) {
        this.item = item;
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_chat.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        BubbledLabel bl1 = null;
        if (item.isSelf) {
            bl1 = new BubbledLabel(BubbleSpec.FACE_RIGHT_CENTER);
            column.setHalignment(HPos.RIGHT);
            bl1.setStyle("-fx-font-size: 30;-fx-background-color: #4af17a");
        } else {
            bl1 = new BubbledLabel(BubbleSpec.FACE_LEFT_CENTER);
            column.setHalignment(HPos.LEFT);
            bl1.setStyle("-fx-font-size: 30;-fx-background-color: #f1be21");

        }


        String HTML_A_TAG_PATTERN = "(?i)<a([^>]+)>(.+?)</a>";
        String HTML_A_HREF_TAG_PATTERN = "\\s*(?i)href\\s*=\\s*(\"([^\"]*\")|'[^']*'|([^'\">\\s]+))";

        String str = item.message;

        {
            Pattern MY_PATTERN = Pattern.compile(HTML_A_TAG_PATTERN);
            Matcher m = MY_PATTERN.matcher(str);
            while (m.find()) {
                String s = m.group(1);
                str = str.replace(s, "");
                System.out.println(s);
                System.out.println(str);
            }
        }
        List<Text> listDownload = new ArrayList<>();
        {
            Pattern MY_PATTERN = Pattern.compile(HTML_A_HREF_TAG_PATTERN);
            Matcher m = MY_PATTERN.matcher(item.message);
            while (m.find()) {
                String s = m.group(1);
                Text text = new Text(System.lineSeparator() + "файл для скачивания.");
                text.setId("link_chat");
                text.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        DialogFactory.InfoDialog("dsfsd", "dsfsf");
                    }
                });
                listDownload.add(text);
                String ff = item.message.replace(s, "");
                System.out.println(s);
                System.out.println(ff);
            }
        }


        Text sww = new Text("Дата: 12.12.12.2/.2018" + System.lineSeparator());
        sww.setOnTouchMoved(new EventHandler<TouchEvent>() {
            @Override
            public void handle(TouchEvent event) {

            }
        });
        sww.setStyle("-fx-font-weight: bold");
        Text sw = new Text(str);

        bl1.getChildren().add(sww);
        bl1.getChildren().add(sw);
        for (Text text : listDownload) {
            bl1.getChildren().addAll(text);
        }


        StringBuilder sb = new StringBuilder();
        for (Node node : bl1.getChildren()) {
            if (node instanceof Text) {
                Text t = (Text) node;
                sb.append(t.getText());
            }
        }
        String sdd[] = sb.toString().split("\\n", -1);
        bl1.setMaxHeight(sdd.length * 20);
        int f = 0;
        for (String s : sdd) {
            if (s != null && s.length() > f) {
                f = s.length();
            }
        }
        bl1.setMaxWidth(f * 20d);


        this.getChildren().addAll(bl1);//,bl2,bl3,bl4,bl5,bl6);


    }
}
