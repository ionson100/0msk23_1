package bitnic.pagechat2.itemchat;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.TextFlow;

public class BubbledLabel extends TextFlow {

    private BubbleSpec bs = BubbleSpec.FACE_LEFT_CENTER;
    private double pading = 5.0;
    private boolean systemCall = false;

    public BubbledLabel() {
        super();
        init();
    }


    public BubbledLabel(BubbleSpec bubbleSpec) {
        super();
        this.bs = bubbleSpec;
        init();
    }


    private void init() {
        DropShadow ds = new DropShadow();
        ds.setOffsetX(1.3);
        ds.setOffsetY(1.3);
        ds.setColor(Color.RED);
        setPrefSize(Label.USE_COMPUTED_SIZE, Label.USE_COMPUTED_SIZE);
        shapeProperty().addListener((arg0, arg1, arg2) -> {
            if (systemCall) {
                systemCall = false;
            } else {
                shapeIt();
            }

//				if(arg2.getClass().isAssignableFrom(Bubble.class)){
//					systemCall = false;
//					return;
//				}else{
//					systemCall = true;
//					setShape(new Bubble(bs));
//					System.gc();
//				}

        });

        heightProperty().addListener(arg0 -> {
            if (!systemCall)
                setPrefHeight(Label.USE_COMPUTED_SIZE);
        });

        widthProperty().addListener(observable -> {
            if (!systemCall)
                setPrefHeight(Label.USE_COMPUTED_SIZE);
        });

        shapeIt();
    }

    @Override
    protected void updateBounds() {


        super.updateBounds();
        double d = getWidth();
        //top right  bottom  left
        switch (bs) {

            case FACE_LEFT_CENTER:

                setPadding(new Insets(pading, pading, pading, d / 7));
                break;

            case FACE_RIGHT_CENTER:
                setPadding(new Insets(pading, pading, pading, 40));
                break;

            default:
                break;
        }
    }

    public final double getPading() {
        return pading;
    }

    public void setPading(double pading) {
        if (pading > 25.0)
            return;
        this.pading = pading;
    }

    public BubbleSpec getBubbleSpec() {
        return bs;
    }

    public void setBubbleSpec(BubbleSpec bubbleSpec) {
        this.bs = bubbleSpec;
        shapeIt();
    }

    private final void shapeIt() {
        systemCall = true;
        setShape(new Bubble(bs));
        System.gc();
    }
}


