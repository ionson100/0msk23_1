package bitnic.pagechat2.itemchat;

import javafx.scene.shape.*;

public class Bubble extends Path {

    public Bubble(BubbleSpec bubbleSpec) {
        super();
        switch (bubbleSpec) {
            case FACE_LEFT_CENTER:
                drawRectBubbleLeftCenterIndicator();
                break;
            case FACE_RIGHT_CENTER:
                drawRectBubbleRightCenterIndicator();
                break;
            default:
                break;
        }
    }
    private void drawRectBubbleRightCenterIndicator() {
        getElements().addAll(new MoveTo(3.0f, 2.5f),
                new LineTo(2.8f, 2.4f),
                new VLineTo(1f),
                new HLineTo(0f),
                new VLineTo(4f),
                new HLineTo(2.8f),
                new VLineTo(2.7f),
                new LineTo(3.0f, 2.5f)
        );
    }

    protected double drawRectBubbleIndicatorRule = 0.2;

    private void drawRectBubbleLeftCenterIndicator() {
        getElements().addAll(new MoveTo(1.0f, 2.5f),
                new LineTo(1.2f, 2.4f),
                new VLineTo(1f),
                new HLineTo(2.9f),
                new VLineTo(4f),
                new HLineTo(1.2f),
                new VLineTo(2.7f),
                new LineTo(1.0f, 2.5f)
        );
    }


}

