package bitnic.pagechat2.addmessage;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogBase;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import java.net.URL;
import java.util.ResourceBundle;

public class FAddMessage implements Initializable {
    public TextArea text_arrea;
    public Button bt_close;
    public Button bt_key;
    public Button bt_send;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        bt_close.setOnAction(e -> close());
        text_arrea.textProperty().addListener((observable, oldValue, newValue) -> validate());
    }

    void close() {
        DialogBase.DeleteBlenda();
    }

    void validate() {
        if (text_arrea.getText().trim().length() == 0) {
            bt_send.setDisable(true);
        } else {
            bt_send.setDisable(false);
        }
    }
}
