package bitnic.settings;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface SettingField {
    String title();
    int index();
    int image() default 0;
    int styleTitle() default 0;
    int styleDescription() default 0;
    TypeField typeField() default TypeField.String;
    String descriptions() default "";
    Class IListItem() default String.class;

}

