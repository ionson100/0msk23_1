package bitnic.settings;

public enum TypeField {
    SubMenu,
    BooleanSwitch,
    BooleanCheck,
    Color,
    String,
    Integer,
    Mail,
    Float,
    list

}
