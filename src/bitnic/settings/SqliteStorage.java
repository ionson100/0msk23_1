package bitnic.settings;

import com.google.gson.Gson;
import bitnic.utils.UtilsOmsk;
import bitnic.settingscore.SettingAppE;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

class SqliteStorage {

    private static Object lock=new Object();
    private static String getTableName(Class aClass) {
        return aClass.getName().replace('.', '_')+ SettingAppE.instance.user_id;
    }

    private static boolean existTable(Class aClass) {//;
        List<String> masters = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(Reanimation.GetBaseSettingsPath());
            PreparedStatement statement = connection.prepareStatement("SELECT name FROM sqlite_master");


            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String ss=resultSet.getString(1);
                masters.add(ss);
            }
            statement.close();
            resultSet.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String name = getTableName(aClass);
        return masters.contains(name);
    }

    static Object getObject(Class aClass) throws SQLException {//synchronized

        synchronized (lock){
            Connection connection;
            Object resO = null;
            connection = DriverManager.getConnection(Reanimation.GetBaseSettingsPath());
            if (!existTable(aClass)) {
                try {
                    connection.setAutoCommit(false);
                    String sql = "create table " + getTableName(aClass) + " ( _id integer primary key autoincrement, ass text not null);";
                    PreparedStatement statement = connection.prepareStatement(sql);
                    boolean resultSet = statement.execute();
                    statement.close();

                    resO = aClass.newInstance();
                    Gson sd3 = new Gson();
                    String str = sd3.toJson(resO);
                    sql = "INSERT INTO " + getTableName(aClass) + " (ass) VALUES ('" + str + "');";
                    statement = connection.prepareStatement(sql);
                    resultSet = statement.execute();
                    connection.setAutoCommit(true);
                } catch (Exception ex) {
                    if (connection != null) {
                        try {
                            connection.rollback();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                    throw new RuntimeException("reanimator Create-insert: " + ex.getMessage());
                } finally {
                    if (connection != null)
                        connection.close();
                }
            } else {


                try {

                    String sql = "select ass from " + getTableName(aClass) + " where _id=1";
                    PreparedStatement statement = connection.prepareStatement(sql);
                    ResultSet resultSet = statement.executeQuery();
                    resultSet.next();
                    String str = resultSet.getString(1);
                    if (str == null) {
                        resO = aClass.newInstance();
                    } else {
                        Gson ss = UtilsOmsk.getGson();
                        resO = ss.fromJson(str, aClass);
                    }
                } catch (Exception ex) {
                    throw new RuntimeException("reanimator select  as Get object: " + ex.getMessage() + " class - " + aClass.getName());
                } finally {
                    if (connection != null)
                        connection.close();
                }
            }
            return resO;
        }

    }

    static void saveObject(Object o, Class aClass) throws SQLException {//synchronized

        synchronized (lock){
            Connection connection = DriverManager.getConnection(Reanimation.GetBaseSettingsPath());
            Gson sd3 = new Gson();
            String str = sd3.toJson(o);
            String sql = "UPDATE " + getTableName(aClass) + " SET ass = '" + str + "' WHERE _id = 1";

            try {
                PreparedStatement statement = connection.prepareStatement(sql);
                boolean res = statement.execute();

                statement.close();
            } catch (Exception ex) {

            } finally {
                connection.close();
            }
        }

    }


}


