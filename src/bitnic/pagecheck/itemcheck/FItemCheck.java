package bitnic.pagecheck.itemcheck;

import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.question.ButtonAction;
import bitnic.utils.support;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import bitnic.model.MCheck;
import bitnic.model.MProduct;
import bitnic.senders.IAction;
import bitnic.transaction.Transaction;
import bitnic.utils.UtilsOmsk;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FItemCheck extends GridPane implements Initializable {

    public Label check_number;
    public Label check_type;
    public Label check_date;
    public Label check_summ;
    public Label check_amount;
    public Label check_status;
    public Button bt_show_prod, bt_print_double, bt_cancellation;
    GridPane pane;
    private MCheck mCheck;

    public FItemCheck(MCheck mCheck) {
        this.mCheck = mCheck;
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_check.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        UtilsOmsk.painter3d(bt_show_prod, bt_print_double, bt_cancellation);
        check_number.setText(String.valueOf(mCheck.id));
        check_type.setText("Продажа");
        check_date.setText(mCheck.date);
        double sum = 0;
        for (MProduct mProduct : mCheck.mProducts) {
            sum = sum + mProduct.price * mProduct.selectAmount;
        }
        check_summ.setText(String.valueOf(UtilsOmsk.round(sum, 2)));
        check_amount.setText(String.valueOf(mCheck.mProducts.size()));
        if (mCheck.isRemove) {
            check_status.setText(support.str("name136"));
        } else {
            check_status.setText(support.str("name137"));
        }

        bt_show_prod.setOnAction(event -> DialogFactory.ProductForCheckDialog(mCheck));
        bt_print_double.setOnAction(event -> bitnic.kassa.PrintMyLastCheck.PrintLast(mCheck));
        if (mCheck.isRemove) {
            bt_cancellation.setVisible(false);
        } else {
            bt_cancellation.setOnAction(event -> {
                DialogFactory.QuestionDialog(support.str("name134"), support.str("name135"),
                        new ButtonAction(support.str("name138"), new IAction<java.lang.Object>() {
                            @Override
                            public boolean action(java.lang.Object o) {
                                new Transaction().deleteCheck(mCheck, mCheck.id, new IAction<Integer>() {
                                    @Override
                                    public boolean action(Integer integer) {
                                        return false;
                                    }
                                });
                                return false;
                            }
                        }));
            });
        }
    }
}
