package bitnic.pagecheck;

import bitnic.core.Controller;
import bitnic.pagetableproduct.IRefrashLoader;
import bitnic.utils.Pather;
import bitnic.utils.support;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;

import javafx.util.Callback;
import bitnic.model.MCheck;
import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.pagecheck.itemcheck.FItemCheck;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class FChesk extends GridPane implements Initializable, IRefrashLoader {

    private static final Logger log = Logger.getLogger(FChesk.class);
    public ListView list_check;
    public GridPane grid1;
    public Set<Integer> integers = new HashSet<>();


    GridPane pane;

    public FChesk() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_chesk.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        UtilsOmsk.resizeNode(this, grid1);

        refrashCheck();
        Controller.WriteMessage(support.str("name24"));


    }

    private void refrashCheck() {
        list_check.getItems().clear();
        list_check.setItems(FXCollections.observableArrayList());
        File f = new File(Pather.sender_report_file);
        if (f.exists() == false) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                log.error(e);
                e.printStackTrace();
            }
        }
        Path wiki_path = Paths.get(Pather.sender_report_file);

        Charset charset = Charset.forName("UTF-8");
        List<String> lines = null;
        try {
            lines = Files.readAllLines(wiki_path, charset);
            for (String line : lines) {
                System.out.println(line);
            }
        } catch (IOException e) {
            log.error(e);
            System.out.println(e);
        }
        if (lines == null) return;

        Map<Integer, MCheck> checkMap = new HashMap<>();

        for (String line : lines) {
            if (line.trim().length() < 10) continue;


            String[] strings = line.split(";", -1);

            if (strings[3].equals("63")) {
                checkMap.clear();
                continue;
            }

            if (strings[3].equals("300")) {
                try {
                    int s = Integer.parseInt(strings[4]);
                    integers.add(s);
                } catch (Exception e) {

                    log.error(e);
                }
                continue;
            }


            if (strings[3].equals("11") && strings[12].equals("0")) {

                String date1 = strings[1];
                String date2 = strings[2];
                String amount = strings[10];
                String idProd = strings[7];
                String idDoc = strings[5];
                String code_doc=strings[25];

                int id_doc = Integer.parseInt(idDoc);
                if (checkMap.containsKey(id_doc)) {
                    MCheck check = checkMap.get(id_doc);
                    boolean r = false;
                    for (MProduct mProduct : check.mProducts) {
                        if (mProduct.code_prod.equals(idProd)) {
                            mProduct.selectAmount = mProduct.selectAmount + Double.parseDouble(amount.replace(",", "."));
                            r = true;
                            break;
                        }
                    }
                    if (r == false) {
                        List<MProduct> list = Configure.GetSession().getList(MProduct.class, " id_core =? ", idProd);
                        if (list.size() == 0) {
                            continue;
                            // throw new RuntimeException("не могу найти продукт!!!");
                        }
                        list.get(0).selectAmount = Double.parseDouble(amount.replace(",", "."));
                        check.mProducts.add(list.get(0));
                    }
                } else {
                    MCheck check = new MCheck();

                    check.id = id_doc;
                    List<MProduct> list = Configure.GetSession().getList(MProduct.class, " id_core =? ", idProd);
                    if (list.size() == 0) {
                        String er = "не могу найти продукт: " + idProd + "!!!";
                        System.out.println(er);
                        throw new RuntimeException(er);
                    }
                    list.get(0).selectAmount = Double.parseDouble(amount.replace(",", "."));
                    check.mProducts.add(list.get(0));
                    check.date = date2 + " " + date1;
                    check.code_doc =code_doc;
                    checkMap.put(id_doc, check);

                }
            }
        }

        List<MCheck> mCheckList = new ArrayList<>(checkMap.values());
        Collections.sort(mCheckList, Comparator.comparingInt(o -> o.id));

        for (MCheck check : mCheckList) {
            if (integers.contains(check.id)) {
                check.isRemove = true;
            }
        }


        ObservableList<MCheck> data = FXCollections.observableArrayList(mCheckList);
        list_check.setItems(data);
        list_check.setCellFactory((Callback<ListView<MCheck>, ListCell<MCheck>>) list -> new CheckCell()
        );
    }

    @Override
    public void refrash() {
        refrashCheck();
    }

    static class CheckCell extends ListCell<MCheck> {
        @Override
        public void updateItem(MCheck item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {
                FItemCheck fItemCheck = new FItemCheck(item);
                item.gridPane = fItemCheck;
                setGraphic(fItemCheck);
            }
        }
    }

}
