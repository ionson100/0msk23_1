package bitnic.pagesettings.pagesettingsintegeritem;

import bitnic.pagesale.CustomScrollEvent;
import bitnic.pagesettings.SettingItem;
import bitnic.utils.support;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FSettingsInteger extends GridPane implements Initializable {


    private double  lastYposition=0d;
    private Logger log=Logger.getLogger(getClass());
    public Label label_description;
    public TextField tf_value;
    public Button bt_plus, bt_minus;


    private int value;
    private SettingItem des;
    private IAction<Integer> iAction;

    public FSettingsInteger(int value, SettingItem des, IAction<Integer> iAction) {
        this.value = value;
        this.des = des;
        this.iAction = iAction;

        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_settings_integer.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if (SettingAppE.instance.isTypeshow) {
            tf_value.getStyleClass().add("delivery_3d");
            bt_minus.getStyleClass().add("button_key_big_3d");
            bt_plus.getStyleClass().add("button_key_big_3d");
        } else {
            tf_value.getStyleClass().add("delivery");
            bt_minus.getStyleClass().add("button_key_big");
            bt_plus.getStyleClass().add("button_key_big");
        }
        if(des.isFontSize ()){
            label_description.setStyle ("-fx-text-fill: #e7e7e7;-fx-font-size: "+value );
        }else {
            label_description.setStyle ("-fx-text-fill: #e7e7e7");
        }

        label_description.setWrapText(true);
        if(des.description().length()>0){
            label_description.setText(des.description());
        }else {
            if(des.key().length()>0){
                label_description.setText(support.str(des.key()));
            }else {
                label_description.setText("Не определено");
            }
        }

        tf_value.setText(String.valueOf(value));
        tf_value.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if(newValue.trim().length()==0) return;
                value = Integer.parseInt(newValue.trim());
                iAction.action(value);
                if(des.isFontSize ()){
                    label_description.setStyle ("-fx-text-fill: #e7e7e7;-fx-font-size: "+value );
                }
            } catch (Exception ex) {
                tf_value.setText(oldValue);
            }
        });
        bt_plus.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                value = value + 1;
                tf_value.setText(String.valueOf(value));
                iAction.action(value);
            }
        });

        bt_minus.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                value = value - 1;
                tf_value.setText(String.valueOf(value));
                iAction.action(value);
            }
        });
        this.setOnMousePressed(new EventHandler<MouseEvent> () {

            @Override
            public void handle(MouseEvent event) {

                lastYposition = event.getSceneY();
            }
        });

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });

    }


}