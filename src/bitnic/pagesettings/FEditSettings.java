package bitnic.pagesettings;

import bitnic.core.Controller;
import bitnic.utils.support;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import bitnic.pagesettings.pagesettingsintegeritem.FSettingsInteger;
import bitnic.pagesettings.pagesettingsitem.FSettingsItem;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;


import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;
import java.util.List;

public class FEditSettings extends GridPane implements Initializable {

    Logger log=Logger.getLogger(getClass());

    public Button bt_save;
    public GridPane grid1;
    public ScrollPane scrol_pane;




    public FEditSettings() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_settings.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        UtilsOmsk.resizeNode(this, grid1);
        UtilsOmsk.painter3d(bt_save);

        bt_save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                SettingAppE.save();
            }
        });

        SettingAppE setting = SettingAppE.instance;
        Field[] fields = SettingAppE.instance.getClass().getFields();
        List<WrapperSettings> wrapperSettingsList = new ArrayList<>();

        for (Field field : fields) {
            SettingItem item = field.getAnnotation(SettingItem.class);
            if (item != null) {
                wrapperSettingsList.add(new WrapperSettings(item, field));
            }
        }

        Collections.sort(wrapperSettingsList, Comparator.comparingInt(o -> o.item.index()));

        VBox vBox = new VBox();
        vBox.setStyle("-fx-background-color: #6b7e87");
        vBox.setSpacing(5);
        for (WrapperSettings ws : wrapperSettingsList) {

            if (ws.item.type() == TypeSettings.booleans) {
                booleanType(setting, vBox, ws);
            }
            if (ws.item.type() == TypeSettings.integers) {

                integerType(setting, vBox, ws);
            }

            if (ws.item.type() == TypeSettings.colors) {

                Color color = null;
                try {
                    color = (Color) ws.field.get(setting);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                ColorPicker colorPicker = new ColorPicker();


                String s = "#" + FSettingsItem.ColorToHex(color);
                Color c = Color.valueOf(s);
                colorPicker.setValue(c);
                FSettingsItem item = new FSettingsItem(color, colorPicker, ws.item.description());
                colorPicker.setPrefHeight(50);
                colorPicker.setPrefWidth(250);
                colorPicker.setOnAction((EventHandler) t -> {
                    try {

                        ws.field.set(setting, colorPicker.getValue());
                        item.refrashColor(colorPicker.getValue());
                    } catch (IllegalAccessException e) {
                        log.error(e);
                        e.printStackTrace();
                    }

                });


                item.setPrefWidth(bitnic.core.Main.MyStage.getWidth());
                vBox.getChildren().add(item);

            }


        }
        scrol_pane.setContent(vBox);


        Controller.WriteMessage(support.str("name34"));

    }

    private void integerType(SettingAppE setting, VBox vBox, WrapperSettings ws) {
        Integer b = null;
        try {
            b = (Integer) ws.field.get(setting);
        } catch (IllegalAccessException e) {
            log.error(e);
            e.printStackTrace();
        }


        FSettingsInteger item = new FSettingsInteger(b, ws.item, integer -> {
            try {
                ws.field.set(setting, integer);
            } catch (IllegalAccessException e) {
                log.error(e);
                e.printStackTrace();
            }
            return false;
        });
        item.setPrefWidth(bitnic.core.Main.MyStage.getWidth());
        vBox.getChildren().add(item);
    }

    private void booleanType(SettingAppE setting, VBox vBox, WrapperSettings ws) {
        VBox box = new VBox();
        ToggleGroup group = new ToggleGroup();
        Boolean b = null;
        try {
            b = (Boolean) ws.field.get(setting);
        } catch (IllegalAccessException e) {
            log.error(e);
            e.printStackTrace();
        }
        RadioButton r1 = new RadioButton();
        r1.setText("Включено");
        r1.setToggleGroup(group);
        r1.setStyle("-fx-text-fill: #e7e7e7;-fx-font-size: 25px");
        r1.setPadding(new Insets(9, 9, 20, 9));
        if (b) {
            r1.setSelected(true);
        }
        box.getChildren().add(r1);
        RadioButton r2 = new RadioButton();
        r2.setText("Выключено");
        r2.setStyle("-fx-text-fill: #e7e7e7;-fx-font-size: 25px");
        r2.setPadding(new Insets(20, 9, 9, 9));
        r2.setToggleGroup(group);
        if (b == false) {
            r2.setSelected(true);
        }
        box.getChildren().add(r2);
        r1.selectedProperty().addListener((obs, wasPreviouslySelected, isNowSelected) -> {
            try {
                ws.field.set(setting, isNowSelected);
            } catch (IllegalAccessException e) {
                log.error(e);
                e.printStackTrace();
            }
        });


        FSettingsItem item=null;
        if(ws.item.description().length()>0){
            item = new FSettingsItem(box, ws.item.description());

        }else {
            if(ws.item.key().length()>0){
                item = new FSettingsItem(box, support.str(ws.item.key()));
            }else {
                item = new FSettingsItem(box,"Не определено");
            }
        }



        item.setPrefWidth(bitnic.core.Main.MyStage.getWidth());
        vBox.getChildren().add(item);
    }


}

class WrapperSettings {
    public final SettingItem item;
    public final Field field;

    public WrapperSettings(SettingItem item, Field field) {

        this.item = item;
        this.field = field;
    }
}