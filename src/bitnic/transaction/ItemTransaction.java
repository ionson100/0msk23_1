package bitnic.transaction;

public class ItemTransaction {
    @ItemIndex(1)
    public Integer numberAction;            //1 Целое № транзакции
    @ItemIndex(2)
    public String dateAction;           //2 Дата Дата транзакции
    @ItemIndex(3)
    public String timeAction;           //3 Время Время транзакции
    @ItemIndex(4)
    public Integer number;                  //4 Целое 1 / 11 2 / 12
    @ItemIndex(5)
    public Integer numberPm;                //5 Целое Номер РМ
    @ItemIndex(6)
    public Integer numberDoc;               //6 Целое Номер документа
    @ItemIndex(7)
    public String cod_kassir;           //7 Строка Код кассира
    @ItemIndex(8)
    public String cod_product;          //8 Строка 0 / Код товара
    @ItemIndex(9)
    public String str1;                 //9 Строка Коды значений разрезов через запятую
    @ItemIndex(10)
    public Double priceProd;            //10 Дробное Цена товара без скидок
    @ItemIndex(11)
    public Double amountProd;           //11 Дробное Количество товара Количество товара
    @ItemIndex(12)
    public Double sumProd1;             //12 Дробное Сумма товара + сумма округления
    @ItemIndex(13)
    public Integer typeCheck;               //13 Целое Тип чека ККМ
    @ItemIndex(14)
    public Integer numberSmena;             //14 Целое Номер смены
    @ItemIndex(15)
    public Double priceProdItogo;       //15 Дробное Округленная итоговая цена со скидками
    @ItemIndex(16)
    public Double sumProd2;             //16 Дробное Сумма товара + сумма округления со скидками
    @ItemIndex(17)
    public String codGroupProd;         //17 Строка Код ГП товара
    @ItemIndex(18)
    public String articul;              //18 Строка – / Артикул товара
    @ItemIndex(19)
    public Integer barkodeReg;              //19 Целое Штрихкод регистрации
    @ItemIndex(20)
    public Double SummProdNoDiscount;   //20 Дробное Сумма товара без скидок Сумма товара без скидок;
    @ItemIndex(21)
    public Integer int1;                    //21 Целое –
    @ItemIndex(22)
    public Integer int2;                    //22 Целое –
    @ItemIndex(23)
    public String codDoc;               //23 Строка Код вида документа
    @ItemIndex(24)
    public Integer int3;                    //24 Целое –
    @ItemIndex(25)
    public Integer int4;                    //25 Целое –
    @ItemIndex(26)
    public String infoDoc;              //26 Строка Информация о документе
    @ItemIndex(27)
    public Integer int5;                    //27 Целое –
    @ItemIndex(28)
    public Integer int6;                    //28 Целое –
    @ItemIndex(28)
    public Integer int7;                    //29 Целое –
    @ItemIndex(30)
    public Integer int8;                    //30 Строка –
    @ItemIndex(31)
    public Integer int9;                    //31 Целое –
    @ItemIndex(32)
    public Integer int10;                   //32 Целое –
    @ItemIndex(33)
    public Integer int11;                   //33 Строка –
    @ItemIndex(34)
    public String marka;                //34 Строка Марка

}

