package bitnic.transaction.eventfrontol;

import bitnic.transaction.ItemIndex;

import java.util.ArrayList;
import java.util.List;

import static bitnic.transaction.eventfrontol.Event_1_11_2_12_4_14.parse;

public class Event_60_61_62_63_64 extends EventBaseE {

    @ItemIndex(8)
    public String str_1_8;               //8 Строка –
    @ItemIndex(9)
    public String str_2_9;             //9 Строка –
    @ItemIndex(10)
    public Double sum_smena_10;           //10 Дробное Выручка за смену –
    @ItemIndex(11)
    public Double sum_kassa_11;           //11 Дробное Наличность в кассе –
    @ItemIndex(12)
    public Double itogo_smena_12;         //12 Дробное Сменный итог –
    @ItemIndex(13)
    public Integer int_1_13;              //13 Целое –
    @ItemIndex(14)
    public Integer number_smena_14;       //14 Целое Номер смены
    @ItemIndex(15)
    public Double double_1_15;            //15 Дробное –
    @ItemIndex(16)
    public Double double_2_16;            //16 Дробное –
    @ItemIndex(17)
    public Integer cod_print_group_17=0;    //17 Целое Код группы печати
    @ItemIndex(18)
    public String str_3_18;               //18 Строка –
    @ItemIndex(19)
    public Integer int_3_19;              //19 Целое –
    @ItemIndex(20)
    public Double double_33_20;           //20 Дробное –
    @ItemIndex(21)
    public Integer int_4_21;              //21 Целое –
    @ItemIndex(22)
    public Integer int_5_22=0;              //22 Целое –
    @ItemIndex(23)
    public Integer int_6_23=8;              //23 Целое –
    @ItemIndex(24)
    public Integer int_7_24=0;              //24 Целое –
    @ItemIndex(25)
    public Integer int_8_25;              //25 Целое –
//    @ItemIndex(26)
//    public String number_chek_26;         //26 Строка Кассовый номер чека, документа и смены –
//    @ItemIndex(27)
//    public Integer code_firma_27;           //27 Целое Идентификатор предприятия
    @ItemIndex(28)
    public Integer int_9_28;              //28 Целое –
    @ItemIndex(29)
    public Integer int_10_29;             //29 Целое –
    @ItemIndex(30)
    public String str_33_30;              //30 Строка –
    @ItemIndex(31)
    public Integer int_11_31;             //31 Целое –
    @ItemIndex(32)
    public Integer int_12_32;             //32 Целое –
    @ItemIndex(33)
    public String str_4_33;               //33 Строка –
    @ItemIndex(34)
    public String str_5_34;               //34 Строка –
    @ItemIndex(35)
    public String str_6_35;               //35 Строка –
    @ItemIndex(36)
    public String datetime_36;            //36 Дата ивремя

    @ItemIndex(37)
    public String   null_37;
    @ItemIndex(38)
    public String   null_38;
    @ItemIndex(39)
    public String   null_39;
    @ItemIndex(40)
    public String   null_40;
    @ItemIndex(41)
    public String   null_41;
    @ItemIndex(42)
    public String   null_42;
    @ItemIndex(43)
    public String   null_43;

    @ItemIndex(44)
    public String   null_44;





    public Event_60_61_62_63_64() {
        super();
    }

    private static List<ItemIndexTemp> list=new ArrayList<>();
    public Event_60_61_62_63_64(String[] strings) {
        super();

        parse(strings,list,this);
    }






}
