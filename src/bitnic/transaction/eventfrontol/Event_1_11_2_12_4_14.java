package bitnic.transaction.eventfrontol;

// 11 - продажв 12 налог ндс отдельно

import bitnic.transaction.ItemIndex;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Event_1_11_2_12_4_14 extends EventBaseE {

    //1/11 Регистрация товара по свободной цене/из справочника см. стр. 243
    //2/12 Сторно товара по свободной цене/из справочника см. стр. 243
    //4/14 Налог на товар по свободной цене/из справочника см. стр. 243
    //41 Оплата без ввода суммы клиента см. стр. 249
    //42 Открытие документа см. стр. 269
    //45 Закрытие документа в ККМ см. стр. 269
    //50 Внесение см. стр. 273
    //51 Выплата см. стр. 273
    //55 Закрытие документа см. стр. 269
    //60 Отчёт без гашения см. стр. 277
    //61 Закрытие смены см. стр. 277
    //62 Открытие смены см. стр. 277
    //63 Отчёт с гашением см. стр. 277
    //64 Документ открытия смены см. стр. 277


    @ItemIndex(8)
    public String codeProd_8;           //8 Строка –/Идентификатор товара
    @ItemIndex(9)
    public String codeCut_9;            //9 Строка Коды значений разрезов
    @ItemIndex(10)
    public Double price_not_discount_10; //10 Дробное Цена без скидок Код налоговой группы
    @ItemIndex(11)
    public Double amount_prod_11;        //11 Дробное Количество товара Количество товара Код налоговой ставки
    @ItemIndex(12)
    public Double amount_rount_prod_12;  //12 Дробное Округленная сумма товара
    @ItemIndex(13)
    public Integer kkm_operation_13 = 0;     //13 Целое Операция
    @ItemIndex(14)
    public Integer number_smena_14 = 1;      //14 Целое Номер смены
    @ItemIndex(15)
    public Double priee_discount_15;     //15 Дробное Итоговая цена со скидками
    @ItemIndex(16)
    public Double price_discount_rub_16; //16 Дробное Итоговая сумма в базовой валюте со скидками
    @ItemIndex(17)
    public Integer code_group_print_17 = 0;  //17 Целое Код группы печати
    @ItemIndex(18)
    public String articul_prod_18;       //18 Строка –/Артикул товара
    @ItemIndex(19)
    public String barcode_19;           //19 Целое Штрихкод регистрации
    @ItemIndex(20)
    public Double summ_itogo_20;               //20 Дробное Сумма в базовой валюте без скидок
    @ItemIndex(21)
    public Integer kmm_section_21 = 0;       //21 Целое Секция ККМ –
    @ItemIndex(22)
    public Integer int_2_22;             //22 Целое –
    @ItemIndex(23)
    public Integer code_vid_code_23 = 1;     //23 Целое Код вида документа
    @ItemIndex(24)
    public Integer code_doc_24;          //24 Целое Код комментария
    @ItemIndex(25)
    public Integer int_3_25;             //25 Целое –
    //    @ItemIndex(26)
//    public String info_doc_26;           //26 Строка Информация о документе///////////////////////////
//    @ItemIndex(27)
//    public Integer code_firma_27 = 1;        //27 Целое Идентификатор предприятия
    @ItemIndex(28)
    public Integer code_user_28 = 0;         //28 Целое Код сотрудника
    @ItemIndex(29)
    public Integer int_4_29;             //29 Целое –
    @ItemIndex(30)
    public String str_1_30;              //30 Строка –
    @ItemIndex(31)
    public Integer int_5_31;             //31 Целое –
    @ItemIndex(32)
    public Integer type_prod_32 = 0;         //32 Целое Тип номенклатуры:  0 – обычный товар;1 – алкогольная продукция;2 – маркированная продукция.
    @ItemIndex(33)
    public String kiz_prod_33;           //33 Строка КИЗ маркированной продукции –
    @ItemIndex(34)
    public String excise_mark_34;        //34 Строка Акцизная марка/Список акцизных марок,разделенных «¤»
    @ItemIndex(35)
    public String code_pesonal_35;       //35 Строка Код группы персональных модификаторов Код группы персональных модификаторов
    @ItemIndex(36)
    public String date_time_egaiz_36;    //36 Дата и время Дата фиксации в ЕГАИС илидата ТТН (для документа  постановки на баланс ЕГАИС)
    @ItemIndex(37)
    public String srrre_37;

    @ItemIndex(38)
    public String srrre2_38;
    @ItemIndex(39)
    public Integer alc_code_39;          //39 Строка AlcCode
    @ItemIndex(40)
    public Double amount_egaiz_40;       //40 Дробное Количество из документаприёмки ЕГАИС
    @ItemIndex(41)
    public String nimber_a_41;           //41 Строка Номер справки А
    @ItemIndex(42)
    public String number_b_42;           //42 Строка Номер справки Б

    @ItemIndex(43)
    public String null_43;
    @ItemIndex(44)
    public String null_44;

    public Event_1_11_2_12_4_14() {
        super();
    }


    private static List<ItemIndexTemp> list = new ArrayList<>();

    public Event_1_11_2_12_4_14(String[] strings) {
        super();

        parse(strings, list, this);


    }

    public static void parse(String[] strings, List<ItemIndexTemp> list, EventBaseE eventBaseE) {
        if (list.size() == 0) {
            for (Field field : eventBaseE.getClass().getSuperclass().getDeclaredFields()) {
                ItemIndex in = field.getAnnotation(ItemIndex.class);
                if (in == null) continue;
                list.add(new ItemIndexTemp(field, in));
            }
            for (Field field : eventBaseE.getClass().getDeclaredFields()) {
                ItemIndex in = field.getAnnotation(ItemIndex.class);
                if (in == null) continue;
                list.add(new ItemIndexTemp(field, in));
            }
            Collections.sort(list, Comparator.comparingInt(o -> o.index.value()));
        }

        String val = "";
        try {
            for (int i = 0; i < list.size(); i++) {
                if (strings[i] == null || strings[i].toString().equals("")) {
                    list.get(i).field.set(eventBaseE, null);
                } else {
                    val = strings[i].toString();
                    Type type = list.get(i).field.getType();
                    if (type == String.class) {
                        list.get(i).field.set(eventBaseE, val);
                    } else if (type == Integer.class) {
                        list.get(i).field.set(eventBaseE, Integer.parseInt(val));
                    } else if (type == Double.class) {
                        list.get(i).field.set(eventBaseE, Double.parseDouble(val.replace(",", ".")));
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("-----------------------" + val);
            ex.printStackTrace();
        } finally {
            int f = 1;
        }
    }


}
