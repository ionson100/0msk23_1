package bitnic.transaction.eventfrontol;

import bitnic.transaction.ItemIndex;

import java.util.ArrayList;
import java.util.List;

import static bitnic.transaction.eventfrontol.Event_1_11_2_12_4_14.parse;

public class Event_50_51 extends EventBaseE {


    @ItemIndex(8)
    public String str_1_8;              //8 Строка –
    @ItemIndex(9)
    public String str_2_9;              //9 Строка –
    @ItemIndex(10)
    public Double double_1_10;           //10 Дробное –
    @ItemIndex(11)
    public Double double_2_11=1d;           //11 Дробное –
    @ItemIndex(12)
    public Double double_3_12;           //12 Дробное Сумма в базовой валюте
    @ItemIndex(13)
    public Integer kkm_operation_13 =4;     //13 Целое Операция
    @ItemIndex(14)
    public Integer number_smena_14=1;      //14 Целое Номер смены
    @ItemIndex(15)
    public Double double_4_15;           //15 Дробное –
    @ItemIndex(16)
    public Double double_5_16;           //16 Дробное –
    @ItemIndex(17)
    public Integer code_print_group_17=0;  //17 Целое Код группы печати
    @ItemIndex(18)
    public String str_3_18;              //18 Строка –
    @ItemIndex(19)
    public Integer int_1_19;             //19 Целое –
    @ItemIndex(20)
    public Double double_6_20;           //20 Дробное –
    @ItemIndex(21)
    public Integer int_2_21;             //21 Целое –
    @ItemIndex(22)
    public Integer int_3_22;             //22 Целое –
    @ItemIndex(23)
    public Integer int_4_23;             //23 Целое Код вида документа
    @ItemIndex(24)
    public Integer int_5_24;             //24 Целое –
    @ItemIndex(25)
    public Integer int_6_25;             //25 Целое –
//    @ItemIndex(26)
//    public String info_doc_26="";           //26 Строка Информация о документе
//    @ItemIndex(27)
//    public Integer code_firma_27 =1;             //27 Целое Идентификатор предприятия
    @ItemIndex(28)
    public Integer int_8_28;             //28 Целое –
    @ItemIndex(29)
    public Integer int_9_29;             //29 Целое –
    @ItemIndex(30)
    public String str_4_30;              //30 Строка –
    @ItemIndex(31)
    public Integer int_10_31;            //31 Целое –
    @ItemIndex(32)
    public Integer int_11_32;            //32 Целое –
    @ItemIndex(33)
    public String str_5_33;              //33 Строка –
    @ItemIndex(34)
    public String str_6_34;              //34 Строка –
    @ItemIndex(35)
    public String str_7_35;              //35 Строка –
    @ItemIndex(36)
    public String datetime_36;           //36 Дата и время
    @ItemIndex(37)
    public String null_37;
    @ItemIndex(38)
    public String null_38;
    @ItemIndex(39)
    public String null_39;
    @ItemIndex(40)
    public String null_40;
    @ItemIndex(41)
    public String null_41;
    @ItemIndex(42)
    public String null_42;
    @ItemIndex(43)
    public String null_43;
    @ItemIndex(44)
    public String null_44;



    private static List<ItemIndexTemp> list=new ArrayList<>();
    public Event_50_51(String[] strings) {
        super();

        parse(strings,list,this);
    }
    public Event_50_51() {
        super();
    }
}
