package bitnic.transaction.eventfrontol;

import bitnic.transaction.ItemIndex;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Event_56 extends EventBaseE  {

    @ItemIndex(8)
    public String number_card_8;              //8 Строка Номер карт
    @ItemIndex(9)
    public String code_payment_9;              //9 Строка Код вида оплаты
    @ItemIndex(10)
    public Double type_operation_payment_10;    //10 Дробное Операция вида оплаты
    @ItemIndex(11)
    public Double summ_doc_11;                  //11 Дробное Сумма клиента в валюте оплаты   Итоговая суммадокумента в валюте
    @ItemIndex(12)
    public Double summ_client_12;               //12 Дробное Сумма клиента в базовой валютеИтоговая сумма
    @ItemIndex(13)
    public Integer kkm_operation_13 =0;            //13 Целое Операция
    @ItemIndex(14)
    public Integer number_smena_14=1;             //14 Целое Номер смены
    @ItemIndex(15)
    public Integer cod_action_15=0;              //15 Целое Код акции
    @ItemIndex(16)
    public Integer cod_action_16=0;               //16 Целое Код мероприятия
    @ItemIndex(17)
    public Integer code_print_group_17=0;         //17 Целое Код группы печати

    @ItemIndex(18)
    public String articul_18;                  //–/Артикул товара

    @ItemIndex(19)
    public Integer code_currency_19=1;            //19 Целое Код валюты
    @ItemIndex(20)
    public Double double_20;                  //20 Дробное –
    @ItemIndex(21)
    public Integer type_code_iterator_21=0;       //21 Целое Код вида счётчика
    @ItemIndex(22)
    public Integer kode_itrerator_22=0;           //22 Целое Код счётчика
    @ItemIndex(23)
    public Integer type_doc_23;                 //23 Целое Код вида документа
    @ItemIndex(24)
    public Integer int_10_24;                   //24 Целое –
    @ItemIndex(25)
    public Integer int_11_25;                   //25 Целое –


    public Event_56(){

    }

    private static List<ItemIndexTemp> list=new ArrayList<>();
    public Event_56(String[] strings) {
        super();
        parse(strings, list, this);
    }
    public static void parse(String[] strings, List<ItemIndexTemp> list, EventBaseE eventBaseE) {
        if (list.size() == 0) {
            for (Field field : eventBaseE.getClass().getSuperclass().getDeclaredFields()) {
                ItemIndex in = field.getAnnotation(ItemIndex.class);
                if (in == null) continue;
                list.add(new ItemIndexTemp(field, in));
            }
            for (Field field : eventBaseE.getClass().getDeclaredFields()) {
                ItemIndex in = field.getAnnotation(ItemIndex.class);
                if (in == null) continue;
                list.add(new ItemIndexTemp(field, in));
            }
            Collections.sort(list, Comparator.comparingInt(o -> o.index.value()));
        }

        String val = "";
        try {
            for (int i = 0; i < list.size(); i++) {
                if (strings[i] == null || strings[i].toString().equals("")) {
                    list.get(i).field.set(eventBaseE, null);
                } else {
                    val = strings[i].toString();
                    Type type = list.get(i).field.getType();
                    if (type == String.class) {
                        list.get(i).field.set(eventBaseE, val);
                    } else if (type == Integer.class) {
                        list.get(i).field.set(eventBaseE, Integer.parseInt(val));
                    } else if (type == Double.class) {
                        list.get(i).field.set(eventBaseE, Double.parseDouble(val.replace(",", ".")));
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("-----------------------" + val);
            ex.printStackTrace();
        } finally {
            int f = 1;
        }
    }

}
