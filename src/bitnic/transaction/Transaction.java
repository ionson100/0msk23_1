package bitnic.transaction;

import bitnic.kassa.OpenSession;
import bitnic.model.MCheck;
import bitnic.model.MProduct;
import bitnic.senders.IAction;

import bitnic.transaction.eventfrontol.EventBaseE;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Transaction {

    public List<EventBaseE> list=new ArrayList<>();
    public Date date;
    public Date time;

    public Integer numberDoc;
    public int numberSession;
    public int numberCheck;
    public double summSmena;
    public double выручка_отчет;
    public double сменный_итог;
    public double сумма_нал_ккм;


    public void saleCache(List<MProduct> mProducts){
        if(mProducts.size()==0) return;
        Transaction dd=this;
        new  bitnic.kassa.PrintCheck2().print(mProducts,dd,0d,null,null);

    }


    public void casheIn(Double summ){
        Transaction dd=this;
        new bitnic.kassa.ContributingMoney().run(summ,dd);
    }

    public String getDate() {
        //28.12.2017;8:42:16
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");// HH:mm:ss
        return dateFormat.format(date);
    }

    public String getTime() {
        //28.12.2017;8:42:16
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(time);
    }

    public void cashOut(double summ) {
        Transaction dd=this;
        new bitnic.kassa.EntryMoney().run(summ,dd);
    }

    public void reportZ() {
        Transaction transaction=this;
        new bitnic.kassa.ZReport().print(transaction);
    }

    public void deleteCheck(MCheck mCheck, int nDoc,IAction< Integer>  iAction) {
        bitnic.kassa.DeleteCheck.DeleteReturn(mCheck.mProducts,nDoc,this,mCheck.code_doc,iAction  );
    }

    public void openSession() {
        Transaction transaction=this;
        new OpenSession().open(transaction);
    }

    public void reportX() {
        Transaction transaction=this;
        new bitnic.kassa.XReport().print(transaction);
    }

    public void salleCacheCard(List<MProduct> mProducts, String numberCard) {

        if(mProducts.size()==0) return;
        Transaction dd=this;
        new  bitnic.kassa.PrintCheck2().print(mProducts,dd,1d,numberCard,null);

    }
}