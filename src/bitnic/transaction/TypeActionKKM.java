package bitnic.transaction;

//13 поле
 public class TypeActionKKM {

    public static final int SALE = 0;//0 – продажа;
    public static final int RETURN = 1;// 1 – возврат;
    public static final int ANULATE = 2; // 2 – аннулирование;
    public static final int MONEYIN = 4;// 4 – внесение;
    public static final int MONEYOUT = 5;// 5 – выплата;
    public static final int USERS = 6;// 6 – пользовательская;
    public static final int OPEN_SMENA = 8;// 8 – открытие смены;
    public static final int ACTION_KKM = 9;// 9 – операция в ККМ;
    public static final int CLOSE_SMENA = 10;// 10 – закрытие смены;
    public static final int CLOSE_SMENA_SYS = 11;// 11 – закр. смены плат. сис
    public static final int EDIT_USER = 12;// 12 – ред. списка сотрудни
    public static final int ACTION_HOST = 13;//// 13 – служебная операция;
    public static final int PRIHOD = 14;// 14 – приход;
    public static final int RASHOD = 15;// 15 – расход;
    public static final int INVENTORY = 16;// 16 – инвентаризация;
    public static final int RESUZE = 17;// 17 – переоценка;
    public static final int OPEN_TARA = 18;// 18 – вскрытие тары.

}
