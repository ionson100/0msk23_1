package bitnic.transaction;

public class TypeEvent {
    public static final int EVEN_REG_FREE_PRICE = 1;      //  1/11 Регистрация товара по свободной цене/из справочника см. стр. 65
    public static final int EVEN_STORNO_FREE_PRICE = 2;   //  2/12 Сторно товара по свободной цене/из справочника см. стр. 65
    public static final int EVENT_DISCOUNT_1 = 15;        //  15 Скидка суммой на позицию товара см. стр. 71
    public static final int EVEN_DISCOUNT_2 = 17;         //  17 Скидка % на позицию товара см. стр. 71
    public static final int EVEN_REG_MONEY = 21;          //  21/23 Регистрация купюр по свободной цене/из справочника см. стр. 67
    public static final int EVEN_STORNO_MONEY = 22;       //  22/24 Сторно купюр по свободной цене/из справочника см. стр. 67
    public static final int EVEN_DISCOUNT_3 = 35;         //  35 Скидка суммой на документ см. стр. 73
    public static final int EVEN_DISCOUNT_4 = 37;         //  37 Скидка % на документ см. стр. 73
    public static final int EVEN_PAYMENT_NO_MONEY = 40;   //  40 Оплата с вводом суммы клиента см. стр. 69
    public static final int EVEN_PAYMENT_AND_MONEY = 41;  //  41 Оплата без ввода суммы клиента см. стр. 69
    public static final int EVEN_OPEN_DOC = 42;           //  42 Открытие документа см. стр. 76
    public static final int EVEN_PAYMENT_MAP = 43;        //  43 Распределение оплаты по ГП см. стр. 69
    public static final int EVEN_CLOSE_DOC_KKM = 45;      //  45 Закрытие документа в ККМ см. стр. 76
    public static final int EVEN_CLOSE_OC_GP = 49;        //  49 Закрытие документа по ГП см. стр. 76
    public static final int EVEN_IN_MONEY = 50;           //  50 Внесение см. стр. 78
    public static final int EVEN_Out_MONEY = 51;          //  51 Выплата см. стр. 78
    public static final int EVEN__DOC = 55;               //  55 Закрытие документа см. стр. 76
    public static final int EVEN_DOC_NOT_CLOSE_KKM = 56;  //  56 Документ не закрыт в ККМ см. стр. 76
    public static final int EVEN_X_REPORT = 60;            //  60 Отчет без гашения см. стр. 80
    public static final int EVEN_CLOSE_SMENA = 61;        //  61 Закрытие смены см. стр. 80
    public static final int EVEN_OPEN_SMENA = 62;         //  62 Открытие смены см. стр. 80
    public static final int EVEN_Z_REPORT = 63;           //  63 Отчет с гашением см. стр. 80
    public static final int EVEN_OPEN_SMENA_DOC = 64;     //  64 Документ открытия смены см. стр. 80
    public static final int EVEN_DISCOUNT_5 = 85;         //  85 Скидка суммой на документ, распределенная по позициям см. стр. 73
    public static final int EVEN_DISCOUNT_6 = 87;         //  87 Скидка % на документ, распределенная по позициям см. стр. 73
    public static final int EVEN_TO_EGAIS = 120;          //  120 Отправка данных в ЕГАИС

}
