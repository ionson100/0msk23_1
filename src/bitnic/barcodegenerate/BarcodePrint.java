package bitnic.barcodegenerate;

import bitnic.dialogfactory.DialogFactory;
import bitnic.utils.Pather;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Logger;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;
import bitnic.settingscore.SettingAppE;

import javax.imageio.ImageIO;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.print.*;
import java.io.File;


import java.io.FileOutputStream;
import java.io.OutputStream;

public class BarcodePrint implements Printable {

    private static final Logger log = Logger.getLogger(BarcodePrint.class);
    public void Print(String code) {
        createImage(code);

        printCore();
    }

    private void printCore() {
        if(SystemUtils.IS_OS_LINUX){
            printLinux();
        }else {
            printWindows();
        }


    }

    private void printLinux() {

        try {
            PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
            pras.add(new Copies(SettingAppE.instance.copy));
            PrinterJob job = PrinterJob.getPrinterJob();
            PageFormat pf = job.defaultPage();
            Paper p = pf.getPaper();

            int resolution = 260; // dpi
            double margin = UnitConv.mm2px(1, resolution);
            double w = UnitConv.mm2px(45, resolution);
            double h = UnitConv.mm2px(25, resolution);
            p.setSize(w,h);
            System.out.println("margin: " + String.valueOf(margin));
            System.out.println("w: " + String.valueOf(w));
            System.out.println("h: " + String.valueOf(h));
            p.setSize(w/2, h/2);
            p.setImageableArea(margin, 2*margin, w -  margin, h - 2 * margin);
            pf.setPaper(p);
            job.setPrintable(this, pf);
            try {
                job.print(pras);
            } catch (Exception e) {
                DialogFactory.ErrorDialog(e);
                e.printStackTrace();
                log.error(e);
            }

        } catch (Exception e) {
            DialogFactory.ErrorDialog(e);
            e.printStackTrace();
            log.error(e);
        }
    }

    private void printWindows() {
        try {
            PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
            pras.add(new Copies(SettingAppE.instance.copy));
            PrinterJob job = PrinterJob.getPrinterJob();
            PageFormat pf = job.defaultPage();
            Paper p = pf.getPaper();
            int resolution = 100; // dpi
            double margin = UnitConv.mm2px(1, resolution);
            double w = UnitConv.mm2px(45, resolution);
            double h = UnitConv.mm2px(25, resolution);
            System.out.println("margin: " + String.valueOf(margin));
            System.out.println("w: " + String.valueOf(w));
            System.out.println("h: " + String.valueOf(h));
            p.setSize(w, h);
            p.setImageableArea(margin, 2*margin, w - 2 * margin, h - 2 * margin);
            pf.setPaper(p);
            job.setPrintable(this, pf);
            try {
                job.print(pras);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(e);
            }

        } catch (Exception e) {
            DialogFactory.ErrorDialog(e);
            e.printStackTrace();
            log.error(e);
        }
    }

    private void createImage(String code) {
        try {

            Code128Bean bean = new Code128Bean();
            final int dpi = 160;
            bean.setModuleWidth(UnitConv.in2mm(2.3f / dpi)); //makes the narrow bar
            bean.setBarHeight(15);
            bean.doQuietZone(false);
            File outputFile = new File(Pather.barcodeImage);
            OutputStream out = new FileOutputStream(outputFile);
            try {
                BitmapCanvasProvider canvas = new BitmapCanvasProvider(
                        out, "bitnic/image/jpeg", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
                bean.generateBarcode(canvas, code);
                canvas.finish();
            } finally {
                out.close();
            }
        } catch (Exception e) {
            DialogFactory.ErrorDialog(e);
            e.printStackTrace();
            log.error(e);
        }
    }


    public static BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return dimg;
    }

    @Override
    public int print(Graphics g, PageFormat pf, int page) {
        if (page > 0) {
            return NO_SUCH_PAGE;
        }
        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(pf.getImageableX(), pf.getImageableY());
        BufferedImage bim = null;
        try {
            bim = ImageIO.read(new File(Pather.barcodeImage));
        } catch (Exception ex) {
            System.err.println("error in bim " + ex);
            log.error(ex);
        }
        //g.drawImage(bim, 0, 0, null);
        g.drawImage(bim, 0, 0,110,60, null);
        return PAGE_EXISTS;
    }
}
