package bitnic.settingscore;

import bitnic.utils.FactorySettings;
import bitnic.utils.IFileStorage;
import bitnic.utils.Pather;
import bitnic.utils.Starter;

import java.io.File;
import java.io.IOException;

public class SettingAppE extends SettingsE implements IFileStorage {

    public static transient SettingAppE instance;

    static {
        instance = FactorySettings.GetSettingsE(SettingAppE.class);
    }

    public String getVersion() throws IOException {
       return Starter.GetStringVersion();
    }


    // интервал таймера для опроса списка фалов для меня.
    public int tumerIntervalUpdate=5000;

    public static void RefrashInstance(){
        instance = FactorySettings.GetSettingsE(SettingAppE.class);
    }


    //пароль сиса по умолчанию
    public static final String defUser = "312873";
    //максимальная сумма чека
    public double mapSumm = 100000d;


    // число копий при печати штрихкодов
    public int copy = 1;
    public double maxSummPrice = 100000d;


    public static void save() {
        FactorySettings.saveE(instance);
    }

    public int ftp_port =21;


    public String ftp_remote_file="goods.txt";





    public String ftp_user;


    public String ftp_password;


    public String ftp_cahrset;

    //настройки кассы
    public String settings_kassa;


    @Override
    public String getFileName() {
        SettingAppMode d=FactorySettings.GetSettingsE(SettingAppMode.class);
        return Pather.settingsFolder + File.separator + "settingsApp" + d.getProfile() + ".txt";
    }


}

