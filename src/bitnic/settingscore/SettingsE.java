package bitnic.settingscore;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import bitnic.model.MProduct;
import bitnic.pagesettings.SettingItem;
import bitnic.pagesettings.TypeSettings;
import bitnic.table.ColumnUserObject;
import bitnic.utils.FactorySettings;
import bitnic.utils.IFileStorage;
import bitnic.utils.Pather;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by USER on 10.01.2018.
 */
public class SettingsE  extends SettingAppMode implements ExclusionStrategy, IFileStorage {



    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return SettingsE.class.equals(fieldAttributes.getDeclaringClass());
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return SettingsE.class.equals(aClass);
    }


    // выбранный продукт
    public List<MProduct> selectProduct=new ArrayList<>();
    //словарь ширины колонак таблицадмина
    public Map<String,List<ColumnUserObject>> map=new HashMap<>();
    // максимальный показ количества строк для поиска продукта в странице продуктов
    public boolean isCheckFinishProduct =false;

    public List<ColumnUserObject> getColumnUserObjects(String tableName){
        if(map.containsKey(tableName)==false){
            List<ColumnUserObject> objectList=new ArrayList<>();
            map.put(tableName,objectList);
        }
        return map.get(tableName);
    }

    @Override
    public String getFileName() {
        SettingAppMode mode= FactorySettings.GetSettingsE(SettingAppMode.class);
        return Pather.settingsFolder+ File.separator+"settingsE_"+mode.user_id+".txt";
    }





    @SettingItem(index = 101,type = TypeSettings.booleans,key = "name122")
    public boolean isTypeshow;



    @SettingItem(index = 1,type = TypeSettings.integers,key = "name123",isFontSize = true)
    public int fontSizeBigButton=30;
//    @SettingItem(index = -1,type = TypeSettings.integers,description = "Размер шрифта кнопок денежных расчетов")
//    public int fontSizeBigButtonDelivery=35;

    @SettingItem(index = 2,type = TypeSettings.integers,key = "name124",isFontSize = true)
    public int fontSizeProduct=18;

    @SettingItem(index = 3,type = TypeSettings.integers,key = "name125",isFontSize = true)
    public int fontSizeSelectProduct=18;

    @SettingItem(index = 4,type = TypeSettings.integers,key = "name126",isFontSize = true)
    public int fontSizeDelivery=18;

    @SettingItem(index = 5,type = TypeSettings.integers,key = "name127",isFontSize = true)
    public int fontSizeDeliverySearch =35;

    @SettingItem(index = 7,type = TypeSettings.integers,key = "name128",isFontSize = true)
    public int fontSizeDeliveryButton =18;

    @SettingItem(index = 8,type = TypeSettings.integers,key = "name129",isFontSize = true)
    public int fontSizeDeliveryButtonCountProduct =20;

    @SettingItem(index = 20,type = TypeSettings.booleans,key = "name132")
    public boolean isSearchProductName;


    @SettingItem(index = 21,type = TypeSettings.integers,key = "name133",isFontSize = true)
    public int fontSizeMenuProduct=20;



//    @SettingItem(index = -10,type = TypeSettings.colors,description = "Цвет шрифта кнопок главной панели")
//    public Color colorFont= Color.web("#0076a3");
}
