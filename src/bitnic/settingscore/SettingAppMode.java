package bitnic.settingscore;

import bitnic.model.MUser;
import bitnic.orm.Configure;
import bitnic.pagesettings.SettingItem;
import bitnic.pagesettings.TypeSettings;
import bitnic.utils.*;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;

public class SettingAppMode implements ExclusionStrategy, IFileStorage {
    private transient static volatile int pointid = -1;

    private transient static final Logger log = Logger.getLogger(SettingAppMode.class);

    @SettingItem(index = 6, type = TypeSettings.integers, key = "name121")
    public int scanerDelau = 10000000;

    public String getOfdUrl() {
        return ofd_url[profile];
    }

    public int getPortOfd() {
        return ofd_port[profile];
    }

    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return SettingAppMode.class.equals(fieldAttributes.getDeclaringClass());
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return SettingAppMode.class.equals(aClass);
    }


    // профиль приложения
    private transient volatile static int profile = -1;
    private static final Integer[] ofd_port = {21101, 7790, 7790};
    private static final String[] ofd_url = {"185.170.204.91", "91.107.67.212", "91.107.67.212"};
    private static final String[] url = {"91.144.170.205", "174.138.11.176", "178.62.249.118"};
    private static final String[] names = {"1C", "Bitnic", "Bitnic Test 1C"};

    //индентификатор торговой точки
    public int getPointId() {
        if (pointid == -1) {
            String dd = UtilsOmsk.readFile(Starter.Pathid);
            if (dd == null) {
                pointid = 0;
            }
            try {
                dd = dd.replace("\n", "");
                pointid = Integer.parseInt(dd);
            } catch (Exception ex) {
                log.error(ex);
                pointid = 0;
            }
        }
        return pointid;

    }

    public void setPointId(int id) {
        try {
            pointid = id;
            UtilsOmsk.rewriteFile(Starter.Pathid, String.valueOf(id));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
    }

    public int getProfile() {
        if (profile == -1) {
            String dd = UtilsOmsk.readFile(Starter.Pathprofile);
            if (dd == null) {
                profile = 0;
            }
            try {
                dd = dd.replace("\n", "");
                profile = Integer.parseInt(dd);
            } catch (Exception ex) {
                log.error(ex);
                profile = 0;
            }
        }
        return profile;
    }

    public void setProfile(int id) {
        try {
            profile = id;
            UtilsOmsk.rewriteFile(Starter.Pathprofile, String.valueOf(id));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e);
        }
    }

    // число дней для проверки срока годности продукта, меньше нее - алерт
    public int maxDatecheckProduct = 20;


    //индентификатор пользователя  его табличный код
    public String user_id;

    //наименование профиля
    public static String[] GetNamesProfile() {
        return names;
    }

    public String getUrl() {
        return url[profile];
    }

    public String getNameProfile() {
        return names[profile];
    }

    public String getUserName() {

        if (user_id == null) {
            return support.str("name131");
        }

        if (user_id.equals("1")) {
            return support.str("name130");
        }

        List<MUser> mUsers = Configure.GetSession().getList(MUser.class, " cod = ?", user_id);

        if (mUsers.size() > 0) {
            return mUsers.get(0).name;
        } else {
            return support.str("name131");
        }
    }


    @Override
    public String getFileName() {
        return Pather.settingsFolder + File.separator + "settingsAppMode.txt";
    }

    public String getUserCheck() {

        if (user_id == null) {
            return "не найден";
        }
        if (user_id.equals("1")) {
            return "Cистемный администратор";
        }

        List<MUser> mUsers = Configure.GetSession().getList(MUser.class, " cod = ?", user_id);
        if (mUsers.size() > 0) {
            return mUsers.get(0).name_check;
        } else {
            return "не найден";
        }
    }
}
