package bitnic.dialogfactory.checkstory;

import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.checkstory.ietemCheckStory.FItemCheckStory;
import bitnic.utils.UtilsOmsk;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import javafx.util.Callback;
import bitnic.model.MCheck;
import bitnic.transaction.eventfrontol.*;
import bitnic.utils.Pather;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FCheckStory extends DialogBase {

    private static final Logger log = Logger.getLogger(FCheckStory.class);
    public Button bt_close1, bt_close2;
    public ListView list_checks;
    private ObservableList<EventBaseE> eList = FXCollections.observableArrayList();


    public FCheckStory(URL location, Stage stage) {
        super(location, stage);

        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        UtilsOmsk.painter3d(bt_close1);
        init();
    }

    void init() {
        Path wiki_path = Paths.get(Pather.sender_report_file);
        Charset charset = Charset.forName("UTF-8");
        List<String> lines = null;
        try {
            lines = Files.readAllLines(wiki_path, charset);
            for (String line : lines) {
                System.out.println(line);
            }
        } catch (IOException e) {
            log.error(e);
            DialogFactory.ErrorDialog(e);
            System.out.println(e);
        }
        if (lines == null) return;

        Map<Integer, MCheck> checkMap = new HashMap<>();

        for (String line : lines) {
            if (line.trim().length() < 10) continue;

            String[] strings = line.split(";", -1);
            switch (strings[3]) {
                case "11": {
                    eList.add(new Event_1_11_2_12_4_14(strings));
                    break;
                }
                case "12": {
                    eList.add(new Event_1_11_2_12_4_14(strings));
                    break;
                }
                case "14": {
                    eList.add(new Event_1_11_2_12_4_14(strings));
                    break;
                }
                case "40": {
                    //40 Оплата с вводом суммы клиента см. стр. 249
                    eList.add(new Event_40_41(strings));
                    break;
                }
                case "41": {
                    eList.add(new Event_40_41(strings));
                    break;
                }
                case "42": {
                    eList.add(new Event_42_55(strings));
                    break;
                }
                case "55": {
                    eList.add(new Event_42_55(strings));
                    break;
                }
                case "50": {
                    eList.add(new Event_50_51(strings));
                    break;
                }
                case "51": {
                    eList.add(new Event_50_51(strings));
                    break;
                }
                case "60": {
                    eList.add(new Event_60_61_62_63_64(strings));
                    break;
                }
                case "61": {
                    eList.add(new Event_60_61_62_63_64(strings));
                    break;
                }
                case "62": {
                    eList.add(new Event_60_61_62_63_64(strings));
                    break;
                }
                case "63": {
                    eList.add(new Event_60_61_62_63_64(strings));
                    break;
                }

                case "64": {
                    eList.add(new Event_60_61_62_63_64(strings));
                    break;
                }

                case "56": {
                    eList.add(new Event_56(strings));
                    break;
                }
                case "300": {
                    break;
                }
                default: {
                    log.error(" не могу определить трансакцию - " + strings[3]);
                    throw new RuntimeException(" не могу определить трансакцию - " + strings[3]);
                }
            }


        }
        list_checks.getItems().addAll(eList);

        list_checks.setCellFactory((Callback<ListView<EventBaseE>, ListCell<EventBaseE>>) list -> new CheckCell());


    }

    static class CheckCell extends ListCell<EventBaseE> {
        @Override
        public void updateItem(EventBaseE item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {

                FItemCheckStory fItemCheckStory=new FItemCheckStory(item);
                setGraphic(fItemCheckStory);
            }
        }
    }
}
