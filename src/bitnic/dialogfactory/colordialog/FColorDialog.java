package bitnic.dialogfactory.colordialog;

import bitnic.dialogfactory.DialogBase;
import bitnic.senders.IAction;
import bitnic.utils.UtilsOmsk;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.net.URL;

public class FColorDialog  extends DialogBase {
    private final Color color;
    private final IAction<Color> colorIAction;
    public Label label_titul;
    public Label label_content;
    public Button bt_close1, bt_close2;


    public FColorDialog(URL location, Stage stage, Color color, IAction<Color> colorIAction) {
        super(location, stage);
        this.color = color;
        this.colorIAction = colorIAction;
        label_titul.setText("Выбор цвета");
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        UtilsOmsk.painter3d(bt_close1);
    }
}
