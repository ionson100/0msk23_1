package bitnic.dialogfactory.info;

import bitnic.dialogfactory.DialogBase;
import bitnic.utils.UtilsOmsk;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import bitnic.settingscore.SettingAppE;

import java.net.URL;

public class FInfo extends DialogBase{
    public Label label_titul;
    public Label label_content;
    public Button bt_close1,bt_close2;


    public FInfo(URL location, Stage stage,String labelTitul,String labelContent) {
        super(location, stage);
        label_titul.setText(labelTitul);
        label_content.setText(labelContent);
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        UtilsOmsk.painter3d(bt_close1);
//        if(SettingAppE.instance.isTypeshow){
//            bt_close1.getStyleClass().add("button_dialog_3d");
//        }else {
//            bt_close1.getStyleClass().add("button_dialog");
//        }
    }
}
