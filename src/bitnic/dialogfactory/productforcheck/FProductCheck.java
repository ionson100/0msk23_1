package bitnic.dialogfactory.productforcheck;

import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.productforcheck.itemProductforcheck.FItemProductForCheck;
import bitnic.utils.UtilsOmsk;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import javafx.util.Callback;
import bitnic.model.MCheck;
import bitnic.model.MProduct;
import bitnic.pagecheck.FChesk;
import bitnic.pagecheck.itemcheck.FItemCheck;

import java.net.URL;
import java.util.Observable;

public class FProductCheck extends DialogBase {

    public Button bt_close1, bt_close2;
    public ListView list_product;
    private MCheck mCheck;


    public FProductCheck(URL location, Stage stage, MCheck mCheck) {
        super(location, stage);
        this.mCheck = mCheck;
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        ObservableList observableList= FXCollections.observableList(mCheck.mProducts);
        list_product.getItems().addAll(observableList);
        list_product.setCellFactory((Callback<ListView<MProduct>, ListCell<MProduct>>) list -> new CheckCell()
        );
        UtilsOmsk.painter3d(bt_close1);

    }
    static class CheckCell extends ListCell<MProduct> {
        @Override
        public void updateItem(MProduct item, boolean empty) {
            super.updateItem(item, empty);

            if (item != null) {
                FItemProductForCheck check=new FItemProductForCheck(item);
                setGraphic(check);
            }
        }
    }
}
