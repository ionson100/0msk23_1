package bitnic.dialogfactory.question;

import bitnic.dialogfactory.DialogBase;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.net.URL;

public class FQuestion extends DialogBase {

    private final ButtonAction[] buttonActions;
    public Label label_titul;
    public Label label_content;
    public Button bt_close1, bt_close2;
    public HBox bt_host;


    public FQuestion(URL location, Stage stage, String labelTitus, String labelContent,ButtonAction...buttonActions) {
        super(location, stage);

        this.buttonActions = buttonActions;
        label_titul.setText(labelTitus);
        label_content.setText(labelContent);
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        UtilsOmsk.painter3d(bt_close1);
//        if(SettingAppE.instance.isTypeshow){
//            bt_close1.getStyleClass().add("button_dialog_3d");
//        }else {
//            bt_close1.getStyleClass().add("button_dialog");
//        }
        init();
    }
    private void init(){

        for (ButtonAction buttonAction : buttonActions) {
            Button button=new Button();
            if(SettingAppE.instance.isTypeshow){
                button.getStyleClass().add("button_dialog_3d");
            }else {
                button.getStyleClass().add("button_dialog");
            }


            button.setPrefWidth(100);
            button.setText(buttonAction.buttonName);
            button.setOnAction(e->{
                buttonAction.iAction.action(e);
                bt_close1.fire();
            });
            bt_host.getChildren().add(bt_host.getChildren().size()-1,button);
        }

    }
}
