package bitnic.dialogfactory.question;

import bitnic.senders.IAction;

public class ButtonAction {
     final String buttonName;
     final IAction<Object> iAction;

    public ButtonAction(String buttonName, IAction<Object> iAction){

        this.buttonName = buttonName;
        this.iAction = iAction;
    }

}
