package bitnic.dialogfactory.updateapp;

import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.updateapp.itemFile.FItemFileUpdate;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.Starter;
import bitnic.utils.UtilsOmsk;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.util.List;

public class FUpdateApp extends DialogBase {

    private static final Logger log = Logger.getLogger(FUpdateApp.class);
    private final List<File> files;
    public Button bt_close1, bt_close2,bt_run;
    public Label label_titul;
    public ScrollPane scrol_host;
    File selectFile;



    public FUpdateApp(URL location, Stage stage, List<File> files, IAction iAction) {
        super(location, stage);
        this.files = files;


        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        UtilsOmsk.painter3d(bt_close1,bt_run);


        try{
            label_titul.setText("Текущая версия программы - "+String.valueOf(SettingAppE.instance.getVersion()));
        }catch (Exception ex){
            log.error(ex);
        }


        VBox vBox=new VBox();
        for (File file : files) {
            FItemFileUpdate update=new FItemFileUpdate(file, new IAction() {
                @Override
                public boolean action(Object o) {
                    selectFile= (File) o;
                    bt_run.setDisable(false);
                    return false;
                }
            });
            vBox.getChildren().add(update);
        }
        scrol_host.setContent(vBox);
        bt_run.setOnAction(event -> {
            bt_close1.fire();
            iAction.action(selectFile);
        });
    }

}
