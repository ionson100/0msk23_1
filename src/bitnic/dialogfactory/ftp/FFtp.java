package bitnic.dialogfactory.ftp;

import bitnic.dialogfactory.DialogBase;
import bitnic.utils.Pather;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.settingscore.SettingAppE;

import java.net.URL;

public class FFtp extends DialogBase {


    public TextField t_port,  f_file_server, t_server, t_file_local, t_user, t_pasword;
    public ComboBox com_box;
    private SettingAppE app = SettingAppE.instance;


    public Button bt_close1, bt_close2, bt_save;


    public FFtp(URL location, Stage stage) {
        super(location, stage);
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        bt_save.setOnAction(e -> save());
        bt_save.setDisable(true);

        t_port.setText(String.valueOf(app.ftp_port));
        f_file_server.setText(app.ftp_remote_file);
        t_server.setText(app.getUrl());
        t_file_local.setText(Pather.inFilesData);
        t_user.setText(app.ftp_user);
        t_pasword.setText(app.ftp_password);

        t_port.textProperty().addListener((observable, oldValue, newValue) -> validate());
        f_file_server.textProperty().addListener((observable, oldValue, newValue) -> validate());
        t_server.textProperty().addListener((observable, oldValue, newValue) -> validate());
        t_file_local.textProperty().addListener((observable, oldValue, newValue) -> validate());
        t_user.textProperty().addListener((observable, oldValue, newValue) -> validate());
        t_pasword.textProperty().addListener((observable, oldValue, newValue) -> validate());
        com_box.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                validate();
            }
        });
        if(app.ftp_cahrset==null||app.ftp_cahrset.trim().length()==0){
            com_box.getSelectionModel().selectFirst();
        }else {
            com_box.getSelectionModel().select(app.ftp_cahrset);
        }



    }

    private void validate() {

        if(t_port.getText()== null||
                f_file_server.getText()== null||
                t_server.getText()== null ||
                t_file_local.getText()== null ||
                t_user.getText()== null||
                t_pasword.getText()== null){
            bt_save.setDisable(true);
            return;
        }

        if(t_port.getText().trim().length() == 0 ||
                f_file_server.getText().trim().length() == 0 ||
                t_server.getText().trim().length() == 0 ||
                t_file_local.getText().trim().length() == 0 ||
                t_user.getText().trim().length() == 0 ||
                t_pasword.getText().trim().length() == 0){
            bt_save.setDisable(true);
        }else {
            bt_save.setDisable(false);
        }
        int d = 0;
        try {
            d = Integer.parseInt(t_port.getText().trim());
        } catch (Exception ignored) {
            bt_save.setDisable(true);
        }
       if(com_box.getSelectionModel().getSelectedIndex()<=0) {
           bt_save.setDisable(true);
       }

    }

    private void save() {

        app.ftp_remote_file = f_file_server.getText();
        app.ftp_port = Integer.parseInt(t_port.getText().trim());
      
        app.ftp_user = t_user.getText().trim();
        app.ftp_password = t_pasword.getText().trim();

        app.ftp_cahrset= (String) com_box.getValue();
        SettingAppE.save();
        this.handle(null);
    }
}
