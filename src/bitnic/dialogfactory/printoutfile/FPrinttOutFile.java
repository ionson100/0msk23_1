package bitnic.dialogfactory.printoutfile;

import bitnic.dialogfactory.DialogBase;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.utils.DesktopApi;
import bitnic.utils.Pather;

import java.io.File;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FPrinttOutFile extends DialogBase {

    public Button bt_close1, bt_close2, bt_print;
    public TextField tf_print_id;


    public FPrinttOutFile(URL location, Stage stage) {
        super(location, stage);

        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        bt_print.setOnAction(event -> print());
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                tf_print_id.requestFocus();
            }
        });
        UtilsOmsk.painter3d(bt_close1,bt_print);
    }

    private void print() {
        int i = 0;
        try {
            i = Integer.parseInt(tf_print_id.getText().trim());
        } catch (Exception ex) {

        }
        if (i == 0) return;

        Path wiki_path = Paths.get(Pather.sender_report_file);

        Charset charset = Charset.forName("UTF-8");
        try {
            List<String> lines = Files.readAllLines(wiki_path, charset);
            for (String line : lines) {
                int d = line.indexOf(";");
                if (d == -1) continue;
                String s = line.substring(0, d);
                if (s.equals(String.valueOf(i))) {
                    StringBuilder sb = new StringBuilder();
                    String[] strs = line.split(";", -1);
                    int number = 1;
                    for (String str : strs) {
                        sb.append(String.format("Поле %s   %s%s", number, str, System.lineSeparator()));
                        number++;
                    }


                    String pp= Pather.settingsFolder + "/out.txt";
                    Path ball_path = Paths.get(pp);
                    byte[] ball_bytes = sb.toString().getBytes();
                    Files.write(ball_path, ball_bytes);
                    DesktopApi.open(new File(pp));

                }

            }
            for (String line : lines) {
                System.out.println(line);
            }
        } catch (Exception e) {
            System.out.println(e);
        }


    }
}