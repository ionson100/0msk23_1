package bitnic.dialogfactory.appmode;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.utils.UtilsOmsk;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.FactorySettings;
import org.apache.log4j.Logger;

import java.net.URL;

public class FAppMode extends DialogBase {

    private static final Logger log = Logger.getLogger(FAppMode.class);

    public ComboBox com_box;

    private SettingAppE appMode = SettingAppE.instance;


    public Button bt_close1, bt_close2, bt_save;

    public FAppMode(URL location, Stage stage) {
        super(location, stage);

        try{
            UtilsOmsk.painter3d(bt_close1,bt_save);


            bt_close1.setOnAction(this);
            bt_close2.setOnAction(this);
            bt_save.setOnAction(e -> save());
            bt_save.setDisable(true);
            for (String name : appMode.GetNamesProfile()) {
                com_box.getItems().add(name);
            }
            com_box.getSelectionModel().select(appMode.getProfile());

            com_box.getSelectionModel().selectedItemProperty().addListener((observable,
                                                                            oldValue,
                                                                            newValue) -> bt_save.setDisable(false));

        }catch (Exception e){
            DialogFactory.ErrorDialog(e);
            log.error(e);
        }


    }

    private void save() {

        appMode.setProfile(com_box.getSelectionModel().getSelectedIndex());
        SettingAppE.instance= FactorySettings.GetSettingsE(SettingAppE.class);
        bt_close1.fire();
        Controller.RefrachProfileApp();
        //new bitnic.kassa.UserSettungsKmm().Print();
    }
}
