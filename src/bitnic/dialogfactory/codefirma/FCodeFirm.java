package bitnic.dialogfactory.codefirma;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogBase;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.FactorySettings;
import bitnic.utils.UtilsOmsk;

import java.net.URL;

public class FCodeFirm extends DialogBase {


    public Button bt_close1, bt_close2,bt_open_board;
    public TextField tf_id;


    public FCodeFirm(URL location, Stage stage) {
        super(location, stage);

      stage.setOnHidden(new EventHandler<WindowEvent>() {
          @Override
          public void handle(WindowEvent event) {
              if(SettingAppE.instance.getPointId() ==0){
                  Platform.exit();

              }
              SettingAppE.instance= FactorySettings.GetSettingsE(SettingAppE.class);
          }
      });

        tf_id.setText(String.valueOf(SettingAppE.instance.getPointId()));
        bt_close1.setOnAction(e -> close());
        bt_close2.setOnAction(e -> close());
        tf_id.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                SettingAppE.instance.setPointId(Integer.parseInt(newValue));
            } catch (Exception ex) {
                tf_id.setText("0");
                SettingAppE.instance.setPointId(0);
            }
        });
        Platform.runLater(() -> tf_id.requestFocus());
        UtilsOmsk.painter3d(bt_close1,tf_id);
        bt_open_board.setOnAction(a-> {
            UtilsOmsk.openKeyBoard();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    tf_id.requestFocus();
                }
            });
        });



    }

    private void close() {
        Controller.GetInstans().refrashPointid();
        handle(null);
    }
}
