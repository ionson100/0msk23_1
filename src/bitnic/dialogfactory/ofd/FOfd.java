package bitnic.dialogfactory.ofd;

import bitnic.dialogfactory.DialogBase;
import bitnic.dialogfactory.DialogFactory;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.kassa.FactoryOfd;
import bitnic.senders.IAction;

import java.net.URL;

public class FOfd extends DialogBase  {

    public TextField ofd_ip,ofd_port;
    public Button bt_close1,bt_close2,bt_save;
    public FOfd(URL location, Stage stage) {
        super(location, stage);
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        stage.setOnCloseRequest(we -> {
            DialogBase.DeleteBlenda();
        });
        ofd_ip.textProperty().addListener((observable, oldValue, newValue) -> validate());
        ofd_port.textProperty().addListener((observable, oldValue, newValue) -> validate());
        Platform.runLater(() -> ofd_ip.requestFocus());
        bt_save.setDisable(true);
        bt_save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                new bitnic.kassa.FactoryOfd().saveOfd(new FactoryOfd.Ofd(ofd_ip.getText(), Integer.parseInt(ofd_port.getText())), new IAction<String>() {
                    @Override
                    public boolean action(String s) {
                        DialogFactory.InfoDialog("Настройки офд",s);
                        return false;
                    }
                });
            }
        });

        new bitnic.kassa.FactoryOfd().getOfd(new IAction<FactoryOfd.Ofd>() {
            @Override
            public boolean action(FactoryOfd.Ofd ofd) {
                ofd_ip.setText(ofd.ofd_ip);
                ofd_port.setText(String.valueOf(ofd.ofd_port));
                return false;
            }
        });

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                ofd_ip.requestFocus();
            }
        });

        UtilsOmsk.painter3d(bt_close1,bt_save,ofd_ip,ofd_port);
//        if(SettingAppE.instance.isTypeshow){
//            bt_save.getStyleClass().add("button_dialog_3d");
//            bt_close1.getStyleClass().add("button_dialog_3d");
//        }else {
//            bt_save.getStyleClass().add("button_dialog");
//            bt_close1.getStyleClass().add("button_dialog");
//        }

    }
    private   void validate(){
        int i=ofd_ip.getText().trim().length(),i2=ofd_port.getText().trim().length();
        if(i>0&&i2>0){
            bt_save.setDisable(false);
        }else {
            bt_save.setDisable(true);
        }
    }
}
