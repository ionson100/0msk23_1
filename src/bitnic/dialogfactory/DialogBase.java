package bitnic.dialogfactory;

import bitnic.core.Controller;
import bitnic.settingscore.SettingAppE;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

public abstract class DialogBase extends GridPane implements Initializable, EventHandler<ActionEvent> {

    private static final Logger log = Logger.getLogger(DialogBase.class);
    private final Stage stage;

    public DialogBase(URL location, Stage stage) {
        this.stage = stage;

        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(location,bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException e) {
            log.error(e);
            throw new RuntimeException(e);
        }



    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if( SettingAppE.instance.isTypeshow){
            getStyleClass().add("dialog_3d");
        }

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                DeleteBlenda();
            }
        });
    }

    @Override
    public void handle(ActionEvent event) {
        stage.close();
        DeleteBlenda();

    }

    public static void DeleteBlenda() {
        List<Node> deletelist=new ArrayList<>();

        for (int i = 1; i < Controller.Instans.stack_panel.getChildren().size(); i++) {
            deletelist.add(Controller.GetInstans().stack_panel.getChildren().get(i));
        }
        Collections.reverse(deletelist);
        for (Node node : deletelist) {
            Controller.Instans.stack_panel.getChildren().remove(node);
        }
    }
}
