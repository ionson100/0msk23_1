package bitnic.dialogfactory.error;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogBase;
import bitnic.utils.UtilsOmsk;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;

import java.net.URL;

public class CErrorMessage extends DialogBase {

    private final Stage stage;
    private final String error;
    private final IAction iAction;
    public TextArea text_error;
    public Button bt_close1, bt_close2;

    public CErrorMessage(URL location, Stage stage, String error, IAction iAction) {
        super(location, stage);
        this.stage = stage;
        this.error = error;
        this.iAction = iAction;
        text_error.setText(error);
        if (iAction != null) {
            bt_close1.setOnAction(event -> {
                iAction.action(null);
                stage.close();
               DialogBase.DeleteBlenda();
            });
            bt_close2.setOnAction(event -> {
                iAction.action(null);
                stage.close();
                DialogBase.DeleteBlenda();
            });
        }
        else {
            bt_close1.setOnAction(this);
            bt_close2.setOnAction(this);
        }
        UtilsOmsk.painter3d(bt_close1);
//        if(SettingAppE.instance.isTypeshow){
//            bt_close1.getStyleClass().add("button_dialog_3d");
//        }else {
//            bt_close1.getStyleClass().add("button_dialog");
//        }

    }

//    public TextArea text_error;
//    private String bitnic.pageerror;
//    private Stage MyStage;
//    public  Button bt_close1,bt_close2;
//
//
//    public CErrorMessage(String bitnic.pageerror,Stage MyStage) {
//        this.bitnic.pageerror = bitnic.pageerror;
//        this.MyStage = MyStage;
//        GridPane pane;
//
//
//        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_error_message.fxml"));
//        fxmlLoader.setRoot(this);
//        fxmlLoader.setController(this);
//        try {
//            pane = fxmlLoader.load();
//        } catch (IOException exception) {
//            throw new RuntimeException(exception);
//        }
//    }
//
//    public void btClose(ActionEvent actionEvent) {
//        MyStage.close();
//        if (Controller.instans.stack_panel.getChildren().size() > 1) {
//            Object s = Controller.instans.stack_panel.getChildren().
//                    Get(Controller.instans.stack_panel.getChildren().size() - 1);
//            Controller.instans.stack_panel.getChildren().remove(s);
//        }
//    }
//
//
//    @Override
//    public void initialize(URL location, ResourceBundle resources) {
//        text_error.setText(bitnic.pageerror);
//        bt_close1.setOnAction(e->btClose(e));
//        bt_close2.setOnAction(e->btClose(e));
//
//        MyStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
//            public void handle(WindowEvent we) {
//                if (Controller.instans.stack_panel.getChildren().size() > 1) {
//                    Object s = Controller.instans.stack_panel.getChildren().
//                            Get(Controller.instans.stack_panel.getChildren().size() - 1);
//                    Controller.instans.stack_panel.getChildren().remove(s);
//                }
//            }
//        });
//    }
}
