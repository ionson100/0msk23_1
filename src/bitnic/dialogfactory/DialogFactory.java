package bitnic.dialogfactory;

import bitnic.core.Controller;
import bitnic.core.Main;
import bitnic.dialogfactory.appmode.FAppMode;
import bitnic.dialogfactory.barcodeprint.FBarcodePrint;
import bitnic.dialogfactory.checkstory.FCheckStory;
import bitnic.dialogfactory.codefirma.FCodeFirm;
import bitnic.dialogfactory.contributingmoney.FContributingMoney;
import bitnic.dialogfactory.editcountproduct.FEditCountProduct;
import bitnic.dialogfactory.edititemkkm.FEditItemKkm;
import bitnic.dialogfactory.entrymoney.FEntryMoney;
import bitnic.dialogfactory.error.CErrorMessage;
import bitnic.dialogfactory.ftp.FFtp;
import bitnic.dialogfactory.info.FInfo;
import bitnic.dialogfactory.ofd.FOfd;
import bitnic.dialogfactory.printoutfile.FPrinttOutFile;
import bitnic.dialogfactory.productforcheck.FProductCheck;
import bitnic.dialogfactory.question.ButtonAction;
import bitnic.dialogfactory.question.FQuestion;
import bitnic.dialogfactory.reguser.FRegUser;
import bitnic.dialogfactory.statekkm.FStatekkm;
import bitnic.dialogfactory.product.ProfuctHar;
import bitnic.dialogfactory.updateapp.FUpdateApp;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import bitnic.kassa.ItemSettingKkm;
import bitnic.kassa.MStateKKM;
import bitnic.model.MCheck;
import bitnic.model.MProduct;
import bitnic.senders.IAction;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


public class DialogFactory {

    private static final Logger log = Logger.getLogger(DialogFactory.class);


    public static void ErrorDialog(Exception ex) {

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String sStackTrace = sw.toString(); // stack trace as a string
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Stage stage = new Stage();
                URL url = null;
                try {
                    url = new URL(getClass().getResource("/bitnic/dialogfactory/error/f_error_message.fxml").toString());
                } catch (MalformedURLException e) {
                    log.error(e);
                    e.printStackTrace();
                }
                CErrorMessage har = new CErrorMessage(url, stage, sStackTrace,null);
                ActivateDialog(stage, har);
            }
        });
    }


    public static void ErrorDialog(String stre) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Stage stage = new Stage();
                URL url = null;
                try {

                    url = new URL(getClass().getResource("/bitnic/dialogfactory/error/f_error_message.fxml").toString());
                } catch (MalformedURLException e) {
                    log.error(e);
                    e.printStackTrace();
                }
                CErrorMessage har = new CErrorMessage(url, stage, stre,null);
                ActivateDialog(stage, har);
            }
        });

    }

    public static void ErrorDialog(String stre, IAction iAction) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Stage stage = new Stage();
                URL url = null;
                try {
                    url = new URL(getClass().getResource("/bitnic/dialogfactory/error/f_error_message.fxml").toString());
                } catch (MalformedURLException e) {
                    log.error(e);
                    e.printStackTrace();
                }
                CErrorMessage har = new CErrorMessage(url, stage, stre,iAction);
                ActivateDialog(stage, har);
            }
        });

    }


    public static void RollBackDialog(List<File> files, IAction iAction) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Stage stage = new Stage();
                URL url = null;
                try {
                    url = new URL(FUpdateApp.class.getResource("f_update_app.fxml").toString());
                } catch (MalformedURLException e) {
                    log.error(e);
                    e.printStackTrace();
                }
                FUpdateApp har = new FUpdateApp(url, stage, files,iAction);
                ActivateDialog(stage, har);
            }
        });

    }


    public static void ProductDialog(MProduct mProduct) {
        Stage stage = new Stage();
        URL url = null;
        try {
            url = new URL(ProfuctHar.class.getResource("f_product_har.fxml").toString());
        } catch (MalformedURLException e) {
            log.error(e);
            e.printStackTrace();
        }
        ProfuctHar har = new ProfuctHar(url, mProduct, stage);
        ActivateDialog(stage, har);
    }


    public static void ActivateDialog(Stage stage, GridPane har) {


        FXMLLoader loader = new FXMLLoader(DialogFactory.class.getResource("blenda.fxml"));
        Node pane = null;
        try {
            pane = loader.load();
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
        }
        Scene dialog = new Scene(har);
        stage.setScene(dialog);
        Controller.Instans.stack_panel.getChildren().add(pane);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.initOwner(Main.MyStage);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.show();
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);



    }


    public static void SettingsOfdDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FOfd.class.getResource("f_ofd.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FOfd har = new FOfd(url,stage);
            ActivateDialog(stage, har);

        });
    }


    public static void SettingsFtpDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FFtp.class.getResource("f_ftp.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FFtp har = new FFtp(url,stage);
            ActivateDialog(stage, har);

        });
    }


    public static void InfoDialog(String titul, String message) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FInfo.class.getResource("f_info.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FInfo har = new FInfo(url, stage, titul, message);
            ActivateDialog(stage, har);

        });
    }


    public static void PrintBarcodeDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FBarcodePrint.class.getResource("f_barcode_print.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FBarcodePrint har = new FBarcodePrint(url, stage);
            ActivateDialog(stage, har);

        });
    }

    public static void ContributindMoneyDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FContributingMoney.class.getResource("f_contributing_money.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FContributingMoney har = new FContributingMoney(url, stage);
            ActivateDialog(stage, har);

        });
    }

    public static void EntryMoneyDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FEntryMoney.class.getResource("f_entry_money.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FEntryMoney har = new FEntryMoney(url, stage);
            ActivateDialog(stage, har);

        });
    }



    public static void QuestionDialog(String titul, String message, ButtonAction ...buttonActions) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FQuestion.class.getResource("f_question.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FQuestion har = new FQuestion(url, stage, titul, message,buttonActions);
            ActivateDialog(stage, har);

        });
    }


    public static void StateKkmDialog(MStateKKM stateKKM) {



        Platform.runLater(() -> {
            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FStatekkm.class.getResource("f_statekkm.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FStatekkm har = new FStatekkm(url,stage,stateKKM);
            ActivateDialog(stage, har);

        });
    }

    public static void RegCodeFirmDialig() {
        Platform.runLater(() -> {
            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FCodeFirm.class.getResource("f_code_firma.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FCodeFirm har = new FCodeFirm(url,stage);
            ActivateDialog(stage, har);

        });
    }
    public static void RegUserDialog() {
        Platform.runLater(() -> {
            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FRegUser.class.getResource("f_reg_user.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FRegUser har = new FRegUser(url,stage);
            ActivateDialog(stage, har);

        });
    }

    public static void EditItemKkmDialog(ItemSettingKkm value, IAction<Object> iAction) {
        Platform.runLater(() -> {
            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FEditItemKkm.class.getResource("f_edit_item_kkm.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FEditItemKkm har = new FEditItemKkm(url,stage,value,iAction);
            ActivateDialog(stage, har);

        });
    }




    public static void AppModeDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FAppMode.class.getResource("f_app_mode.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FAppMode har = new FAppMode(url,stage);
            ActivateDialog(stage, har);

        });
    }

    public static void EditCountProduct(MProduct mProduct, IAction<Double> iAction) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FEditCountProduct.class.getResource("f_edit_count_product.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FEditCountProduct har = new FEditCountProduct(url,stage,mProduct,iAction);
            ActivateDialog(stage, har);

        });
    }

    public static void ProductForCheckDialog(MCheck mCheck) {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FProductCheck.class.getResource("f_product_check.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FProductCheck har = new FProductCheck(url,stage,mCheck);
            ActivateDialog(stage, har);

        });
    }

    public static void CheckStoryDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FCheckStory.class.getResource("f_check_story.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FCheckStory har = new FCheckStory(url,stage);
            ActivateDialog(stage, har);

        });
    }
    public static void PrintOutFileDialog() {
        Platform.runLater(() -> {

            Stage stage = new Stage();
            URL url = null;
            try {
                url = new URL(FPrinttOutFile.class.getResource("f_print_out_file.fxml").toString());
            } catch (MalformedURLException e) {
                log.error(e);
                e.printStackTrace();
            }
            FPrinttOutFile har = new FPrinttOutFile(url, stage);
            ActivateDialog(stage, har);

        });
    }
}
