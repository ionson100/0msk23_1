package bitnic.dialogfactory.product;

import bitnic.barcodegenerate.BarcodePrint;
import bitnic.dialogfactory.DialogBase;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.model.MBarcodes;
import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.settingscore.SettingAppE;

import java.net.URL;
import java.util.List;

public class ProfuctHar extends DialogBase {

    public Button bt_close1, bt_close2, bt_print;
    public TextField tf_count_barcode;
    public Label label_name;
    public Label label_name_check;
    public Label label_price;
    public Label label_barcode;
    private String barcode;

    public ProfuctHar(URL url, MProduct mProduct, Stage stage) {

        super(url, stage);

        UtilsOmsk.painter3d(bt_close1,tf_count_barcode,bt_print);
//        if(SettingAppE.instance.isTypeshow){
//            tf_count_barcode.getStyleClass().add("delivery_3d");
//            bt_close1.getStyleClass().add("button_dialog_3d");
//            bt_print.getStyleClass().add("button_dialog_3d");
//        }else {
//            tf_count_barcode.getStyleClass().add("delivery");
//            bt_close1.getStyleClass().add("button_dialog");
//            bt_print.getStyleClass().add("button_dialog");
//        }
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        stage.setOnCloseRequest(we -> {
            DialogBase.DeleteBlenda();
        });
        label_name.setText(mProduct.name);
        label_name_check.setText(mProduct.name_check);
        label_price.setText(String.valueOf(mProduct.price) + " руб.");
        List<MBarcodes> mBarcodes= Configure.GetSession().getList(MBarcodes.class," id_product = ? ",mProduct.code_prod);
        if(mBarcodes.size()==0){
            label_barcode.setText("Barcode отсутствует");
        }else {
            barcode=mBarcodes.get(0).barcode;
            label_barcode.setText(barcode);
        }
        tf_count_barcode.textProperty().addListener((observable, oldValue, newValue) -> {
            int i;
            try{

                i=Integer.parseInt(newValue);
                if(i>0){
                    SettingAppE.instance.copy=i;
                    SettingAppE.save();
                }
            }catch (Exception ex){
                tf_count_barcode.setText("");
            }
        });
        tf_count_barcode.setText(String.valueOf(SettingAppE.instance.copy));
        bt_print.setOnAction(event -> {
           if(barcode==null||barcode.trim().length()==0){
               return;
           }
           new BarcodePrint().Print(barcode);
        });

        Platform.runLater(() -> tf_count_barcode.requestFocus());



    }
}
