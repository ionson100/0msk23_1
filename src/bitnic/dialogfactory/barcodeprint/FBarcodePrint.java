package bitnic.dialogfactory.barcodeprint;

import bitnic.barcodegenerate.BarcodePrint;
import bitnic.dialogfactory.DialogBase;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.settingscore.SettingAppE;
import org.apache.log4j.Logger;


import java.net.URL;

public class FBarcodePrint extends DialogBase {
    private static final Logger log = Logger.getLogger(FBarcodePrint.class);
    public Button bt_close1, bt_close2, bt_print;
    public TextField tf_count, tf_barcode;


    public FBarcodePrint(URL location, Stage stage) {
        super(location, stage);
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);

        tf_count.textProperty().addListener((observable, oldValue, newValue) -> {
            int i;

            try {
                i = Integer.parseInt(newValue.trim());
                if (i > 0) {
                    SettingAppE.instance.copy = i;
                    SettingAppE.save();
                }
            } catch (Exception ex) {
                tf_barcode.setText("1");
                log.error(ex);

            }
        });
        tf_count.setText(String.valueOf(SettingAppE.instance.copy));
        bt_print.setOnAction(event -> {
            if (tf_barcode.getText().trim().length() > 0) {
                new BarcodePrint().Print(tf_barcode.getText());
            }
        });
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                tf_barcode.requestFocus();
            }
        });

        UtilsOmsk.painter3d(bt_close1,bt_print,tf_count,tf_barcode);

    }
}
