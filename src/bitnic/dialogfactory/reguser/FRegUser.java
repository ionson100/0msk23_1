package bitnic.dialogfactory.reguser;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogBase;
import bitnic.header.Cheader;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.model.MUser;
import bitnic.orm.Configure;
import bitnic.pagesale.Cmainform;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.List;

public class FRegUser extends DialogBase {
    private static final Logger log = Logger.getLogger(FRegUser.class);
    public Button bt_close1, bt_close2, bt_save,bt_open_board;
    public TextField tf_pwd;
    public Label label_error;


    public FRegUser(URL location, Stage stage) {
        super(location, stage);

        stage.setOnHidden(event -> {
            String str = SettingAppE.instance.user_id;
            List<MUser> mUsers = Configure.GetSession().getList(MUser.class, " password = ? ", str);
            if (mUsers.size() == 0) {
                label_error.setText("Пользователь не найден!");
                //Platform.exit();
               // FactorySettings.GetSettingsE(SettingAppE.class);
            }
        });

        UtilsOmsk.painter3d(bt_close1,bt_save,tf_pwd);
//        if(SettingAppE.instance.isTypeshow){
//            tf_pwd.getStyleClass().add("delivery_3d");
//            bt_close1.getStyleClass().add("button_dialog_3d");
//            bt_save.getStyleClass().add("button_dialog_3d");
//        }else {
//            tf_pwd.getStyleClass().add("delivery");
//            bt_close1.getStyleClass().add("button_dialog");
//            bt_save.getStyleClass().add("button_dialog");
//        }
        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        bt_save.setOnAction(event -> {

            String str = tf_pwd.getText();
            if(str.equals(SettingAppE.instance.defUser)){
                SettingAppE.instance.user_id="1";
                SettingAppE.save();
                SettingAppE.RefrashInstance();
                Controller.GetInstans().refrashMenu();
                Cheader.RefrashUser();
                Controller.GetInstans().factoryPage(new Cmainform());


                bt_close1.fire();
                //new bitnic.kassa.UserSettungsKmm().Print();
            }else {
                List<MUser> mUsers = Configure.GetSession().getList(MUser.class, " password = ? or map_user = ?", str, str);
                if (mUsers.size() > 0) {
                    MUser user = mUsers.get(0);
                    SettingAppE.instance.user_id=user.cod;
                    SettingAppE.save();
                    SettingAppE.RefrashInstance();
                    Controller.GetInstans().refrashMenu();
                    Controller.GetInstans().factoryPage(new Cmainform());
                    Cheader.RefrashUser();

                    log.info("Вход пользователя: "+mUsers.get(0).name +" id: "+mUsers.get(0).cod);


                    bt_close1.fire();

                }else {
                    label_error.setText("Пользователь не найден!");
                }
            }

        });


        tf_pwd.textProperty().addListener((observable, oldValue, newValue) -> {
            validate(newValue);
            if (newValue.contains("\r")) {
                bt_save.fire();
            }
        });




        Platform.runLater(() -> tf_pwd.requestFocus());
        tf_pwd.setOnKeyPressed(event -> {
            String a = event.getText();
            if (a.equals("\r")) {
                bt_save.fire();
            }

        });

        bt_open_board.setOnAction(a-> {
            UtilsOmsk.openKeyBoard();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    tf_pwd.requestFocus();
                }
            });
        });
    }


    private void validate(String str) {
        if (str == null || str.length() == 0) {
            bt_save.setDisable(true);
        } else {
            bt_save.setDisable(false);
        }
    }

}
