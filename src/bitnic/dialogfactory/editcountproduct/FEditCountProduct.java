package bitnic.dialogfactory.editcountproduct;

import bitnic.dialogfactory.DialogBase;
import bitnic.model.MBarcodes;
import bitnic.orm.Configure;
import bitnic.utils.UtilsOmsk;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.model.MProduct;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;


import java.net.URL;
import java.util.List;

public class FEditCountProduct extends DialogBase {

    private boolean first;
    private  MProduct mProduct;
    private  IAction<Double> iAction;
    public Button bt_close1,bt_close2,bt_update,bt_delete;
    public TextField tf_value;
    public Label label_name,label_barcode;
    public Button bt_1,bt_2,bt_3,bt_4,bt_5,bt_6,bt_7,bt_8,bt_9,bt_10,bt_del,bt_del2;

    double d=0;

    public FEditCountProduct(URL location, Stage stage, MProduct mProduct, IAction<Double> iAction) {
        super(location, stage);
        this.mProduct = mProduct;
        this.iAction = iAction;

        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        tf_value.setText(String.valueOf(mProduct.selectAmount));
        label_name.setText(mProduct.name_check);

        tf_value.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                try{
                  d=  mProduct.selectAmount=Double.parseDouble(newValue.replace(",","."));
                }catch (Exception ex){
                   d=0;
                }

            }

        });




        bt_update.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    mProduct.selectAmount=d;
                }catch (Exception ex){
                    tf_value.setText("0");
                }
                if(iAction!=null){
                    iAction.action(mProduct.selectAmount);
                }
                bt_close1.fire();
                SettingAppE.save();
            }
        });

        bt_delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                if(iAction!=null){
                    SettingAppE.instance.selectProduct.remove(mProduct);
                    SettingAppE.save();
                    iAction.action(-1d);

                }
                bt_close1.fire();
            }
        });
        bt_1.setOnAction(event -> action(event));
        bt_2.setOnAction(event -> action(event));
        bt_3.setOnAction(event -> action(event));
        bt_4.setOnAction(event -> action(event));
        bt_5.setOnAction(event -> action(event));
        bt_6.setOnAction(event -> action(event));
        bt_7.setOnAction(event -> action(event));
        bt_8.setOnAction(event -> action(event));
        bt_8.setOnAction(event -> action(event));
        bt_9.setOnAction(event -> action(event));
        bt_10.setOnAction(event -> action(event));
        bt_del.setOnAction(event -> action(event));
        bt_del2.setOnAction(event -> action(event));

       UtilsOmsk.painter3d(tf_value,bt_close1,bt_update,bt_delete);
        if(SettingAppE.instance.isTypeshow){

            bt_1.getStyleClass().add("button_key_big_3d");
            bt_2.getStyleClass().add("button_key_big_3d");
            bt_3.getStyleClass().add("button_key_big_3d");
            bt_4.getStyleClass().add("button_key_big_3d");
            bt_5.getStyleClass().add("button_key_big_3d");
            bt_6.getStyleClass().add("button_key_big_3d");
            bt_7.getStyleClass().add("button_key_big_3d");
            bt_8.getStyleClass().add("button_key_big_3d");
            bt_8.getStyleClass().add("button_key_big_3d");
            bt_9.getStyleClass().add("button_key_big_3d");
            bt_10.getStyleClass().add("button_key_big_3d");
            bt_del.getStyleClass().add("button_key_big_3d");
            bt_del2.getStyleClass().add("button_key_big_3d");


        }else {

            bt_1.getStyleClass().add("button_key_big");
            bt_2.getStyleClass().add("button_key_big");
            bt_3.getStyleClass().add("button_key_big");
            bt_4.getStyleClass().add("button_key_big");
            bt_5.getStyleClass().add("button_key_big");
            bt_6.getStyleClass().add("button_key_big");
            bt_7.getStyleClass().add("button_key_big");
            bt_8.getStyleClass().add("button_key_big");
            bt_8.getStyleClass().add("button_key_big");
            bt_9.getStyleClass().add("button_key_big");
            bt_10.getStyleClass().add("button_key_big");
            bt_del.getStyleClass().add("button_key_big");
            bt_del2.getStyleClass().add("button_key_big");


        }

        bt_1.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_2.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_3.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_4.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_5.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_6.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_7.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_8.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_8.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_9.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_10.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_del.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);
        bt_del2.setStyle("-fx-font-size: "+SettingAppE.instance.fontSizeDeliveryButtonCountProduct);


        List<MBarcodes>  barcodesList= Configure.GetSession().getList(MBarcodes.class," id_product =?",mProduct.code_prod);
        if(barcodesList.size()==0){

            label_barcode.setText("Не найден");

        }else {
            label_barcode.setText(barcodesList.get(0).barcode);
        }





        Platform.runLater(() -> tf_value.requestFocus());
    }
    void action(ActionEvent event){

        Button button= (Button) event.getSource();
        String id=button.getId();
        switch (id){
            case "bt_1":{


                if(first==false){
                    tf_value.setText("1");
                    first=true;
                }else {
                    tf_value.setText(tf_value.getText()+"1");
                }

                break;
            }
            case "bt_2":{
                if(first==false){
                    tf_value.setText("2");
                    first=true;
                }else {
                    tf_value.setText(tf_value.getText()+"2");
                }
                break;
            }
            case "bt_3":{
                if(first==false){
                    tf_value.setText("3");
                    first=true;
                }else {
                    tf_value.setText(tf_value.getText()+"3");
                }
                break;
            }
            case "bt_4":{
                if(first==false){
                    tf_value.setText("4");
                    first=true;
                }else {
                    tf_value.setText(tf_value.getText()+"4");
                }
                break;
            }
            case "bt_5":{
                if(first==false){
                    tf_value.setText("5");
                    first=true;
                }else {
                    tf_value.setText(tf_value.getText()+"5");
                }
                break;
            }
            case "bt_6":{
                if(first==false){
                    tf_value.setText("6");
                    first=true;
                }else {
                    tf_value.setText(tf_value.getText()+"6");
                }
                break;
            }
            case "bt_7":{
                if(first==false){
                    tf_value.setText("7");
                    first=true;
                }else {
                    tf_value.setText(tf_value.getText()+"7");
                }
                break;
            }
            case "bt_8":{
                if(first==false){
                    tf_value.setText("8");
                    first=true;
                }else {
                    tf_value.setText(tf_value.getText()+"8");
                }
                break;
            }
            case "bt_9":{
                if(first==false){
                    tf_value.setText("9");
                    first=true;
                }else {
                    tf_value.setText(tf_value.getText()+"9");
                }
                break;
            }
            case "bt_10":{
                if(first==false){
                    tf_value.setText("0");
                    first=true;
                }else {
                    tf_value.setText(tf_value.getText()+"0");
                }
                break;
            }
            case "bt_del":{

                if(first==false){
                    tf_value.setText("");
                    first=true;
                }else {
                    String s=tf_value.getText();

                    if(s.length()==0){
                        tf_value.setText("");
                    }else {
                        tf_value.setText(s.substring(0,s.length()-1));
                    }
                }
                break;
            }
            case "bt_del2":{
                tf_value.setText("");
                break;
            }
        }
    }
}
