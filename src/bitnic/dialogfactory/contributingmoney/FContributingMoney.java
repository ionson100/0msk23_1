package bitnic.dialogfactory.contributingmoney;

import bitnic.dialogfactory.DialogBase;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Transaction;
import bitnic.utils.UtilsOmsk;

import java.net.URL;

public class FContributingMoney extends DialogBase {

    private double aDouble;
    public  TextField tf_summ;
    public Button bt_close1,bt_close2,bt_execute,bt_open_board;


    public FContributingMoney(URL location, Stage stage) {
        super(location, stage);

        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);

        tf_summ.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try{
                   aDouble=Double.parseDouble(tf_summ.getText().trim().replace(",","."));
                }catch (Exception ex){
                    aDouble=0;
                    tf_summ.setText("0");
                }
            }
        });


        bt_execute.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(aDouble==0d) return;
               bt_close1.fire();
               new Transaction().casheIn(aDouble);
            }
        });
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                tf_summ.requestFocus();
            }
        });

        UtilsOmsk.painter3d(bt_close1,tf_summ,bt_execute);

        bt_open_board.setOnAction(a-> {
            UtilsOmsk.openKeyBoard();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    tf_summ.requestFocus();
                }
            });
        });
    }
}
