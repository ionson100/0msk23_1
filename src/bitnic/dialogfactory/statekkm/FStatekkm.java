package bitnic.dialogfactory.statekkm;

import bitnic.core.Controller;
import bitnic.dialogfactory.DialogBase;
import bitnic.utils.support;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import bitnic.kassa.MStateKKM;
import bitnic.utils.UtilsOmsk;
import javafx.stage.Stage;

import java.net.URL;

public class FStatekkm extends DialogBase {


    public Label port_ofd,kkm_mode,addrres_ofd,date_first,not_send_ofd,kkm_pwd, number_kkm,kkm_date,kkm_user_name,label_session;

    public Button bt_close1,bt_close2;

    public FStatekkm(URL location, Stage stage,MStateKKM kkm) {
        super(location, stage);


        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        kkm_user_name.setText(kkm.username);
        //  kkm_date.setText(UtilsOmsk.simpleDateFormatE(kkm.date));
        number_kkm.setText(kkm.serialNumer);
        kkm_pwd.setText(kkm.userPassword);
        not_send_ofd.setText(String.valueOf(kkm.notSenderOfd));
        date_first.setText(UtilsOmsk.simpleDateFormatE(kkm.notSenderFirstDate));
        addrres_ofd.setText(kkm.ofdUrl);
        port_ofd.setText(String.valueOf(kkm.ofdPort));
        kkm_mode.setText(String.valueOf(kkm.mode));
        label_session.setText(String.valueOf(kkm.session));

        Controller.WriteMessage(support.str("name23"));

        UtilsOmsk.painter3d(bt_close1);
    }

    public void btClose() {
        DialogBase.DeleteBlenda();
    }




}
