package bitnic.dialogfactory.entrymoney;

import bitnic.dialogfactory.DialogBase;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import bitnic.settingscore.SettingAppE;
import bitnic.transaction.Transaction;
import bitnic.utils.UtilsOmsk;

import java.net.URL;

public class FEntryMoney extends DialogBase {
    private double aDouble;
    public TextField tf_summ;
    public Button bt_close1,bt_close2,bt_execute,bt_open_board;


    public FEntryMoney(URL location, Stage stage) {
        super(location, stage);


        bt_close1.setOnAction(this);
        bt_close2.setOnAction(this);
        tf_summ.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try{
                    aDouble=Double.parseDouble(tf_summ.getText().trim().replace(",","."));
                }catch (Exception ex){
                    aDouble=0;
                    tf_summ.setText("0");
                }
            }
        });


        bt_execute.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(aDouble==0d) return;
                bt_close1.fire();

                new Transaction().cashOut(aDouble);
                //new  bitnic.kassa.EntryMoney().Run(aDouble);
            }
        });

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                tf_summ.requestFocus();
            }
        });

        UtilsOmsk.painter3d(bt_close1,bt_execute,tf_summ);
//        if(SettingAppE.instance.isTypeshow){
//            bt_close1.getStyleClass().add("button_dialog_3d");
//            bt_execute.getStyleClass().add("button_dialog_3d");
//            tf_summ.getStyleClass().add("delivery_3d");
//        }else {
//            bt_close1.getStyleClass().add("button_dialog");
//            bt_execute.getStyleClass().add("button_dialog");
//            tf_summ.getStyleClass().add("delivery");
//        }
        bt_open_board.setOnAction(a-> {
            UtilsOmsk.openKeyBoard();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    tf_summ.requestFocus();
                }
            });
        });
    }
}
