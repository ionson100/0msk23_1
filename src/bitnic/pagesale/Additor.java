package bitnic.pagesale;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import bitnic.model.MProduct;
import bitnic.pagesale.itemsproduct.ItemProductCheck;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;



public class Additor {

    private IAction updateIAction;
    private ListView box;
    public  void refresh(ListView box, IAction updateIAction) {

        this.box = box;
        box.getItems().clear();
        this.updateIAction=updateIAction;
        ObservableList observableList = FXCollections.observableArrayList();
        int i=0;
        for (MProduct mProduct : SettingAppE.instance.selectProduct) {
            mProduct.indexForCheck=++i;
            observableList.add(mProduct);
        }
        box.setCellFactory(param -> new CheckCell());
        box.setItems(observableList);
    }
    class CheckCell extends ListCell<MProduct> {

        @Override
        public void updateItem(MProduct item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null) {
                ItemProductCheck check = new ItemProductCheck(item, new IAction() {
                    @Override
                    public boolean action(Object o) {
                        updateIAction.action(null);
                        return false;
                    }
                });
                check.setPrefWidth(box.getWidth()-50);
                setGraphic(check);
            }
        }
    }
}
