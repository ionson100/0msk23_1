package bitnic.pagesale.stacksearcher.itenstacksearcher;

import bitnic.pagesale.CustomScrollEvent;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import bitnic.model.MBarcodes;
import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class FItemStackSearcher extends GridPane implements Initializable {

    private double lastYposition = 0;
    public Button bt_add;

    public Label laabel_price, laabel_self_amount, label_name;
    private MProduct mProduct;
    private IAction iAction;

    public FItemStackSearcher(MProduct mProduct, IAction iAction) {
        this.mProduct = mProduct;
        this.iAction = iAction;
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_item_stack_searcher.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if (SettingAppE.instance.isTypeshow) {
            bt_add.getStyleClass().add("button_key_big_3d");
        } else {
            bt_add.getStyleClass().add("button_key_big");
        }
        String css = "-fx-font-size: " + SettingAppE.instance.fontSizeSelectProduct;
        laabel_price.setStyle(laabel_price.getStyle() + ";" + css);
        laabel_self_amount.setStyle(laabel_self_amount.getStyle() + ";" + css);
        label_name.setStyle(label_name.getStyle() + ";" + css);
        ;
        label_name.setText(mProduct.name_check);
        laabel_price.setText(String.valueOf(mProduct.price));
        laabel_self_amount.setText(String.valueOf(mProduct.amount_self));
        // List<MBarcodes> mBarcodes = Configure.getSession().getList(MBarcodes.class, " id_product = ?", mProduct.code_prod);

        bt_add.setOnAction(event -> {


            iAction.action(mProduct);

        });

        this.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                lastYposition = event.getSceneY();
            }
        });

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });

    }
}
