package bitnic.pagesale.itemsproduct;

import bitnic.dialogfactory.DialogFactory;
import bitnic.pagesale.CustomScrollEvent;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;

import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import bitnic.model.MProduct;
import bitnic.utils.UtilsOmsk;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;


public class ItemProductCheck extends GridPane implements Initializable {

    private double lastYposition = 0;
    private boolean isFirst = true;
    private boolean isShow;

    @FXML
    public Label label_name, label_price, labe_summ, label_name2, label_index;
    public Label label_amount;

    GridPane pane;
    private MProduct mProduct;
    private IAction updateIAction;
    private int index;

    public ItemProductCheck(MProduct mProduct, IAction updateIAction) {
        this.mProduct = mProduct;
        this.updateIAction = updateIAction;
        this.index = mProduct.indexForCheck;


        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("item_product.fxml"), bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        setUserData(mProduct);

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {



        this.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                isShow = true;
                lastYposition = event.getSceneY();
            }
        });

        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                isShow=false;
                double newYposition = event.getSceneY();
                double diff = newYposition - lastYposition;
                lastYposition = newYposition;
                CustomScrollEvent cse = new CustomScrollEvent();
                cse.fireVerticalScroll((int) diff, this, (EventTarget) event.getSource());
            }
        });









        String css = "-fx-font-size: " + SettingAppE.instance.fontSizeProduct;
        label_name.setStyle(label_name.getStyle() + ";" + css);
        label_price.setStyle(label_price.getStyle() + ";" + css);
        labe_summ.setStyle(labe_summ.getStyle() + ";" + css);
        label_amount.setStyle(label_amount.getStyle() + ";" + css);
        label_index.setStyle(label_index.getStyle() + ";" + css);
        ;

        this.setOnMouseClicked ( new EventHandler <MouseEvent> () {
            @Override
            public void handle ( MouseEvent event ) {
                if(event.getClickCount()==2)
                DialogFactory.EditCountProduct(mProduct, aDouble -> {
                    if (aDouble != null) {
                        label_amount.setText(String.valueOf(aDouble));
                        labe_summ.setText(String.valueOf(UtilsOmsk.round(mProduct.selectAmount * mProduct.price, 2)));
                    } else {
                        SettingAppE.instance.selectProduct.remove(mProduct);
                        SettingAppE.save();
                    }
                    updateIAction.action(null);

                    return false;
                });
            }
        } );

        label_index.setText(String.valueOf(index));
        label_name.setText(mProduct.name_check);
        label_name2.setText(mProduct.name);
        label_amount.setText(String.valueOf(mProduct.selectAmount));
        label_price.setText(String.valueOf(mProduct.price));
        labe_summ.setText(String.valueOf(mProduct.price * mProduct.selectAmount));

        label_amount.textProperty().addListener((observable, oldValue, newValue) -> {
            if (isFirst) {
                label_amount.setText(label_amount.getText().replace(oldValue, ""));
                isFirst = false;

            }
            if (label_amount.getText().trim().equals("0")) {
                label_amount.setText(newValue);
            }

            double amoumt = 0d;
            try {
                amoumt = Double.parseDouble(newValue);
            } catch (Exception ex) {
                amoumt = 0;
                label_amount.setText("0");
            }
            mProduct.selectAmount = amoumt;
            updateIAction.action(null);
            SettingAppE.save();
        });
    }
}
