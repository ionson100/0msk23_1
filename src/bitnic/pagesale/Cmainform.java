package bitnic.pagesale;


import bitnic.core.Controller;
import bitnic.core.IDestroy;
import bitnic.dialogfactory.DialogFactory;
import bitnic.dialogfactory.question.ButtonAction;
import bitnic.utils.support;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import bitnic.keyboard.KeyKeyB;
import bitnic.model.MBarcodes;
import bitnic.model.MProduct;
import bitnic.orm.Configure;
import bitnic.pagesale.blender.FUserBlender;
import bitnic.pagetableproduct.IFocusable;
import bitnic.senders.IAction;
import bitnic.settingscore.SettingAppE;
import bitnic.utils.UtilsOmsk;
import bitnic.transaction.Transaction;

import bitnic.utils.WatcherScaner;
import org.apache.log4j.Logger;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.List;

public class Cmainform extends GridPane implements Initializable, IDestroy, IFocusable, IRefrashUser, ICleatTextField {
    private ResourceBundle bundle;
    private static final Logger log = Logger.getLogger(Cmainform.class);

    public StackPane stack21, stack22;
    public Label label_itogo_amount;
    public Label label_itogo_summ;
    public WatcherScaner watcherScaner;
    public static KeyKeyB keyKeyB;
    private TextField currentTextField;


    public GridPane grid_delivery_host, grid_buttons_host, grid_blue_top, grid_blue_bottom, grid_pane_itogo;
    public static Cmainform cmainform;
    public TextField kb_text_field;
    public Button bt_clear, bt_50, bt_100, bt_200, bt_500, bt_1000, bt_2000, bt_5000;
    private Button bt_print_check,bt_print_check2;
    public TextField tb_get_many, tb_delivery, tb_check_summ;
    double widthHost = 100d;
    @FXML
    GridPane pane;
    @FXML
    GridPane grid1;
    @FXML
    ListView box;
    @FXML
    Button bt_2;
    @FXML
    Label label_nds;
    @FXML
    Label label_nds2;
    @FXML
    private GridPane keyboard;

    public Cmainform() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResource("aa_ru_RU.properties").openStream();
            ResourceBundle bundle = new PropertyResourceBundle(inputStream);
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("f_sale.fxml"),bundle);
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            pane = fxmlLoader.load();
        } catch (IOException exception) {
            log.error(exception);
            throw new RuntimeException(exception);
        }
        cmainform = this;
    }

    List<MBarcodes> mBarcodes = new ArrayList<>();
    StringBuilder s = new StringBuilder("");


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        Controller.WriteMessage(support.str("name1"));

        bundle=resources;
        String css = "-fx-font-size: " + SettingAppE.instance.fontSizeDelivery;

        tb_get_many.setStyle(css);
        tb_delivery.setStyle(css);
        tb_check_summ.setStyle(css);

        if (SettingAppE.instance.isTypeshow) {//button_delivery_clear,button_key_big
            tb_get_many.getStyleClass().add("delivery_3d");
            tb_check_summ.getStyleClass().add("delivery_3d");
            tb_delivery.getStyleClass().add("delivery_3d");
            bt_clear.getStyleClass().add("button_delivery_clear_3d");
            bt_clear.getStyleClass().add("button_key_big_3d");
            grid_blue_top.getStyleClass().add("left_grid_head_3d");
            grid_blue_bottom.getStyleClass().add("left_grid_head_3d");
            grid_pane_itogo.getStyleClass().add("left_grid_head_3d");

            bt_clear.getStyleClass().add("button_key_big_3d");


            bt_50.getStyleClass().add("button_key_big_3d");
            bt_100.getStyleClass().add("button_key_big_3d");
            bt_200.getStyleClass().add("button_key_big_3d");
            bt_500.getStyleClass().add("button_key_big_3d");
            bt_1000.getStyleClass().add("button_key_big_3d");
            bt_2000.getStyleClass().add("button_key_big_3d");
            bt_5000.getStyleClass().add("button_key_big_3d");

            bt_50.setText("50");
            bt_100.setText("100");
            bt_200.setText("100");
            bt_500.setText("500");
            bt_1000.setText("1000");
            bt_2000.setText("2000");
            bt_5000.setText("5000");


        } else {
            bt_50.getStyleClass().add("button_money_50");
            bt_100.getStyleClass().add("button_money_100");
            bt_200.getStyleClass().add("button_money_200");
            bt_500.getStyleClass().add("button_money_500");
            bt_1000.getStyleClass().add("button_money_1000");
            bt_2000.getStyleClass().add("button_money_2000");
            bt_5000.getStyleClass().add("button_money_5000");

            bt_clear.getStyleClass().add("button_delivery_clear");
            grid_blue_top.getStyleClass().add("left_grid_head");
            grid_blue_bottom.getStyleClass().add("left_grid_head");
            grid_pane_itogo.getStyleClass().add("ritch_grid_hed");
        }

        bt_50.setStyle("-fx-font-size: " + SettingAppE.instance.fontSizeDeliveryButton);
        bt_100.setStyle("-fx-font-size: " + SettingAppE.instance.fontSizeDeliveryButton);
        bt_200.setStyle("-fx-font-size: " + SettingAppE.instance.fontSizeDeliveryButton);
        bt_500.setStyle("-fx-font-size: " + SettingAppE.instance.fontSizeDeliveryButton);
        bt_1000.setStyle("-fx-font-size: " + SettingAppE.instance.fontSizeDeliveryButton);
        bt_2000.setStyle("-fx-font-size: " + SettingAppE.instance.fontSizeDeliveryButton);
        bt_5000.setStyle("-fx-font-size: " + SettingAppE.instance.fontSizeDeliveryButton);

        label_itogo_amount.setText("");
        label_itogo_summ.setText("");

        keyKeyB.iActionDeletecheck = new IAction<String>() {
            @Override
            public boolean action(String s) {
                if (SettingAppE.instance.selectProduct.size() > 0) {
                    DialogFactory.QuestionDialog(bundle.getString("name114"),
                            bundle.getString("name115"), new ButtonAction("Да", new IAction<Object>() {
                                @Override
                                public boolean action(Object o) {
                                    SettingAppE.instance.selectProduct.clear();
                                    SettingAppE.save();
                                    refrachCheck();
                                    return false;
                                }
                            }));
                }


                return false;
            }
        };

        keyKeyB.iActionKeyPress = s -> {
            switch (s) {
                case "0": {
                    printableDelivery("0");
                    break;
                }
                case "1": {

                    printableDelivery("1");

                    break;
                }
                case "2": {
                    printableDelivery("2");
                    break;
                }
                case "3": {
                    printableDelivery("3");
                    break;
                }
                case "4": {
                    printableDelivery("4");
                    break;
                }
                case "5": {
                    printableDelivery("5");
                    break;
                }
                case "6": {
                    printableDelivery("6");
                    break;
                }
                case "7": {
                    printableDelivery("7");
                    break;
                }
                case "8": {
                    printableDelivery("8");
                    break;
                }
                case "9": {
                    printableDelivery("9");
                    break;
                }
                case "88": {
                    if (currentTextField.getText().length() > 0) {
                        currentTextField.setText(currentTextField.getText().substring(0, currentTextField.getText().length() - 1));
                    } else {
                        watcherScaner.clear();
                    }

                    break;
                }

                case "888": {
                    if (currentTextField == kb_text_field) {
                        watcherScaner.addProduct(kb_text_field.getText());
                    }

                    break;
                }
                case "8888": {
                    currentTextField.setText("");
                    watcherScaner.clear();
                    break;
                }

            }
            return false;
        };


        if (kb_text_field == null) {
            kb_text_field = keyKeyB.kb_text_field;
        }


        String css2 = "-fx-font-size: " + SettingAppE.instance.fontSizeDeliverySearch;
        kb_text_field.setStyle(css2);


        if (bt_print_check == null) {
            bt_print_check = (Button) keyboard.lookup("#bt_print_check");
        }

        if(bt_print_check2==null){
            bt_print_check2=(Button) keyboard.lookup("#bt_print_check2");
        }

        bt_print_check.setOnAction(e -> onPrintCheck());
        bt_print_check2.setOnAction(e->onPrintCheckCard());


        currentTextField = kb_text_field;

        kb_text_field.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                textFieldFocusable(kb_text_field);
            }
        });

        tb_get_many.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                textFieldFocusable(tb_get_many);
            }
        });


        if (SettingAppE.instance.isTypeshow) {
            kb_text_field.getStyleClass().add("delivery_3d");
        }


        grid_delivery_host.setOnMouseClicked(event -> {

            if (tb_get_many == currentTextField) return;
            textFieldFocusable(tb_get_many);

        });
        grid_buttons_host.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                if (kb_text_field == currentTextField) return;
                textFieldFocusable(kb_text_field);

            }
        });


        UtilsOmsk.resizeNode(this, grid1);


        mBarcodes = Configure.GetSession().getList(MBarcodes.class, null);


       // box.widthProperty().addListener((observableValue, oldSceneWidth, newSceneWidth) ->
      //          widthHost = (Double) newSceneWidth);
        ////////////////////////////////

        tb_check_summ.textProperty().addListener((observable, oldValue, newValue) -> recalculation());
        tb_get_many.textProperty().addListener((observable, oldValue, newValue) -> recalculation());

        bt_50.setOnAction(event -> onManyAction(event));
        bt_200.setOnAction(event -> onManyAction(event));
        bt_100.setOnAction(event -> onManyAction(event));
        bt_1000.setOnAction(event -> onManyAction(event));
        bt_500.setOnAction(event -> onManyAction(event));
        bt_2000.setOnAction(event -> onManyAction(event));
        bt_5000.setOnAction(event -> onManyAction(event));
        bt_clear.setOnAction(event -> tb_get_many.setText(String.valueOf(0)));

        refrachCheck();
        textFieldFocusable(kb_text_field);
        tb_get_many.setDisable(true);


        //box.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        refrashUser();

    }

    private void onPrintCheckCard() {

        List<MProduct> mProducts = SettingAppE.instance.selectProduct;
        if (mProducts.size() == 0) return;
        List<MProduct> selectProduct = new ArrayList<>(mProducts);

        new Transaction().salleCacheCard(selectProduct,"0000000000000000");

    }

    private void printableDelivery(String d) {
        if (currentTextField == tb_get_many && tb_get_many.getText().equals("0")) {
            currentTextField.setText(d);
        } else {
            currentTextField.setText(currentTextField.getText() + d);
        }
    }


    void textFieldFocusable(TextField textFocus) {

        //    currentTextField.setStyle("-fx-border-color: chartreuse;-fx-border-width: 0;");
        //     textFocus.setStyle("-fx-border-color: chartreuse;-fx-border-width: 3;");
        currentTextField = textFocus;
        Platform.runLater(() -> textFocus.requestFocus());

//        String css = ";-fx-font-size: " + SettingAppE.instance.fontSizeDeliverySearch;
//        if (currentTextField == kb_text_field) {
//            currentTextField.setStyle("-fx-border-color: chartreuse;-fx-border-width: 0" + css);
//            if (SettingAppE.instance.isTypeshow)
//                currentTextField.getStyleClass().add("delivery_3d");
//        } else {
//            currentTextField.setStyle("-fx-border-color: chartreuse;-fx-border-width: 0;");
//        }
//        if (textFocus == kb_text_field) {
//            textFocus.setStyle("-fx-border-color: chartreuse;-fx-border-width: 3" + css);
//            if (SettingAppE.instance.isTypeshow)
//                textFocus.getStyleClass().add("delivery_3d");
//        } else {
//            textFocus.setStyle("-fx-border-color: chartreuse;-fx-border-width: 3;");
//        }
//
//        currentTextField = textFocus;
//        Platform.runLater(new Runnable() {
//            @Override
//            public void Run() {
//                textFocus.requestFocus();
//            }
//        });
    }

    private void onManyAction(ActionEvent actionEvent) {
        Button b = (Button) actionEvent.getSource();
        double getmany = 0d;
        try {
            getmany = Double.parseDouble(tb_get_many.getText().trim());
        } catch (Exception ignored) {
            getmany = 0;
            tb_get_many.setText(String.valueOf(0));
        }

        switch (b.getId()) {
            case "bt_50": {
                getmany = getmany + 50;
                break;
            }
            case "bt_100": {
                getmany = getmany + 100;
                break;
            }

            case "bt_200": {
                getmany = getmany + 200;
                break;
            }
            case "bt_500": {
                getmany = getmany + 500;
                break;
            }
            case "bt_1000": {
                getmany = getmany + 1000;
                break;
            }

            case "bt_2000": {
                getmany = getmany + 2000;
                break;
            }
            case "bt_5000": {
                getmany = getmany + 5000;
                break;
            }
        }
        tb_get_many.setText(String.valueOf(getmany));

    }


    private void recalculation() {
        double sum = 0d;
        try {
            sum = Double.parseDouble(tb_check_summ.getText().trim());
        } catch (Exception ignored) {
            sum = 0;
            tb_check_summ.setText(String.valueOf(0d));
        }
        double getmany = 0d;
        try {
            getmany = Double.parseDouble(tb_get_many.getText().trim());
        } catch (Exception ignored) {
            getmany = 0;
            //tb_get_many.setText(String.valueOf(0));
        }
        double delivery = 0d;
        delivery = getmany - sum;
        tb_delivery.setText(String.valueOf(UtilsOmsk.round(delivery, 2)));

    }


    public void printBarcode(String text) {

        if (currentTextField == null) return;
        if (currentTextField == kb_text_field) {
            if (text.equals("\r")) {

                addProd(kb_text_field.getText());
            }
            if (text.equals("\b") && kb_text_field.getText().length() > 0) {

                kb_text_field.setText(kb_text_field.getText().substring(0, kb_text_field.getText().length() - 1));
            } else {
                if (isNumeric(text)) {
                    kb_text_field.setText(kb_text_field.getText() + text);
                    System.out.println(kb_text_field.getText());
                } else {
                    kb_text_field.setText("");
                }
            }
        } else {

            if (text.equals("\b") && currentTextField.getText().length() > 0) {

                currentTextField.setText(currentTextField.getText().substring(0, currentTextField.getText().length() - 1));
            } else {

                if (text.equals(",")) {
                    text = ".";
                }
                if (currentTextField.getText().equals("0")) {
                    currentTextField.setText(text);
                } else {
                    currentTextField.setText(currentTextField.getText() + text);
                }
                System.out.println(currentTextField.getText());
            }
        }
        System.out.println(text);
    }

    private boolean addProd(String s) {
        MBarcodes b = null;
        for (MBarcodes mBarcode : mBarcodes) {
            if (mBarcode.barcode.equals(s)) {
                b = mBarcode;
                break;
            }
        }
        if (b != null) {

            List<MProduct> mProducts = Configure.GetSession().getList(MProduct.class, " id_core = ? ", b.id_product);
            if (mProducts.size() > 0) {
                MProduct pr = mProducts.get(0);
                boolean es = false;
                for (MProduct mProduct : SettingAppE.instance.selectProduct) {
                    if (mProduct.code_prod.equals(pr.code_prod)) {
                        mProduct.selectAmount = mProduct.selectAmount + 1;
                        es = true;
                    }
                }
                if (es == false) {
                    SettingAppE.instance.selectProduct.add(pr);
                    pr.selectAmount = 1;
                    if (SettingAppE.instance.selectProduct.size() == 1) {
                        tb_get_many.setText("0");
                    }
                }
            }

        }
        new Additor().refresh(box, new IAction() {
            @Override
            public boolean action(Object o) {

                refrachCheck();
                return false;
            }
        });
        return false;
    }

    public static boolean isNumeric(String str) {
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }


    public void onPrintCheck() {
        List<MProduct> mProducts = SettingAppE.instance.selectProduct;
        if (mProducts.size() == 0) return;
        List<MProduct> selectProduct = new ArrayList<>(mProducts);
        new Transaction().saleCache(selectProduct);
    }

    @Override
    public void destroy() {

        grid1.setOnKeyTyped(null);
    }

    public void addBarcode(String text) {
        currentTextField = kb_text_field;
        kb_text_field.setText(text);
    }

    public void refrachCheck() {
        textFieldFocusable(kb_text_field);
        new Additor().refresh(box, new IAction() {
            @Override
            public boolean action(Object o) {
                refrachCheck();
                return false;
            }
        });
        kb_text_field.setText("");
        double total = 0;
        double amount = 0d;

        for (MProduct p : SettingAppE.instance.selectProduct) {

            total = total + p.price * p.selectAmount;
            amount = amount + p.selectAmount;

        }
        label_nds.setText(String.valueOf(total));
        if (total == 0) {
            label_nds2.setText(String.valueOf(0d));
        }
        double s1 = (total / 1.18);
        double s = (total - s1);
        label_nds2.setText(String.valueOf(UtilsOmsk.round(s, 2)));
        tb_check_summ.setText(String.valueOf(UtilsOmsk.round(total, 2)));
        label_itogo_amount.setText(String.valueOf(amount));
        label_itogo_summ.setText(String.valueOf(UtilsOmsk.round(total, 2)));
    }


    public void refrashKeyBoard(String s) {
        if (kb_text_field.isFocused() == false) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    kb_text_field.requestFocus();
                    kb_text_field.setText(s);
                    kb_text_field.selectBackward();
                    kb_text_field.positionCaret(1);

                }
            });
        }
    }

    @Override
    public void focus() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                kb_text_field.requestFocus();
            }
        });
    }

    @Override
    public void refrashUser() {
        String userid = SettingAppE.instance.user_id;
        if (userid == null) {
            FUserBlender fUserBlender = new FUserBlender();
            stack22.getChildren().add(fUserBlender);
            kb_text_field.setDisable(true);
            watcherScaner = null;
        } else {

            if (stack22.getChildren().size() > 2) {
                stack22.getChildren().remove(1);
            }
            kb_text_field.setDisable(false);

            watcherScaner = new WatcherScaner(stack21, new IAction() {
                @Override
                public boolean action(Object o) {
                    kb_text_field.setText("");
                    MProduct prod = (MProduct) o;
                    kb_text_field.setText("");
                    boolean f = false;
                    for (int i = 0; i < SettingAppE.instance.selectProduct.size(); i++) {
                        if (SettingAppE.instance.selectProduct.get(i).code_prod.equals(prod.code_prod)) {
                            ++SettingAppE.instance.selectProduct.get(i).selectAmount;
                            f = true;
                        }
                    }
                    if (f == false) {
                        prod.selectAmount = 1;
                        SettingAppE.instance.selectProduct.add(prod);
                        if (SettingAppE.instance.selectProduct.size() == 1) {
                            tb_get_many.setText("0");
                        }
                    }

                    SettingAppE.save();
                    refrachCheck();
                    return false;
                }
            });
            watcherScaner.run(kb_text_field);
        }
    }

    @Override
    public void clearTextField() {
        kb_text_field.setText("");
    }
}



